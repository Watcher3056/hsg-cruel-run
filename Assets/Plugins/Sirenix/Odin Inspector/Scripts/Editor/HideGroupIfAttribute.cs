﻿#if UNITY_EDITOR

namespace Sirenix.OdinInspector
{
    using UnityEngine;
    using UnityEditor;
    using Sirenix.OdinInspector;
    using Sirenix.OdinInspector.Editor;
    using Sirenix.Utilities.Editor;

    public class HideGroupIfAttribute : PropertyGroupAttribute
    {
        public HideGroupIfAttribute(string path)
            : base(path)
        {
        }
    }

    public class HideGroupIfAttributeDrawer : OdinGroupDrawer<HideGroupIfAttribute>
    {
        private InspectorPropertyValueGetter<bool> hideMember;

        protected override void Initialize()
        {
            this.hideMember = new InspectorPropertyValueGetter<bool>(this.Property, this.Property.Name.TrimStart('#'));
        }

        protected override void DrawPropertyLayout(GUIContent label)
        {
            var show = !this.hideMember.GetValue();
            if (SirenixEditorGUI.BeginFadeGroup(this, show))
            {
                for (int i = 0; i < this.Property.Children.Count; i++)
                {
                    this.Property.Children[i].Draw();
                }
            }
            SirenixEditorGUI.EndFadeGroup();
        }
    }
}
#endif