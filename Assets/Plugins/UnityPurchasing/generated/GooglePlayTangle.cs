#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("hzW2lYe6sb6dMf8xQLq2trayt7TXqlElQc2W25753oOSmV3lu9Wo0emD+6xTLSot8z//3bYxm/WVuTp0pu20OdCknW3D41LAj/vcLaqaKPHqG9KJoW53cLI1hrRFDMKCIdiYPjW2uLeHNba9tTW2trc98sS8VLKap0UMja7fXQ9OS3SoRnnnKziebDK+i4j8BlX5pekV4Z2/eBcI4ns8HeZnZET0yjz0Bu19H3PDwV7zgdQu4YD0JBHhElOF4iorbHRAzx3vS+Vj0ass/kDVsbG03VBUUqmmsYbeL+2rodmZEDgqGcF5Zl6DzEs/9bXeLwghQHBEXqYKGPyGoi4bVBK6nGV1IMuTBIJ0+kvy/La/8YTzOnReKbVOrhmIPUwW5rW0tre2");
        private static int[] order = new int[] { 0,3,5,13,7,13,6,11,11,13,10,13,12,13,14 };
        private static int key = 183;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
