#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("l5fOgZCQjIXOg4+Nz4GQkIyFg4HAj4bAlIiFwJSIhY7AgZCQjImDgdPWutCC0evQ6ebjteTm8+K1s9HziYaJg4GUiY+OwKGVlIiPkomUmdF1fprsRKdruzT219MrJO+tLvSJMZ+hSHgZMSqGfMSL8TBDWwT7yiP/YuHg5unKZqhmF4OE5eHQYRLQyuZeFJN7DjKE7yuZr9Q4Qt4ZmB+LKDnWnyFntTlHeVnSohs4NZF+nkGy1nmszZhXDWx7PBOXexKWMpfQryGa0GLhltDu5uO1/e/h4R/k5OPi4ejL5uHl5efi4fb+iJSUkJPaz8+XgoyFwJOUgY6EgZKEwJSFko2TwIHAgY6EwIOFkpSJhomDgZSJj47AkNDx5uO15Orz6qGQkIyFwKmOg87Rh2/oVMAXK0zMwI+QVt/h0GxXoy9vk2GAJvu76c9yUhikqBCA2H71Fak4ln/T9IVBl3QpzeLj4eDhQ2LhUdC4DLrk0myIU2/9PoWTH4e+hVyUiI+SiZSZ0fbQ9ObjteTj8+2hkMzAg4WSlImGiYOBlIXAkI+MiYOZVdpNFO/u4HLrUcH2zpQ13O07gvb/ZWNl+3ndp9cSSXugbsw0UXDyONXS0dTQ09a69+3T1dDS0NnS0dTQSDyewtUqxTU57zaLNELEw/EXQUzm0O/m47X98+HhH+Tl0OPh4R/Q/SCD05cX2ufMtgs678HuOlqT+a9V7ebpymaoZhft4eHl5eDjYuHh4LyyhYyJgY6DhcCPjsCUiImTwIOFkoTVw/Wr9bn9U3QXFnx+L7BaIbiwuUfl6Zz3oLbx/pQzV2vD26dDNY/ovtBi4fHm47X9wORi4ejQYuHk0N3Gh8Bq04oX7WIvPgtDzxmziruEkIyFwLKPj5TAo6HQ/vft0NbQ1NKOhMCDj46EiZSJj46TwI+GwJWThf9xO/6nsAvlDb6ZZM0L1kK3rLUM733dE8upyPooHi5VWe45vvw2K90p+ZIVve41n797EsXjWrVvrb3tEcQCCzFXkD/vpQHHKhGNmA0HVff3S0ORcqeztSFPz6FTGBsDkC0GQ6yUiYaJg4GUhcCCmcCBjpnAkIGSlFf7XXOixPLKJ+/9Vq18voMoq2D39tD05uO15OPz7aGQkIyFwLKPj5SQjIXAo4WSlImGiYOBlImPjsChlcbQxObjteTr8/2hkJCMhcCjhZKUzqBGF6etn+i+0P/m47X9w+T40Pbk5vPitbPR89Dx5uO15Orz6qGQkMpmqGYX7eHh5eXg0ILR69Dp5uO1mcCBk5OVjYWTwIGDg4WQlIGOg4WMhcCpjoPO0cbQxObjteTr8/2hkJKBg5SJg4XAk5SBlIWNhY6Uk87QYPTLMImndJbpHhSLbc6gRhenrZ/m47X97uT25PTLMImndJbpHhSLbWv5aT4Zq4wV50vC0OII+N4YsOkzpZ7/rIuwdqFpJJSC6/BjoWfTamHAo6HQYuHC0O3m6cpmqGYX7eHh4c/QYSPm6Mvm4eXl5+Li0GFW+mFT0GLkW9Bi40NA4+Lh4uLh4tDt5unl4ONi4e/g0GLh6uJi4eHgBHFJ6ecMndlja7PAM9gkUV96r+qLH8scsEpqNToEHDDp59dQlZXB");
        private static int[] order = new int[] { 36,41,8,11,18,17,34,50,58,48,28,35,42,45,48,21,55,27,26,27,44,23,49,50,54,29,57,47,38,44,52,33,56,34,43,50,49,47,53,54,54,59,42,57,53,57,58,59,54,50,58,56,52,55,55,57,59,58,58,59,60 };
        private static int key = 224;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
