﻿using System;
using Homebrew;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

namespace HSG
{
    //Процессинг отложенных операций
    //В него можно добавлять инструкции, которые требуют выполнения какого-то условия
    //Например:
    //UnityAction action = null;
    //action = new UnityAction(() => { if (condition)  {some code} });
    //ProcessingDeferredOperation.Default.Add(action);
    //В конце инструкции "some code" нужно удалить инструкцию action из процессинга
    //ProcessingDeferredOperation.Default.Remove(action);

    //ИТОГ: инструкция action будет проигрываться каждый Tick, пока не выполниться и не удалиться
    public class ProcessingDeferredOperation : ITick, IKernel, IDisposable
    {
        private class Item
        {
            public Action action;
            public bool triggerOnce;
            public bool autoCatch;
            public float triggerPeriod;

            public float curPeriodBuffer;
        }

        public static ProcessingDeferredOperation Default => _default;
        private static ProcessingDeferredOperation _default;

        private List<Item> items;

        public void Setup()
        {
            _default = this;

            ProcessingGroupAttributes.Default.Setup(this);
            ProcessingSignals.Default.Add(this);
            ProcessingUpdate.Default.Add(this);

            items = new List<Item>();

            Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + "PROCESSING" + "] " + "[" + this + "]" + "----Setup End");
        }
        public void Dispose() //Invoke when scene changing
        {
            Toolbox.Get<ProcessingSignals>().Remove(this);
            ProcessingUpdate.Default.Remove(this);

            Setup();
        }
        public void Tick()
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].triggerPeriod > items[i].curPeriodBuffer)
                {
                    items[i].curPeriodBuffer += UnityEngine.Time.unscaledDeltaTime;
                    continue;
                }
                else
                    items[i].curPeriodBuffer = 0f;
                if (items[i].autoCatch)
                {
                    try
                    {
                        items[i].action.Invoke();
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError("ProcessingDeferredOperation: " + ex.Message);
                    }
                }
                else
                    items[i].action.Invoke();
                if (items[i].triggerOnce)
                {
                    items.RemoveAt(i);
                    i--;
                }
            }
        }
        public void Add(Action action, bool triggerOnce = false, bool autoCatch = false, float triggerPeriod = 0f)
        {
            items.Add(new Item { action = action, triggerOnce = triggerOnce, triggerPeriod = triggerPeriod, autoCatch = autoCatch });
        }
        public void Remove(Action action)
        {
            items.Remove(items.Find((x) => { return x.action == action; }));
        }
    }
}
