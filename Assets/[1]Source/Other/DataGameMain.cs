﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataGameMain", fileName = "DataGameMain")]
    [Serializable]
    public class DataGameMain : SerializedScriptableObject
    {
        public static DataGameMain Default => _default;
        private static DataGameMain _default;

        //[FoldoutGroup("Player")]
        //public List<GameObject> playerSkins;
        [AssetList(Path = "[2]Content/Characters/")]
        public List<DataCharacter> characters;
        [AssetList(Path = "[2]Content/Locations/")]
        public List<DataLocation> locations;
        [AssetList(Path = "[2]Content/Achievements/")]
        public List<DataAchievement> achievements;

        public DataGameMain()
        {
            _default = this;
        }
    }
}
