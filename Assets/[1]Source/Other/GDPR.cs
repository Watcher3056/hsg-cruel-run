﻿using Beebyte.Obfuscator;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[Skip]
public class GDPR : MonoBehaviour
{

    public Transform mainPanel;
    public Transform yesPanel;
    public Transform noPanel;
    public TextMeshProUGUI mainText;

    public static bool closeApp;

    string mainString = " personalizes your advertising experience using Appodeal. Appodeal and its partners may collect and process personal data such as device identifiers, location data, and other demographic and interest data to provide advertising experience tailored to you. By consenting to this improved ad experience, you'll see ads that Appodeal and its partners believe are more relevant to you. Learn more. By agreeing, you confirm that you are over the age of 16 and would like a personalized ad experience.";

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        mainText.text = "Cruel Run" + mainString;
    }
    [SkipRename]
    public void onYesClick()
    {
        PlayerPrefs.SetInt("result_gdpr", 1);
        PlayerPrefs.SetInt("result_gdpr_sdk", 1);
        mainPanel.gameObject.SetActive(false);
        yesPanel.gameObject.SetActive(true);
    }
    [SkipRename]
    public void onNoClick()
    {
        PlayerPrefs.SetInt("result_gdpr", 1);
        PlayerPrefs.SetInt("result_gdpr_sdk", 0);
        mainPanel.gameObject.SetActive(false);
        noPanel.gameObject.SetActive(true);
    }
    [SkipRename]
    public void onPLClick()
    {
        Application.OpenURL("https://www.appodeal.com/privacy-policy");
    }
    [SkipRename]
    public void onCloseClick()
    {
        if (closeApp)
            Application.Quit();
        else
            Homebrew.ProcessingSceneLoad.To("loading");
    }

}