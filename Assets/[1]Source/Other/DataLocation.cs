﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;


namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataLocation", fileName = "DataLocation")]
    [Serializable]
    public class DataLocation : SerializedScriptableObject
    {
        #region Settings
        [Serializable]
        public class ComboObstacle
        {
            public ComponentObstacleDefault.ComboObstacleChance chance;
            public List<ComponentObstacleDefault> obstacles;
        }

        public string localizationNameKey;
        [PreviewField(80f, ObjectFieldAlignment.Left)]
        public Sprite previewImage;
        public AudioClip mainTheme;
        public int costToUnlock;
        public float costToUnlockDollars;
        public DataCharacter characterToUnlock;
        public List<ComponentTrackSegment> segmentPrefabs;
        public List<ComponentObstacleDefault> possibleObstacles;
        [ListDrawerSettings(ShowIndexLabels = false, NumberOfItemsPerPage = 5)]
        public List<ComboObstacle> combos;
        #endregion Settings

        public bool IsUnlocked { get { return ProcessingSaveLoad.Load(localizationNameKey, false); } }
        public int LocalDistanceRecord
        {
            set
            {
                //localDistanceRecord = value;
                ProcessingSaveLoad.Save(LocalRecordTag, value);
            }
            get { return ProcessingSaveLoad.Load(LocalRecordTag, 0); }
        }
        //private int localDistanceRecord;

        private string LocalRecordTag { get { return localizationNameKey + ".LocalRecord"; } }

        public void Unlock()
        {
            ProcessingSaveLoad.Save(localizationNameKey, true);
            if (characterToUnlock != null) characterToUnlock.Unlock();
            Debug.Log(localizationNameKey + " --- UNLOCKED!");
        }
        public void Lock()
        {
            ProcessingSaveLoad.Save(localizationNameKey, false);
            characterToUnlock.Lock();
            Debug.Log(localizationNameKey + " --- LOCKED!");
        }
    }
}
