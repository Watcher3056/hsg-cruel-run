﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using Sirenix.OdinInspector;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataGameSession", fileName = "DataGameSession")]
    public class DataGameSession : DataGame, IAwake
    {
        #region SaveTags
        private const string TagRecordDistance = "RecordDistance";
        private const string TagGodTears = "GodTears";
        private const string TagGodTearsTotalCollected = "GodTearsTotalCollected";
        #endregion

        public static DataGameSession Default
        {
            get
            {
                return _default;
            }
        }
        public static DataGameSession _default;

        public int RecordDistance
        {
            get { return recordDistance; }
            set
            {
                recordDistance = value;
                ProcessingSaveLoad.Save(TagRecordDistance, value);
            }
        }
        [ReadOnly]
        [FoldoutGroup("Status")]
        private int recordDistance;
        public int GodTearsCount
        {
            get { return godTearsCount; }
            set
            {
                Debug.Log(GetHashCode() + " godTearsCount New: " + value + " Prev: " + godTearsCount);
                godTearsCount = value;
                ProcessingSaveLoad.Save(TagGodTears, value);
            }
        }
        [ReadOnly]
        [FoldoutGroup("Status")]
        private int godTearsCount;
        public int GodTearsTotalCollected
        {
            get { return godTearsTotalCollected; }
            set
            {
                godTearsTotalCollected = value;
                ProcessingSaveLoad.Save(TagGodTearsTotalCollected, value);
            }
        }
        [ReadOnly]
        [FoldoutGroup("Status")]
        private int godTearsTotalCollected;

        public void OnAwake()
        {
            _default = this;
            Debug.Log(GetHashCode() + " DataGameSession Created!");
            ProcessingSaveLoad.onLocalDataUpdated += Load;
        }
        [Button]
        public void CleanSavesLocal()
        {
            ProcessingSaveLoad.CleanSaves();
        }

        public void SyncProgressWithCloud()
        {
            if (Debug.isDebugBuild || !Social.localUser.authenticated)
                return;
            Debug.Log("ReportScores: Started!");
            Social.ReportScore(RecordDistance, GPGSIds.leaderboard_dashboard, ReportScoreCallback);

            void ReportScoreCallback(bool success)
            {
                Debug.Log("ReportScores: " + success);
            }

            string[] userIds = new string[] { Social.localUser.id };
            ILeaderboard highScoresBoard = Social.CreateLeaderboard();
            highScoresBoard.id = GPGSIds.leaderboard_dashboard;
            highScoresBoard.SetUserFilter(userIds);

            highScoresBoard.LoadScores(onLeaderboardLoadComplete);

            void onLeaderboardLoadComplete(bool success)
            {
                Debug.Log("onLeaderboardLoadComplete: " + success);
                if (success)
                {
                    RecordDistance = RecordDistance < (int)highScoresBoard.localUserScore.value ? (int)highScoresBoard.localUserScore.value : RecordDistance;
                }
            }

        }
        public void Load()
        {
            recordDistance = ProcessingSaveLoad.Load(TagRecordDistance, 0);
            Debug.Log(GetHashCode() + "Loaded recordDistance: " + recordDistance);
            godTearsCount = ProcessingSaveLoad.Load(TagGodTears, 0);
            Debug.Log(GetHashCode() + "Loaded godTearsCount: " + godTearsCount);
            godTearsTotalCollected = ProcessingSaveLoad.Load(TagGodTearsTotalCollected, 0);
            Debug.Log(GetHashCode() + "Loaded godTearsTotalCollected: " + godTearsTotalCollected);
        }
    }
}
