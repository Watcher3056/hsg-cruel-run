﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace HSG
{
#if UNITY_EDITOR
    [ExecuteInEditMode]
#endif
    [RequireComponent(typeof(RectTransform))]
    public class SimpleGrid : MonoBehaviour
    {
        public bool dynamicSize;
        public float cellScale = 1f;
        [MinValue(1)]
        public int cellCount;
        [MinValue(1)]
        public int rowCount;
        public float offsetByCellX;
        public float offsetByCellY;
        public float offsetTop, offsetlLeft;

        private bool wasUpdated, prevGOState;
        private RectTransform localRect;
        private void Update()
        {
            wasUpdated = false;
            localRect = GetComponent<RectTransform>();
            UpdateView();
        }
        void UpdateView()
        {
            if (wasUpdated || !gameObject.activeSelf)
                return;

            Vector2 startPoint = Vector2.zero;
            startPoint.x += offsetlLeft * cellScale;
            startPoint.y -= offsetTop * cellScale;

            Vector2 newLocalSize = new Vector2();
            Vector2 newPos = new Vector2();
            RectTransform rt = null;
            for (int i = 0; i < transform.childCount; i++)
            {
                newPos.x = startPoint.x + offsetByCellX * cellScale * (i % rowCount);
                newPos.y = startPoint.y - offsetByCellY * cellScale * ((int)((float)i / (float)rowCount));
                rt = transform.GetChild(i).GetComponent<RectTransform>();
                rt.anchorMax = new Vector2(0, 1f);
                rt.anchorMin = new Vector2(0, 1f);
                rt.anchoredPosition = newPos;
                rt.localScale = Vector3.one * cellScale;

                if (dynamicSize)
                {
                    //newLocalSize.x += rt.sizeDelta.x * (i % rowCount);
                    newLocalSize.y += (i % rowCount == 1 ? rt.sizeDelta.y : 0) * cellScale;
                }
            }
            if (dynamicSize)
                localRect.sizeDelta = newLocalSize;
            wasUpdated = true;
        }
    }
}
