﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    [CreateAssetMenu(menuName = "SimpleCommands", fileName = "SimpleCommands")]
    public class SimpleCommands : ScriptableObject
    {
        public void KillPlayer()
        {
            ProcessingTrackManager.Default.Player.CurState = ActorPlayer.State.Dead;
        }
    }
}
