﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;


namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataCharacter", fileName = "DataCharacter")]
    [Serializable]
    public class DataCharacter : SerializedScriptableObject
    {
        [Serializable]
        public class Skin
        {
            [PreviewField(80f, ObjectFieldAlignment.Left)]
            public GameObject prefab;
            [PreviewField(80f, ObjectFieldAlignment.Left)]
            public Sprite previewImage;

            [HideInInspector]
            public bool IsUnlocked { get { return ProcessingSaveLoad.Load(unique, false); } }
            [ReadOnly]
            public string unique;

            public void Unlock() { ProcessingSaveLoad.Save(unique, true); Debug.Log(unique + " --- UNLOCKED!"); }
            public void Lock()
            {
                ProcessingSaveLoad.Save(unique, false);
                Debug.Log(unique + " --- LOCKED!");
            }
        }
        [Serializable]
        public class ComicsPage
        {
            public Sprite ru;
            public Sprite en;
        }

        public bool IsUnlocked { get { return ProcessingSaveLoad.Load(localizationNameKey, false); } }
        public bool ComicsIsReaded
        {
            set { ProcessingSaveLoad.Save(localizationNameKey + ".Comics", value); }
            get { return ProcessingSaveLoad.Load(localizationNameKey + ".Comics", false); }
        }

        public string localizationNameKey;
        [OnValueChanged("UpdateSkinsList")]
        public List<Skin> skins;
        [PreviewField(80f, ObjectFieldAlignment.Left)]
        public Sprite previewImage;
        public List<string> notes;
        public List<ComicsPage> comics;

        public void UpdateSkinsList()
        {
            for (int i = 0; i < skins.Count; i++)
                skins[i].unique = localizationNameKey + ".Skin" + i;
        }

        public void Unlock()
        {
            ProcessingSaveLoad.Save(localizationNameKey, true);
            skins[0].Unlock();
            Debug.Log(localizationNameKey + " --- UNLOCKED!");
        }
        public void Lock()
        {
            ProcessingSaveLoad.Save(localizationNameKey, false);
            ClearNotesProgress();
            for (int i = 0; i < skins.Count; i++)
                skins[i].Lock();
            Debug.Log(localizationNameKey + " --- LOCKED!");
        }

        public void SetNoteReaded(int i, bool arg) { ProcessingSaveLoad.Save(localizationNameKey + ".Note" + i, arg); }
        public bool GetNoteReaded(int i) { return ProcessingSaveLoad.Load(localizationNameKey + ".Note" + i, false); }
        public int NoteReadedProgress()
        {
            int progress = 0;
            for (int i = 0; i < notes.Count; i++)
                progress += GetNoteReaded(i) ? 1 : 0;
            return progress;
        }
        public static int NoteReadedProgressByAll()
        {
            int progress = 0;
            foreach (DataCharacter character in DataGameMain.Default.characters)
                progress += character.NoteReadedProgress();
            return progress;
        }
        public void UnlockNewNote()
        {
            if (UnlockedNotesCount() == notes.Count)
                return;
            ProcessingSaveLoad.Save(localizationNameKey + ".NoteGatherProgress", UnlockedNotesCount() + 1);
            if (UnlockedNotesCount() == notes.Count)
                for (int i = 0; i < skins.Count; i++)
                    skins[i].Unlock();
        }
        public int UnlockedNotesCount() { return ProcessingSaveLoad.Load(localizationNameKey + ".NoteGatherProgress", 0); }
        public static int UnlockedNotesCountByAll()
        {
            int result = 0;
            foreach (DataCharacter character in DataGameMain.Default.characters)
                result += character.UnlockedNotesCount();
            return result;
        }
        public static int NotesCountByAll()
        {
            int result = 0;
            foreach (DataCharacter character in DataGameMain.Default.characters)
                result += character.notes.Count;
            return result;
        }
        public void ClearNotesProgress()
        {
            //Clear Reading Progress
            for (int i = 0; i < notes.Count; i++)
                ProcessingSaveLoad.Save(localizationNameKey + ".Note" + i, false);
            //Clear Gather Progress
            ProcessingSaveLoad.Save(localizationNameKey + ".NoteGatherProgress", 0);
        }
        public DataLocation GetRequiredLocation()
        {
            return DataGameMain.Default.locations.Find((loc) => { return loc.characterToUnlock.localizationNameKey == localizationNameKey; });
        }
    }
}
