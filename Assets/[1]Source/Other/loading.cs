﻿using Homebrew;
using HSG;
using UnityEngine;

public class loading : MonoCached
{

    protected override void OnAwake()
    {
        int consentInt = PlayerPrefs.GetInt("result_gdpr", 0);
        bool consent = consentInt != 0;
        if (consent)
        {
#if UNITY_5_3_OR_NEWER
            if (ProcessingSaveLoad.LoadPP("IntroComicsReleased", false))
                ProcessingSceneLoad.To("GameScene");
            else
                ProcessingSceneLoad.To("IntroComics");
#else
			Application.LoadLevel("GameScene");
#endif
        }
        else
        {
#if UNITY_5_3_OR_NEWER
            ProcessingSceneLoad.To("GDPR");
#else
			Application.LoadLevel("GDPR");
#endif
        }
    }
}
