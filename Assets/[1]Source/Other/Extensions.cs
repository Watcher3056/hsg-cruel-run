﻿using Homebrew;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace HSG
{
    public static partial class Functions
    {
        public static string GetFullName(this GameObject go)
        {
            string name = go.name;
            while (go.transform.parent != null)
            {

                go = go.transform.parent.gameObject;
                name = go.name + "/" + name;
            }
            return name;
        }
        public static void RemoveNull<T>(List<T> list) where T : class
        {
            for (int i = 0; i < list.Count; i++)
                if (list[i] == null)
                {
                    list.RemoveAt(i);
                    i--;
                }
        }
        public static bool IsLive(this MonoCached obj)
        {
            if (obj == null)
                return false;
            if (obj.state.HasState(EntityState.Released))
                return false;
            return true;
        }
        public static GameObject FindObject(this GameObject parent, string name)
        {
            Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
            foreach (Transform t in trs)
            {
                if (t.name == name)
                {
                    return t.gameObject;
                }
            }
            return null;
        }
        public static void SetActiveAllComponents(this GameObject content, bool active)
        {
            List<Behaviour> components = new List<Behaviour>(content.GetComponentsInChildren<Behaviour>(true));
            components.AddRange(content.GetComponents<Behaviour>());
            for (int i = 0; i < components.Count; i++)
                components[i].enabled = active;
        }
        public static void ChangeLayerForAll(this IEnumerable<SpriteRenderer> renders, int increment)
        {
            foreach (SpriteRenderer renderer in renders)
                renderer.sortingOrder += increment;
        }
        public static List<T> GetAllComponents<T>(this GameObject obj)
        {
            List<T> result = new List<T>();
            result.AddRange(obj.GetComponents<T>());

            for (int i = 0; i < obj.transform.childCount; i++)
                result.AddRange(obj.transform.GetChild(i).gameObject.GetAllComponents<T>());

            return result;
        }
        public static T FindNearest<T>(this GameObject obj, params GameObject[] exclude) where T : MonoBehaviour
        {
            List<GameObject> listExclude = new List<GameObject>(exclude);
            listExclude.Add(obj);
            List<T> objects = new List<T>(GameObject.FindObjectsOfType<T>());
            T nearestObject = null;
            for (int i = 0; i < objects.Count; i++)
            {
                if (listExclude.Contains(objects[i].gameObject))
                    continue;
                if (nearestObject == null)
                    nearestObject = objects[i];
                else if
                    ((Vector3.Distance(obj.transform.position, objects[i].transform.position) <
                    Vector3.Distance(obj.transform.position, nearestObject.transform.position)))
                    nearestObject = objects[i];
            }
            return nearestObject;
        }
        public static T FindNearest<T>(this GameObject obj, float maxSearchDistance, params GameObject[] exclude) where T : MonoBehaviour
        {
            T nearestObject = obj.FindNearest<T>(exclude);

            if (nearestObject != null &&
                Vector3.Distance(nearestObject.transform.position, obj.transform.position) > maxSearchDistance)
                return null;
            return nearestObject;
        }
        public static bool IsObjectVisible(this Camera camera, Renderer renderer)
        {
            return GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(camera), renderer.bounds);
        }
        public static List<T> GetAllPrefabsWithComponent<T>(string path) where T: MonoBehaviour
        {
            List<GameObject> temp = GetAllPrefabs(path);
            List<T> result = new List<T>();
#if UNITY_EDITOR
            for (int i = 0; i < temp.Count; i++)
                if (temp[i].GetComponent<T>() != null)
                    result.Add(temp[i].GetComponent<T>());
#endif
            return result;
        }
        public static List<GameObject> GetAllPrefabs(string path)
        {
            List<GameObject> result = new List<GameObject>();
#if UNITY_EDITOR
            if (!path.Contains(Application.dataPath))
                path = path.Insert(0, Application.dataPath + "/");
            if (path[path.Length - 1] != '/')
                path += '/';
            string[] aFilePaths = Directory.GetFiles(path);

            string[] directoriesInPath = Directory.GetDirectories(path);

            for (int i = 0; i < directoriesInPath.Length; i++)
            {
                Debug.Log("Search in: " + directoriesInPath[i]);
                result.AddRange(GetAllPrefabs(directoriesInPath[i]));
            }
            foreach (string sFilePath in aFilePaths)
            {
                if (!sFilePath.Contains(".prefab") || sFilePath.Contains(".meta"))
                    continue;
                string sAssetPath = sFilePath.Substring(Application.dataPath.Length - 6);

                GameObject objAsset = UnityEditor.AssetDatabase.LoadAssetAtPath(sAssetPath, typeof(GameObject)) as GameObject;

                if (objAsset != null)
                    result.Add(objAsset);
            }
#endif
            return result;
        }
    }
}
