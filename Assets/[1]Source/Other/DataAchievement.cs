﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Sirenix.OdinInspector;
using Homebrew;
using Sirenix.Serialization;
using GooglePlayGames;

namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataAchievement", fileName = "DataAchievement")]
    public class DataAchievement : SerializedScriptableObject
    {
        public struct Progress { public int current; public int required; }
        #region TagSaves
        private string TagIsUnlocked { get { return id_GooglePlay + ".IsUnlocked"; } }
        #endregion
        #region Settings
        [Serializable]
        public class Distance
        {
            [Serializable]
            public class LocationDistance
            {
                [Required]
                public DataLocation location;
                public int distanceRequired;
            }

            public bool onSpecificLocations;
            [ShowIf("onSpecificLocations")]
            [ValidateInput("_ValidateInputList", "Location must be assigned!"), HideReferenceObjectPicker]
            public List<LocationDistance> list;
            [MinValue(0)]
            [HideIf("onSpecificLocations")]
            public int distanceRequired;
            private bool _ValidateInputList(List<LocationDistance> list)
            {
                return (onSpecificLocations && list.Count > 0) || !onSpecificLocations;
            }

            public Progress GetProgress()
            {
                Progress progress = new Progress();
                if (onSpecificLocations)
                {
                    if (list.Count == 1)
                    {
                        progress.current = list[0].location.LocalDistanceRecord;
                        progress.required = list[0].distanceRequired;
                    }
                    else
                    {
                        foreach (LocationDistance item in list)
                            if (item.location.LocalDistanceRecord >= item.distanceRequired)
                                progress.current += 1;
                        progress.required = list.Count;
                    }
                }
                else
                    return new Progress { current = DataGameSession.Default.RecordDistance, required = distanceRequired };
                return progress;
            }
        }
        [Serializable]
        public class GodTearsBalance
        {
            public enum Type { TotalCollected, TotalOnBalance }
            [EnumToggleButtons]
            public Type type;
            [MinValue(0)]
            public int godTearsRequired;

            public Progress GetProgress()
            {
                if (type == Type.TotalCollected)
                    return new Progress { current = DataGameSession.Default.GodTearsTotalCollected, required = godTearsRequired };
                else
                    return new Progress { current = DataGameSession.Default.GodTearsCount, required = godTearsRequired };
            }
        }
        [Serializable]
        public class DieByTraps
        {
            [Serializable]
            public class ChainItem
            {
                [ValidateInput("_ValidateInputGO", "Only Obstacles can be assigned!")]
                public GameObject trap;
                [MinValue(0)]
                public int killCount;

                private bool _ValidateInputGO(GameObject go)
                {
                    if (go != null)
                        return go.GetComponent<ComponentObstacleDefault>() != null;
                    else
                        return false;
                }
            }
            public enum Type { Standart, DeathChain, ByAll }
            [EnumToggleButtons, OnValueChanged("_OnValueChangedType")]
            public Type type;

            [MinValue(0)]
            public int minimumDistanceRequired;

            [ShowIf("type", Type.ByAll), OnValueChanged("_OnValueChangedByAllAmount")]
            public int byAllAmount;

            [DisableIf("type", Type.ByAll), HideReferenceObjectPicker]
            public List<ChainItem> list = new List<ChainItem>();

            public void _OnValueChangedType(Type type)
            {
                if (type == Type.ByAll)
                {
                    list.Clear();
                    List<ComponentObstacleDefault> _list = Functions.GetAllPrefabsWithComponent<ComponentObstacleDefault>("[2]Content/");
                    for (int i = 0; i < _list.Count; i++)
                        if (!_list[i].isSafe)
                            list.Add(new ChainItem { trap = _list[i].gameObject, killCount = byAllAmount });
                }
            }
            private void _OnValueChangedByAllAmount(int amount)
            {
                if (type != Type.ByAll) return;
                foreach (ChainItem item in list) item.killCount = byAllAmount;
            }

            private bool _AssetListFilter(GameObject obj)
            {
                return obj.GetComponent<ComponentObstacleDefault>() != null;
            }

            public Progress GetProgress()
            {
                Progress progress = new Progress();
                foreach (ChainItem item in list)
                    progress.required += item.killCount;
                if (type == Type.DeathChain)
                {
                    List<ProcessingTrackManager.KillHistoryItem> killHistory = ProcessingTrackManager.Default.GetKillHistory();
                    int startIndex = killHistory.Count - progress.required;
                    string first = "";
                    string second = "";
                    int j = 0, k = 0;
                    if (startIndex < 0)
                        startIndex = 0;

                    for (int i = startIndex; i < killHistory.Count; i++)
                    {
                        first = killHistory[i].trapGuid;
                        second = list[j].trap.GetComponent<ComponentObstacleDefault>().guid.id;
                        if (first == second && killHistory[i].distance >= minimumDistanceRequired)
                        {
                            progress.current++;
                            k++;
                            if (k % list[j].killCount == 0)
                            {
                                k = 0;
                                j++;
                            }
                        }
                        else
                        {
                            progress.current = 0;
                            k = 0;
                            j = 0;
                        }
                    }
                }
                else
                {
                    int killCount = 0;
                    foreach (ChainItem item in list)
                    {
                        killCount = item.trap.GetComponent<ComponentObstacleDefault>().GetKillCount();
                        if (killCount > item.killCount)
                            killCount = item.killCount;
                        progress.current += killCount;
                    }
                }
                return progress;
            }
        }
        [Serializable]
        public class Notes
        {
            public enum Type { Readed, Gathered }

            [EnumToggleButtons]
            public Type type;
            public bool bySpecificCharacter;

            [ShowIf("bySpecificCharacter"), ValidateInput("_ValidateInputCharacter")]
            public DataCharacter character;

            public bool isConstant;

            [MinValue(0)]
            [ShowIf("isConstant")]
            public int requiredNotes;

            [Range(0, 100)]
            [HideIf("isConstant")]
            public int percentOfAllNotesRequired;

            public Progress GetProgress()
            {
                Progress progress = new Progress();
                if (type == Type.Readed)
                {
                    if (bySpecificCharacter)
                    {

                        progress.current = character.NoteReadedProgress();
                        progress.required = percentOfAllNotesRequired * character.notes.Count / 100;
                    }
                    else
                    {
                        progress.current = DataCharacter.NoteReadedProgressByAll();
                        progress.required = percentOfAllNotesRequired * DataCharacter.NotesCountByAll() / 100;
                    }
                }
                else
                {
                    if (bySpecificCharacter)
                    {
                        progress.current = character.UnlockedNotesCount();
                        progress.required = percentOfAllNotesRequired * character.notes.Count / 100;
                    }
                    else
                    {
                        progress.current = DataCharacter.UnlockedNotesCountByAll();
                        progress.required = percentOfAllNotesRequired * DataCharacter.NotesCountByAll() / 100;
                    }
                }
                if (isConstant)
                    progress.required = requiredNotes;
                return progress;
            }
            private bool _ValidateInputCharacter(DataCharacter character)
            {
                return character != null || !bySpecificCharacter;
            }
        }
        public enum Type { Distance, GodTearsBalance, DieByTraps, Notes }

        [ValidateInput("_ValidateInputId_GooglePlay", "This id NOT found in GPGSids!")]
        public string id_GooglePlay;
        [Required]
        public string localizationNameKey;
        [Required]
        public string localizationDescriptionKey;
        [Required]
        public Sprite icon;
        public int reward;

        public Type type;

        [/*NonSerialized, OdinSerialize,*/ HideReferenceObjectPicker, HideLabel, ShowIf("type", Type.Distance)]
        public Distance distance = new Distance();
        [/*NonSerialized, OdinSerialize,*/ HideReferenceObjectPicker, HideLabel, ShowIf("type", Type.GodTearsBalance)]
        public GodTearsBalance godTearsBalance = new GodTearsBalance();
        [/*NonSerialized, OdinSerialize,*/ HideReferenceObjectPicker, HideLabel, ShowIf("type", Type.DieByTraps)]
        public DieByTraps dieByTraps = new DieByTraps();
        [/*NonSerialized, OdinSerialize,*/ HideReferenceObjectPicker, HideLabel, ShowIf("type", Type.Notes)]
        public Notes notes = new Notes();

        #endregion

        public bool IsUnlocked { get { return ProcessingSaveLoad.Load(TagIsUnlocked, false); } }

        public void CheckProgress()
        {
            if (IsUnlocked)
            {
                UnlockOnCloud();
                return;
            }
            Progress progress = GetProgress();
            if (progress.current >= progress.required)
                Unlock(true);
        }
        public Progress GetProgress()
        {
            if (type == Type.Distance) return distance.GetProgress();
            else if (type == Type.GodTearsBalance) return godTearsBalance.GetProgress();
            else if (type == Type.DieByTraps) return dieByTraps.GetProgress();
            else return notes.GetProgress();
        }
        public void Unlock(bool arg)
        {
            Debug.Log(localizationNameKey + " IsUnlocked: " + IsUnlocked);
            ProcessingSaveLoad.Save(TagIsUnlocked, arg);
            DataGameSession.Default.GodTearsCount += reward;
            UnlockOnCloud();
        }
        public void UnlockOnCloud()
        {
            if (Social.localUser.authenticated)
                Social.ReportProgress(id_GooglePlay, 100.0f, (bool success) => { Debug.Log(localizationNameKey + " Sync With Cloud Success: " + success); });
        }

        private bool _ValidateInputId_GooglePlay(string id)
        {
            if (id == null || id == string.Empty)
                return false;
            return typeof(GPGSIds).GetFields()
                .ToList()
                .Find((value) => { return value.GetValue(value).ToString() == id; }) != null;
        }
    }
}
