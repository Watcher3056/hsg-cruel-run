﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Cinemachine;

namespace HSG
{
    [CreateAssetMenu(menuName = "Data/DataGameSettings", fileName = "DataGameSettings")]
    public class DataGameSettings : DataGame
    {
        public static DataGameSettings Default => _default;
        private static DataGameSettings _default;

        public enum SettingsNames
        {
            ControlsType, PostProcessingPresset, GammaValue,
            ControlsTimingValue, SettingsAvaliable, SoundVolumeMusic, SoundVolumeEffects
        }
        public struct Settings
        {
            public enum ControlsType { Buttons, Touches }
            public ControlsType controlsType;
            public int fpsCount;
            public float controlsTimingValue;
            public float soundVolumeMusic;
            public float soundVolumeEffects;
        }
        [FoldoutGroup("Setup")]
        public AudioClip onPause;
        [FoldoutGroup("Target Resolution")]
        [LabelText("Width for portrait")]
        [ShowInInspector]
        public float portrait_ortographic_width
        {
            get { return Default._portrait_ortographic_width; }
            set { _portrait_ortographic_width = value; Default._portrait_ortographic_width = value; AdaptScreenResolution(); }
        }
        [HideInInspector]
        public float _portrait_ortographic_width;
        [FoldoutGroup("Target Resolution")]
        [LabelText("Portrait offset relative to player")]
        [PropertyRange(-1f, 1f)]
        [ShowInInspector]
        public float portrait_horizontal_offset
        {
            get { return Default._portrait_horizontal_offset; }
            set { _portrait_horizontal_offset = value; Default._portrait_horizontal_offset = value; AdaptScreenResolution(); }
        }
        [HideInInspector]
        public float _portrait_horizontal_offset;
        [FoldoutGroup("Target Resolution")]
        [LabelText("Width for landscape")]
        [ShowInInspector]
        public float landscape_ortographic_width
        {
            get { return Default._landscape_ortographic_width; }
            set { _landscape_ortographic_width = value; Default._landscape_ortographic_width = value; AdaptScreenResolution(); }
        }
        [HideInInspector]
        public float _landscape_ortographic_width;
        [FoldoutGroup("Target Resolution")]
        [LabelText("Landscape offset relative to player")]
        [PropertyRange(-1f, 1f)]
        [ShowInInspector]
        public float landscape_horizontal_offset
        {
            get { return Default._landscape_horizontal_offset; }
            set { _landscape_horizontal_offset = value; Default._landscape_horizontal_offset = value; AdaptScreenResolution(); }
        }
        [HideInInspector]
        public float _landscape_horizontal_offset;

        public Settings settings;
        public DataGameSettings()
        {
            _default = this;
        }
        public void LoadSettings()
        {
            settings.controlsType = ProcessingSaveLoad.Load(Enum.GetName(typeof(SettingsNames), SettingsNames.ControlsType), Settings.ControlsType.Buttons);

            settings.controlsTimingValue = ProcessingSaveLoad.Load(Enum.GetName(typeof(SettingsNames), SettingsNames.ControlsTimingValue), 0.1f);

            LoadAudioSettings();

            ApplySettings(settings);
        }
        public void LoadAudioSettings()
        {
            settings.soundVolumeMusic = ProcessingSaveLoad.Load(Enum.GetName(typeof(SettingsNames), SettingsNames.SoundVolumeMusic), 0f);

            settings.soundVolumeEffects = ProcessingSaveLoad.Load(Enum.GetName(typeof(SettingsNames), SettingsNames.SoundVolumeEffects), 0f);

            ApplyAudioSettings(settings);
        }
        public void SaveSettings(Settings newSettings)
        {
            ProcessingSaveLoad.Save(Enum.GetName(typeof(SettingsNames), SettingsNames.ControlsTimingValue), newSettings.controlsTimingValue);
            ProcessingSaveLoad.Save(Enum.GetName(typeof(SettingsNames), SettingsNames.ControlsType), newSettings.controlsType);
            ProcessingSaveLoad.Save(Enum.GetName(typeof(SettingsNames), SettingsNames.SettingsAvaliable), 1);

            SaveAudioSettings(newSettings);

            LoadSettings();
        }
        public void SaveAudioSettings(Settings newSettings)
        {
            ProcessingSaveLoad.Save(Enum.GetName(typeof(SettingsNames), SettingsNames.SoundVolumeMusic), newSettings.soundVolumeMusic);
            ProcessingSaveLoad.Save(Enum.GetName(typeof(SettingsNames), SettingsNames.SoundVolumeEffects), newSettings.soundVolumeEffects);

            LoadAudioSettings();
        }
        public void AdaptScreenResolution()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying) return;
#endif
            CinemachineVirtualCamera virtualCamera = GameObject.FindObjectOfType<CinemachineVirtualCamera>();
            float ratio = ProcessingGameManager.Default.curScreenHeight / ProcessingGameManager.Default.curScreenWidth;
            float ortSize = 0f;
            Vector3 trackerPosition = new Vector3();
            // Configuring game view camera
            if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical)
            {
                ortSize = ratio * Default.portrait_ortographic_width;
                if (ProcessingTrackManager.Default.Player != null)
                    trackerPosition = new Vector3(ProcessingTrackManager.Default.Player.transform.position.x + Default.portrait_ortographic_width * Default.portrait_horizontal_offset,
                    virtualCamera.Follow.position.y,
                    virtualCamera.Follow.position.z);
            }
            else if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Horizontal)
            {
                ortSize = ratio * Default.landscape_ortographic_width;
                if (ProcessingTrackManager.Default.Player != null)
                    trackerPosition = new Vector3(ProcessingTrackManager.Default.Player.transform.position.x + Default.landscape_ortographic_width * Default.landscape_horizontal_offset,
                    virtualCamera.Follow.position.y,
                    virtualCamera.Follow.position.z);
            }
            if (ProcessingTrackManager.Default.Player != null)
                virtualCamera.Follow.position = trackerPosition;
            virtualCamera.m_Lens.OrthographicSize = ortSize;
        }
        private void ApplySettings(Settings settings)
        {
            ApplyAudioSettings(settings);
        }
        private void ApplyAudioSettings(Settings settings)
        {
            Camera.main.GetComponent<AudioSource>().ignoreListenerVolume = true;
            Camera.main.GetComponent<AudioSource>().volume = settings.soundVolumeMusic;
            AudioListener.volume = settings.soundVolumeEffects;
        }
        // Returns the system architecture
        [Beebyte.Obfuscator.Skip]
        [Beebyte.Obfuscator.DoNotFake]
        public static string GetArchitecture()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            using (var system = new AndroidJavaClass("java.lang.System"))
            {
                return system.CallStatic<string>("getProperty", "os.arch");
            }
#endif
            return "";
        }
    }
}
