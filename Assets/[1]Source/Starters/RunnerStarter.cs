﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using SimpleLocalization;

namespace HSG
{
    public class RunnerStarter : Starter
    {
        [FoldoutGroup("Data Track Manager")]
        [HideLabel]
        public DataTrackManager dataTrackManager;

        protected override void Setup()
        {
            Toolbox.Add<ProcessingAchievementsManager>().Setup();
            Toolbox.Add<ProcessingTutorial>().Setup();
            Toolbox.Add<ProcessingSaveLoad>().Setup();
            Toolbox.Add<ProcessingIAPManager>().Setup();
            Toolbox.Add<ProcessingADSManager>().Setup();
            Toolbox.Add<ProcessingCameraManager>().Setup();
            Toolbox.Add<ProcessingGameManager>().Setup();
            Toolbox.Add<ProcessingTrackManager>().Setup(dataTrackManager);

            StartRun();
        }
        public void StartRun()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                ProcessingTrackManager.Default.CurLocation = DataGameMain.Default.locations[0];
            }, true);
        }
    }
}
/*
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWWWWWWWWP'dP'dWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
WWWWWW"VWWWb """'.o.. .dWWWWWWWWWWWWP~~""_{WWWWWWWWWWWWWWWWWWWWWW
WWWWWWb  """""""w.     jWWWWWWWP"  ,.--"'    .wWWWWWWWWWWWWWWWWWW
WWWWWWWbWWWWF"""""`    WWWWWWP" .-"          {WWWWWWWWWWWWWWWWWWW
WWWWWWWWWWWWLLWP jWWWWP"               .wwWWWWWWWWW WWWWWWWW
WWWWW`W`W`WWWW'      jWWW'                 {WWWWWWWWWWP  `VWWWWWW
WWWWWW, dWWWW'      jWWWW                 .wwWWP".w."WWP.WWWWWWWW
WWWWWW' VWWWW        VWW'               _{WWWW(WWWW.P.WWWWWWWWW
::::::':  :::         "                {  `:::::. '' ,.::::::::::
:::::,:::  ::                                `''''  .::::::::::::
::::::::::                                       ..,:::::::::::::
:::::::::::   .                  ..:.      `:::::::::::::::::::::
:::::::::::: ::   .: ::::::::   :::::::.          `::::::::::::::
::::::::::::::' .::::::::::::.    .::::::::::::::  ::::::::::::::
:::::::::::::'.:::::::::::::::::  ::::::::::::::'  ::::::::::::::
:::::::'.....:.. ::::::::'......:. :::::::::'....::.`::::::::::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
*/