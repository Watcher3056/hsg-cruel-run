﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;

namespace HSG
{
    public partial class ActorPlayer : Actor, ITick, ITriggerTransitionReceiver2D
    {
        [HideLabel]
        public DataPlayer dataPlayer;


        public enum State { None, Live, Dead }

        [HideInInspector]
        public Dictionary<State, StateDefault> statesMap = new Dictionary<State, StateDefault>();

        [HideInInspector]
        public State CurState { get { return curState; } set { curState = value; CheckState(); } }
        [SerializeField]
        private State curState = State.Live;
        [HideInInspector]
        public State PrevState { get; set; }
        [HideInInspector]
        public BehaviorPlayerController.State CurAction { get { return Get<BehaviorPlayerController>().curState; } set { Get<BehaviorPlayerController>().curState = value; } }

        public ActorPlayer()
        {
            SetupLive();
            SetupDead();
        }

        protected override void SetupData()
        {
            Add(dataPlayer);
            Add<BehaviorPlayerController>();
        }

        protected override void SetupBehaviors()
        {

        }

        public void Tick()
        {
            CheckState();
            if (curState == State.None)
                return;
            statesMap[CurState].Handle();
        }

        public void CheckState()
        {
            if (PrevState != CurState)
            {
                Debug.Log(this + " CurState: " + CurState + " PrevState: " + PrevState);
                if (statesMap.ContainsKey(CurState) &&
                    statesMap[CurState].Condition((int)PrevState))
                {
                    if (statesMap.ContainsKey(PrevState))
                        statesMap[PrevState].OnEnd();
                    statesMap[CurState].OnStart();

                    PrevState = CurState;
                }
                else
                    CurState = PrevState;
            }
        }
        public void BreakApart()
        {
            dataPlayer.transformView.gameObject.SetActive(false);
            GODead = this.Populate(Pool.None, dataPlayer.prefabDead, gameObject.transform.position, parent: gameObject.transform).gameObject;
            //ProcessingDeferredOperation.Default.Add(() => {
            //    GameObject.Destroy(GODead);
            //}, true, triggerPeriod: 10f);
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {

        }

        public void OnTriggerExit2D(Collider2D collision)
        {

        }

        //public void OnTriggerStay2D(Collider2D collision)
        //{

        //}

        public void OnCollisionEnter2D(Collision2D collision)
        {

        }

        public void OnCollisionExit2D(Collision2D collision)
        {

        }

        //public void OnCollisionStay2D(Collision2D collision)
        //{

        //}
    }
}
/*
                               ==(W{==========-      /===-
                                ||  (.--.)         /===-_---~~~~~~~----__
                                | \_,|**|,__      |===-~___            _,-'`
                   -==\\        `\ ' `--'   ),    `//~\\   ~~~~`--._.-~
               ______-==|        /`\_. .__/\ \    | |  \\          _-~`
         __--~~~  ,-/-==\\      (   | .  |~~~~|   | |   `\       ,'
      _-~       /'    |  \\     )__/==0==-\<>/   / /      \     /
    .'        /       |   \\      /~\___/~~\/  /' /        \   /
   /  ____  /         |    \`\.__/-~~   \  |_/'  /          \/'
  /-'~    ~~~~~---__  |     ~-/~         ( )   /'        _--~`
                    \_|      /        _) | ;  ),   __--~~
                      '~~--_/      _-~/- |/ \   '-~ \
                     {\__--_/}    / \\_>-|)<__\      \
                     /'   (_/  _-~  | |__>--<__|      |
                    |   _/) )-~     | |__>--<__|      |
                    / /~ ,_/       / /__>---<__/      |
                   o-o _//        /-~_>---<__-~      /
                   (^(~          /~_>---<__-      _-~
                  ,/|           /__>--<__/     _-~
               ,//('(          |__>--<__|     /                  .--_
              ( ( '))          |__>--<__|    |                 /' _-_~\
           `-)) )) (           |__>--<__|    | -Tua Xiong    /'  /   ~\`\
          ,/,'//( (             \__>--<__\    \            /'  //      ||
        ,( ( ((, ))              ~-__>--<_~-_  ~--__---~'/'/  /'       VV
      `~/  )` ) ,/|                 ~-_~>--<_/-__      __-~ _/
    ._-~//( )/ )) `                    ~~-'_/_/ /~~~~~__--~
     ;'( ')/ ,)(                              ~~~~~~~~
    ' ') '( (/
*/