﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;


namespace HSG
{
    public partial class ActorPlayer
    {
        private GameObject GODead;
        private void SetupDead()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StateDeadStart;
            state.OnEnd = StateDeadExit;


            statesMap.Add(State.Dead, state);
        }
        private void StateDeadStart()
        {
            if (ComponentRagdollSwitch.Default != null)
                ComponentRagdollSwitch.Default.Toggle();
            List<Rigidbody2D> rigids = gameObject.GetAllComponents<Rigidbody2D>();
            foreach (Rigidbody2D rigid in rigids)
                rigid.bodyType = RigidbodyType2D.Dynamic;
            dataPlayer.animator.enabled = false;
            GetComponentInChildren<Puppet2D.Puppet2D_GlobalControl>().ControlsEnabled = false;
            //dataPlayer.transformView.gameObject.SetActive(false);
            //GODead = this.Populate(Pool.None, dataPlayer.prefabDead, gameObject.transform.position, parent: gameObject.transform).gameObject;
            ProcessingGameManager.Default.SlowMoveAfterPlayerDead();
        }
        private void StateDeadExit()
        {
            //GameObject.Destroy(GODead);
            //dataPlayer.transformView.gameObject.SetActive(true);
        }
    }
}
