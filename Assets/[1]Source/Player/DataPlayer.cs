﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    [Serializable]
    public class DataPlayer : IData, ISetup
    {
        [FoldoutGroup("Visual")]
        public GameObject prefabDead;
        [FoldoutGroup("Visual")]
        public Transform transformView;
        [FoldoutGroup("Setup")]
        public float speedUpPower;
        [FoldoutGroup("Setup")]
        public Animator animator;

        [ReadOnly]
        [FoldoutGroup("Status")]
        public float curSpeed;


        public void Setup(Actor actor)
        {

        }
        public void Dispose()
        {

        }

    }
}
