﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using HSG;


namespace HSG
{
    public partial class ActorPlayer
    {
        private void SetupLive()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StateLiveStart;
            state.OnEnd = StateLiveEnd;

            statesMap.Add(State.Live, state);
        }
        private void StateLiveStart()
        {
            //Add<BehaviorPlayerController>();
        }
        private void StateLiveEnd()
        {
            //Remove<BehaviorPlayerController>();
        }
    }
}
