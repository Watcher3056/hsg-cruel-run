﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public partial class BehaviorPlayerController : ActorBehavior, ITick
    {
        public enum State { None, Run, RunFast, Glide }
        [Bind] DataPlayer dataPlayer;

        ActorPlayer actorPlayer;

        public State curState;

        bool GlideKeyWasPressed;
        float curGlideKeyTiming;
        Vector3 distance;
        protected override void Setup()
        {
            actorPlayer = actor as ActorPlayer;
        }
        public void Tick()
        {
            if (curState == State.None || actorPlayer.CurState != ActorPlayer.State.Live)
                return;

            distance = Vector3.right * dataPlayer.curSpeed * Homebrew.Time.DeltaTime;
            AnimatorStateInfo stateInfo = dataPlayer.animator.GetCurrentAnimatorStateInfo(0);

            if (stateInfo.IsName("GlideEnter") || stateInfo.IsName("GlideExit"))
                curState = State.Glide;
            else if (stateInfo.IsName("Run"))
                curState = State.Run;
            else if (stateInfo.IsName("RunFast"))
                curState = State.RunFast;

            if (DataGameSettings.Default.settings.controlsType == DataGameSettings.Settings.ControlsType.Buttons)
                ProcessControlsButtons();
            else if (DataGameSettings.Default.settings.controlsType == DataGameSettings.Settings.ControlsType.Touches)
                ProcessControlsTouches();

            stateInfo = dataPlayer.animator.GetCurrentAnimatorStateInfo(0);

            if (stateInfo.IsName("GlideEnter") || stateInfo.IsName("GlideExit"))
                curState = State.Glide;
            else if (stateInfo.IsName("Run"))
                curState = State.Run;
            else if (stateInfo.IsName("RunFast"))
                curState = State.RunFast;

            CorrectAnimationSpeed();
            actor.transform.Translate(distance);
        }
        private void CorrectAnimationSpeed()
        {
            if (curState != State.Glide)
                dataPlayer.animator.speed = 1f;
            else
                dataPlayer.animator.speed = dataPlayer.curSpeed * 0.7f;
        }
        private void ProcessControlsButtons()
        {
            if (ComponentControls.CurAction == ComponentControls.Action.Glide && curState != State.Glide)
            {
                dataPlayer.animator.SetTrigger("Glide");
            }
            else if (ComponentControls.CurAction == ComponentControls.Action.RunFast && curState != State.Glide)
            {
                distance *= dataPlayer.speedUpPower;
               dataPlayer.animator.SetBool("RunFast", true);
            }
            else
               dataPlayer.animator.SetBool("RunFast", false);
        }
        private void ProcessControlsTouches()
        {
            curGlideKeyTiming -= UnityEngine.Time.deltaTime;

            if (curGlideKeyTiming <= 0) GlideKeyWasPressed = false;

           dataPlayer.animator.SetBool("RunFast", false);

            if (ComponentControls.IsTouchStart && curState != State.Glide)
            {
                GlideKeyWasPressed = true;
                curGlideKeyTiming = DataGameSettings.Default.settings.controlsTimingValue;
            }
            else if (ComponentControls.IsTouchEnd && GlideKeyWasPressed)
            {
               dataPlayer.animator.SetTrigger("Glide");
                GlideKeyWasPressed = false;
            }
            else if (ComponentControls.IsTouchNow && curState != State.Glide && !GlideKeyWasPressed)
            {
                distance *= dataPlayer.speedUpPower;
               dataPlayer.animator.SetBool("RunFast", true);
            }

            if (ComponentControls.IsTouchEnd)
            {
               dataPlayer.animator.SetBool("RunFast", false);
            }
        }
        protected override void OnDispose()
        {
            dataPlayer = null;
            actor = null;
        }
    }
}
