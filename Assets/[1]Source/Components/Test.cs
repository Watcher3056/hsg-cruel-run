﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace HSG
{
    public class Test : MonoCached
    {
        [Button]
        public void TestMigration()
        {   
            ProcessingSaveLoad.TestMigration();
        }
        [Button]
        public void TestSerialization()
        {
            ProcessingSaveLoad.TestSerialization();
        }
        [Button]
        public void TestSyncronization()
        {
            ProcessingSaveLoad.TestSyncronization();
        }
    }
}
