﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using Beebyte.Obfuscator;
using SimpleLocalization;
using TMPro;

namespace HSG
{
    public class ComponentNotesMenu : MonoCached
    {
        public static ComponentNotesMenu Default => _default;
        private static ComponentNotesMenu _default;

        [FoldoutGroup("Setup")]
        public GameObject menuHolder;
        [FoldoutGroup("Setup")]
        public GameObject notesExplorerHolder;
        [FoldoutGroup("Setup")]
        public GameObject comicsExplorerHolder;
        [FoldoutGroup("Setup")]
        public Image comicsImage;
        [FoldoutGroup("Setup")]
        public Button buttonOpenComics;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI notesExplorerText;
        [FoldoutGroup("Setup")]
        public Button noteButtonTemplate;
        [FoldoutGroup("Setup")]
        public Transform noteButtonHolder;

        [FoldoutGroup("Status")]
        [ReadOnly]
        [ShowInInspector]
        private static int curNotesNumber;
        [FoldoutGroup("Status")]
        [ReadOnly]
        [ShowInInspector]
        private static int curComicsNumber;
        [FoldoutGroup("Status")]
        [ReadOnly]
        [ShowInInspector]
        private bool comicsZoomed;
        [FoldoutGroup("Status")]
        [ReadOnly]
        [ShowInInspector]
        private List<ComponentNoteButton> noteButtons;

        private static bool menuNowOpened, notesExplorerNowOpened, comicsExplorerNowOpened;

        protected override void OnAwake()
        {
            _default = this;

            noteButtons = new List<ComponentNoteButton>();

            for (int i = 0; i < noteButtonHolder.childCount; i++)
                noteButtonHolder.GetChild(i).GetComponent<ComponentNoteButton>().HandleDestroyGO();

            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                notesExplorerHolder.GetComponent<RectTransform>().localPosition = Vector3.zero;
                comicsExplorerHolder.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);

            ButtonToggleMenu(menuNowOpened);
            if (notesExplorerNowOpened) ButtonShowNote(curNotesNumber);
            else ButtonCloseExplorer();
            ButtonToggleComics(comicsExplorerNowOpened);
        }
        [SkipRename]
        public void ButtonToggleComics(bool arg)
        {
            if (arg)
                ProcessingTrackManager.Default.CurCharacter.ComicsIsReaded = true;
            comicsExplorerHolder.SetActive(arg);
            comicsExplorerNowOpened = arg;
            comicsImage.sprite =
                LocalizationManager.Language == "Russian" ?
                ProcessingTrackManager.Default.CurCharacter.comics[0].ru :
                ProcessingTrackManager.Default.CurCharacter.comics[0].en;
            ProcessingTutorial.Default.CheckNotifications();
        }
        [SkipRename]
        public void ButtonScrollComics()
        {
            curComicsNumber++;
            if (curComicsNumber >= ProcessingTrackManager.Default.CurCharacter.comics.Count)
                curComicsNumber = 0;
            comicsImage.sprite =
                LocalizationManager.Language == "Russian" ?
                ProcessingTrackManager.Default.CurCharacter.comics[curComicsNumber].ru :
                ProcessingTrackManager.Default.CurCharacter.comics[curComicsNumber].en;
        }
        [SkipRename]
        public void ComicsZoom()
        {
            if (comicsZoomed)
            {
                comicsImage.gameObject.transform.localScale = new Vector3(1f, 1f);
                comicsZoomed = false;
            }
            else
            {
                comicsImage.gameObject.transform.localScale = new Vector3(2f, 2f);
                comicsZoomed = true;
            }
        }
        [SkipRename]
        public void ButtonShowNote(int noteNumber)
        {
            ProcessingTrackManager.Default.CurCharacter.SetNoteReaded(noteNumber, true);

            curNotesNumber = noteNumber;
            notesExplorerHolder.SetActive(true);
            notesExplorerNowOpened = true;
            notesExplorerText.text = LocalizationManager.Localize(ProcessingTrackManager.Default.CurCharacter.notes[noteNumber]);
            noteButtons[noteNumber].SetReaded(true);
            buttonOpenComics.interactable = AllNotesReaded();
            ProcessingTutorial.Default.CheckNotifications();
        }
        [SkipRename]
        public void ButtonCloseExplorer()
        {
            notesExplorerHolder.SetActive(false);
            notesExplorerNowOpened = false;
        }
        [SkipRename]
        public void ButtonScrollNotesRight()
        {
            curNotesNumber++;
            if (curNotesNumber >= ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount())
                curNotesNumber = 0;
            ButtonShowNote(curNotesNumber);
        }
        [SkipRename]
        public void ButtonScrollNotesLeft()
        {
            curNotesNumber--;
            if (curNotesNumber < 0)
                curNotesNumber = ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount() - 1;
            ButtonShowNote(curNotesNumber);
        }
        [SkipRename]
        public void ButtonToggleMenu(bool arg)
        {
            if (!arg)
            {

                noteButtons.Clear();
                menuHolder.SetActive(false);
                menuNowOpened = false;

                while (noteButtonHolder.childCount != 0)
                {
                    GameObject.Destroy(noteButtonHolder.GetChild(0).gameObject);
                    noteButtonHolder.GetChild(0).SetParent(null);
                }
                return;
            }

            buttonOpenComics.interactable = AllNotesReaded();

            for (int i = 0; i < ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount(); i++)
            {
                int j = i;
                noteButtons.Add(this.Populate(Pool.None, noteButtonTemplate.gameObject, parent: noteButtonHolder).GetComponent<ComponentNoteButton>());
                noteButtons[i].button.onClick.AddListener(() => { ButtonShowNote(j); });
                noteButtons[i].SetReaded(ProcessingTrackManager.Default.CurCharacter.GetNoteReaded(i));
            }
            for (int i = ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount(); i < ProcessingTrackManager.Default.CurCharacter.notes.Count; i++)
            {
                noteButtons.Add(this.Populate(Pool.None, noteButtonTemplate.gameObject, parent: noteButtonHolder).GetComponent<ComponentNoteButton>());
                noteButtons[i].button.interactable = false;
                noteButtons[i].SetReaded(true);
            }
            menuHolder.SetActive(true);
            menuNowOpened = true;
        }

        private bool AllNotesReaded()
        {
            for (int i = 0; i < ProcessingTrackManager.Default.CurCharacter.notes.Count; i++)
                if (!ProcessingTrackManager.Default.CurCharacter.GetNoteReaded(i))
                {
                    return false;
                }
            return true;
        }

        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
    }
}
