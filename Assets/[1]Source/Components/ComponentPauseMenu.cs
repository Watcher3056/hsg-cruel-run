﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using AppodealAds.Unity.Api;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentPauseMenu : MonoCached, ITick
    {
        public static ComponentPauseMenu Default => _default;
        private static ComponentPauseMenu _default;


        [FoldoutGroup("Setup")]
        public GameObject goHint;
        [FoldoutGroup("Setup")]
        public GameObject PanelWatchVideo;
        [FoldoutGroup("Setup")]
        public Button buttonSkins;
        [FoldoutGroup("Setup")]
        public Button buttonSubMenu;
        [FoldoutGroup("Setup")]
        public Button buttonCharacters;
        [FoldoutGroup("Setup")]
        public Button buttonLocations;
        [FoldoutGroup("Setup")]
        public Button buttonNotes;
        [FoldoutGroup("Setup")]
        public Button buttonAchivements;
        [FoldoutGroup("Setup")]
        public Button buttonDismissVideo;
        [FoldoutGroup("Setup")]
        public Button buttonWatchVideo;
        [FoldoutGroup("Setup")]
        public Button buttonDashBoard;
        [FoldoutGroup("Setup")]
        public GameObject buttonStartGame;
        [FoldoutGroup("Setup")]
        public Image pauseButton;
        [FoldoutGroup("Setup")]
        public Sprite pauseImage;
        [FoldoutGroup("Setup")]
        public Sprite playImage;
        [FoldoutGroup("Setup")]
        public Sprite exitImage;
        [FoldoutGroup("Setup")]
        public float timing;
        [FoldoutGroup("Setup")]
        public GameObject loadingGO;
        [FoldoutGroup("Setup")]
        public GameObject activeOnMainMenu;
        [FoldoutGroup("Setup")]
        public GameObject activeOnPause;

        private bool isPause;
        private float curTiming;

        public ComponentPauseMenu()
        {
            _default = this;
        }

        protected override void HandleEnable()
        {
            activeOnPause.GetComponent<RectTransform>().localPosition = Vector3.zero;
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Destroy(loadingGO);
            }, true, triggerPeriod: 1f);
            ProcessingGameManager.OnPlay += HandleOnPlay;
            //ProcessingGameManager.OnPause += HandleOnPause;
            ProcessingGameManager.OnPlayerDead += HandleOnPlayerDead;
            ProcessingGameManager.OnWaitingForStart += HandleOnWaitingForStart;
        }
        protected override void OnBeforeDestroy()
        {
            ProcessingGameManager.OnPlay += HandleOnPlay;
            //ProcessingGameManager.OnPause -= HandleOnPause;
            ProcessingGameManager.OnPlayerDead -= HandleOnPlayerDead;
            ProcessingGameManager.OnWaitingForStart -= HandleOnWaitingForStart;
        }

        public void Tick()
        {
            curTiming -= UnityEngine.Time.deltaTime;
            if (Application.platform != RuntimePlatform.WindowsEditor)
                buttonWatchVideo.interactable = Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
        }
        private void HandleOnPlay()
        {
            activeOnPause.SetActive(false);
            SetMenuVisible(false);
            buttonStartGame.SetActive(false);
            PanelWatchVideo.gameObject.SetActive(false);

            pauseButton.sprite = pauseImage;
            isPause = false;
        }
        private void HandleOnWaitingForStart()
        {
            activeOnPause.SetActive(false);
            SetMenuVisible(true);
            buttonStartGame.SetActive(true);
            PanelWatchVideo.SetActive(false);

            pauseButton.sprite = exitImage;
            isPause = false;
        }
        private void HandleOnPlayerDead()
        {
            activeOnPause.SetActive(false);
            SetMenuVisible(true);
            buttonStartGame.SetActive(false);
            PanelWatchVideo.gameObject.SetActive(true);

            pauseButton.sprite = exitImage;
            isPause = false;
        }
        [SkipRename]
        public void ToggleButton()
        {
            if (ProcessingGameManager.Default.CurState != ProcessingGameManager.GameState.Pause &&
                ProcessingGameManager.Default.CurState != ProcessingGameManager.GameState.Play)
            {
                Debug.Log("EXIT!");
                Application.Quit();
                return;
            }
            else if (curTiming > 0)
                return;
            else if (isPause)
            {
                activeOnPause.SetActive(false);

                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Play;

                curTiming = timing;
                pauseButton.sprite = pauseImage;
                isPause = false;
            }
            else
            {
                activeOnPause.SetActive(true);

                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Pause;

                pauseButton.sprite = playImage;
                isPause = true;
            }
        }
        [SkipRename]
        public void SetMenuVisible(bool arg)
        {
            activeOnMainMenu.SetActive(arg);
        }
        [SkipRename]
        public void SetActiveDashBoard(bool arg)
        {
            buttonDashBoard.interactable = arg;
        }
        [SkipRename]
        public void ButtonOpenDashBoard()
        {
            ProcessingGameManager.Default.CheckGPGS();
            Social.ShowLeaderboardUI();
        }
        [SkipRename]
        public void ButtonStartGame()
        {
            ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Play;
        }
        [SkipRename]
        public void ButtonDismissVideo()
        {
            if (ProcessingADSManager.Default.lastAdShownTime + new TimeSpan(0, 0, ProcessingADSManager.showAdEver) < DateTime.Now &&
                ProcessingGameManager.Default.CurState == ProcessingGameManager.GameState.PlayerDead &&
                Appodeal.isLoaded(Appodeal.INTERSTITIAL) &&
                ProcessingTrackManager.Default.curDistanceTraveled > ProcessingTrackManager.Default.dataTrackManager.distanceWithoutAds)
                ProcessingADSManager.Default.ShowInterstitial(() => { ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.WaitingForStart; });
            else
                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.WaitingForStart;
        }
        [SkipRename]
        public void ButtonWatchVideo()
        {
#if !UNITY_EDITOR
            if (Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
#endif
            ProcessingGameManager.Default.TryRestartForRewardedVideo(true);
        }
        [SkipRename]
        public void ButtonUpdateOrientation()
        {
            ProcessingCameraManager.Default.ChangeOrientation();
        }
        [SkipRename]
        public void ButtonMainMenu()
        {
            ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.WaitingForStart;
        }
        [SkipRename]
        public void SetActiveChangeCharacterMenu(bool arg)
        {
            ComponentChangeCharacterMenu.Default.SetActiveMenu(arg);
        }
        [SkipRename]
        public void SetActiveChangeLocationMenu(bool arg)
        {
            ComponentChangeLocationMenu.Default.SetActiveMenu(arg);
        }
        [SkipRename]
        public void SetActiveNotesMenu(bool arg)
        {
            ComponentNotesMenu.Default.ButtonToggleMenu(arg);
        }
        [SkipRename]
        public void SetActiveSettingsMenu(bool arg)
        {
            ComponentSettingsMenu.Default.ButtonSetActiveWindow(arg);
        }
        [SkipRename]
        public void SetActiveShopMenu(bool arg)
        {
            ComponentShopMenu.Default.ToggleWindow(arg);
        }
        [SkipRename]
        public void SetActiveAchievementsMenu(bool arg)
        {
            ComponentAchievementsMenu.Default.ButtonToggleMenu(arg);
        }
    }
}
