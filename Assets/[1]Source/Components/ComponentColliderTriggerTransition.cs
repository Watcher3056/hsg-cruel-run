﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace HSG
{
    public class ComponentColliderTriggerTransition : MonoCached, ITriggerTransitionReceiver2D
    {
        [InfoBox("Input validation is DISABLED. Be careful!", InfoMessageType = InfoMessageType.Warning, VisibleIf = "ignoreValidate")]
        public bool ignoreValidate;
        [ValidateInput("ValidateInput", "Wrong MonoBehavior. Require MonoBehavior that realize ITriggerTransitionReceiver interface!")]
        public MonoBehaviour transitionTo;
        private ITriggerTransitionReceiver2D _transitionTo;
        public bool playSoundOnEnter;
        [ShowIf("playSoundOnEnter")]
        //[Required]
        public AudioSource audioSource;
        [ShowIf("playSoundOnEnter")]
        //[Required]
        public AudioClip audioClip;

        private bool soundWasPlayed;
        protected override void OnAwake()
        {
            ValidateInput(transitionTo);
        }

        public MonoBehaviour GetRoot()
        {
            if (transitionTo is ComponentColliderTriggerTransition)
                return (transitionTo as ComponentColliderTriggerTransition).GetRoot();
            else
                return transitionTo;
        }
        private bool ValidateInput(MonoBehaviour transitionTo)
        {
            if (transitionTo is ITriggerTransitionReceiver2D)
                _transitionTo = transitionTo as ITriggerTransitionReceiver2D;
            else if (!ignoreValidate)
                return false;

            return true;
        }

        private void OnEnter()
        {
            if (playSoundOnEnter && !soundWasPlayed)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
                soundWasPlayed = true;
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnEnter();
            if (_transitionTo != null)
                _transitionTo.OnTriggerEnter2D(collision);
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (_transitionTo != null)
                _transitionTo.OnTriggerExit2D(collision);
        }
        /*
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (_transitionTo != null)
                _transitionTo.OnTriggerStay2D(collision);
        }*/
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (_transitionTo != null)
                _transitionTo.OnCollisionEnter2D(collision);
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (_transitionTo != null)
                _transitionTo.OnCollisionExit2D(collision);
        }
        /*
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (_transitionTo != null)
                _transitionTo.OnCollisionStay2D(collision);
        }*/
        void ITriggerTransitionReceiver2D.OnTriggerEnter2D(Collider2D collision)
        {
            OnTriggerEnter2D(collision);
        }

        void ITriggerTransitionReceiver2D.OnTriggerExit2D(Collider2D collision)
        {
            OnTriggerExit2D(collision);
        }
        /*
        void ITriggerTransitionReceiver2D.OnTriggerStay2D(Collider2D collision)
        {
            OnTriggerStay2D(collision);
        }
        */
        void ITriggerTransitionReceiver2D.OnCollisionEnter2D(Collision2D collision)
        {
            OnCollisionEnter2D(collision);
        }

        void ITriggerTransitionReceiver2D.OnCollisionExit2D(Collision2D collision)
        {
            OnCollisionExit2D(collision);
        }
        /*
        void ITriggerTransitionReceiver2D.OnCollisionStay2D(Collision2D collision)
        {
            OnCollisionStay2D(collision);
        }*/
    }
}
