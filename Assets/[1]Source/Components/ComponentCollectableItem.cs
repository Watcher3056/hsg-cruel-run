﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;


namespace HSG
{
    public class ComponentCollectableItem : MonoCached, ITriggerTransitionReceiver2D
    {
        public enum ItemType { GodTears, Coins, Note }

        [EnumToggleButtons]
        public ItemType itemType;
        public bool playSoundOnTake;
        [ShowIf("playSoundOnTake")]
        [PropertyRange(0f, 1f)]
        public float volume;
        [ShowIf("playSoundOnTake")]
        [ListDrawerSettings(ShowIndexLabels = false, NumberOfItemsPerPage = 10)]
        public List<AudioClip> possibleClips;

        public static int notesSpawnedNow, godTearsSpawnedNow;
        public static Transform transformLastTear;

        protected bool collected;
        static ComponentCollectableItem()
        {
            notesSpawnedNow = 0;
            godTearsSpawnedNow = 0;
        }
        protected override void HandleEnable()
        {
            if (itemType == ItemType.Note)
                notesSpawnedNow++;
            else if (itemType == ItemType.GodTears)
            {
                godTearsSpawnedNow++;
                transformLastTear = transform;
            }
        }
        protected override void OnBeforeDestroy()
        {
            if (itemType == ItemType.GodTears)
                godTearsSpawnedNow--;
            else if (itemType == ItemType.Note)
                notesSpawnedNow--;
        }
        public void PickUpThis()
        {
            if (collected) return;
            collected = true;
            if (itemType == ItemType.GodTears)
            {
                DataGameSession.Default.GodTearsCount++;
                ProcessingDeferredOperation.Default.Add(() => { DataGameSession.Default.GodTearsTotalCollected++; }, true);
            }
            else if (itemType == ItemType.Note)
                ProcessingTrackManager.Default.CurCharacter.UnlockNewNote();
            if (playSoundOnTake)
                ComponentSoundManager.Default.PlaySound(possibleClips[UnityEngine.Random.Range(0, possibleClips.Count)], volume);
            HandleDestroyGO();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            PickUpThis();
        }

        public void OnTriggerExit2D(Collider2D collision)
        {

        }

        public void OnTriggerStay2D(Collider2D collision)
        {

        }

        public void OnCollisionEnter2D(Collision2D collision)
        {

        }

        public void OnCollisionExit2D(Collision2D collision)
        {

        }

        public void OnCollisionStay2D(Collision2D collision)
        {

        }
    }
}
