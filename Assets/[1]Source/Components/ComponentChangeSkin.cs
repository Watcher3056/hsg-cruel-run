﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;


namespace HSG
{
    public class ComponentChangeSkin : MonoCached, ITick
    {
        [FoldoutGroup("Setup")]
        public GameObject view;
        [FoldoutGroup("Setup")]
        public GameObject buttonPrefab;
        [FoldoutGroup("Setup")]
        public Transform contentHolder;
        [FoldoutGroup("Setup")]
        public Transform marker;


        public int curIndex;
        private List<ComponentSkinButton> buttons;
        private DataCharacter prevCharacter;
        private bool indexChanged;
        protected override void OnAwake()
        {
            buttons = new List<ComponentSkinButton>();
            for (int i = 0; i < contentHolder.childCount; i++)
                buttons.Add(contentHolder.GetChild(i).GetComponent<ComponentSkinButton>());
            view.SetActive(false);
        }

        public void Tick()
        {
            if (!view.activeSelf) return;
            if (prevCharacter != ProcessingTrackManager.Default.CurCharacter)
            {
                curIndex = 0;
                UpdateView();
                prevCharacter = ProcessingTrackManager.Default.CurCharacter;
            }
            if (indexChanged)
            {
                indexChanged = false;
                return;
            }
            UpdateMarker();
        }
        [SkipRename]
        public void ButtonScrollRight()
        {
            indexChanged = true;
            curIndex++;
            if (curIndex > ProcessingTrackManager.Default.CurCharacter.skins.Count - 1)
                curIndex = 0;
            UpdateView();
        }
        [SkipRename]
        public void ButtonScrollLeft()
        {
            indexChanged = true;
            curIndex--;
            if (curIndex < 0)
                curIndex = ProcessingTrackManager.Default.CurCharacter.skins.Count - 1;
            UpdateView();
        }
        [SkipRename]
        public void ToggleActive()
        {
            indexChanged = true;
            view.SetActive(!view.activeSelf);
            UpdateView();
        }
        private void UpdateMarker()
        {
            int sibIndex = marker.GetSiblingIndex();
            Transform prevParent = marker.parent;
            marker.SetParent(buttons[curIndex].transform);
            marker.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            marker.SetParent(prevParent);
            marker.SetSiblingIndex(sibIndex);
        }
        private void UpdateView()
        {
            ComponentSkinButton button = null;
            while (buttons.Count > 0)
            {
                buttons[0].HandleDestroyGO();
                buttons.RemoveAt(0);
            }
            for (int i = 0; i < ProcessingTrackManager.Default.CurCharacter.skins.Count; i++)
            {
                button = this.
                    Populate(Pool.None, buttonPrefab, parent: contentHolder).GetComponent<ComponentSkinButton>();
                button.imagePreview.sprite = ProcessingTrackManager.Default.CurCharacter.skins[i].previewImage;
                button.imagePreview.color =
                    ProcessingTrackManager.Default.CurCharacter.skins[i].IsUnlocked ? Color.white : Color.black;
                if (ProcessingTrackManager.Default.CurCharacter.skins[i].IsUnlocked)
                {
                    int j = i;
                    button.button.onClick.AddListener(() => { curIndex = j; UpdateView(); });
                }
                buttons.Add(button);
            }
            if (!ProcessingTrackManager.Default.CurCharacter.skins[curIndex].IsUnlocked)
                curIndex = 0;
            ProcessingTrackManager.Default.CurSkinIndex = curIndex;
        }
    }
}
