﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;

namespace HSG
{
    [RequireComponent(typeof(ComponentGUID))]
    public class ComponentObstacleDefault : ComponentObstacle, ITriggerTransitionReceiver2D, ITick
    {
        #region TagSaves
        private string TagKillCount { get { return guid.id + ".KillCount"; } }
        #endregion
        #region Settings
        [FoldoutGroup("Setup")]
        [ValidateInput("_ValidateInput", "Require GUID component!")]
        public ComponentGUID guid;
        private bool _ValidateInput(ComponentGUID guid)
        {
            if (this.guid == null) this.guid = GetComponent<ComponentGUID>();
            return this.guid != null;
        }

        public enum KillEffect { Default, CutLings, Break }
        [FoldoutGroup("Setup"), EnumToggleButtons]
        public KillEffect killEffect;
        [FoldoutGroup("Setup"), EnumToggleButtons]
        public ComponentUIManager.ScreenDeathEffect.Type screenDeathEffect;
        [FoldoutGroup("Setup")]
        public bool isSafe;
        [FoldoutGroup("Setup")]
        [MinValue(0)]
        public int maxTraps;
        [FoldoutGroup("Setup"), Required]
        public Renderer viewChecker;
        [FoldoutGroup("Setup"), Required]
        public Transform leftBorder;
        [FoldoutGroup("Setup"), Required]
        public Transform rightBorder;
        [FoldoutGroup("Setup")]
        public BehaviorPlayerController.State requireState;
        [FoldoutGroup("Setup")]
        public bool activeOnVisible;
        [FoldoutGroup("Setup")]
        public bool playSoundOnEnter;
        [FoldoutGroup("Setup")]
        [ShowIf("playSoundOnEnter")]
        //[Required]
        [FoldoutGroup("Setup")]
        public AudioSource audioSource;
        [FoldoutGroup("Setup")]
        [ShowIf("playSoundOnEnter")]
        //[Required]
        public AudioClip audioClip;


        [FoldoutGroup("Status")]
        [ShowInInspector]
        [ReadOnly]
        public bool isVisible;
        [FoldoutGroup("Status")]
        [ShowInInspector]
        [ReadOnly]
        private bool soundWasPlayed;
        [FoldoutGroup("Status")]
        [ShowInInspector]
        [ReadOnly]
        private List<AudioSource> audioSources;
        [FoldoutGroup("Status")]
        [ShowInInspector]
        [ReadOnly]
        private List<Animator> animators;
        private bool playerKillTriggered;

        public enum ComboObstacleChance { VeryLow, Low, Medium, High }
        public static Dictionary<ComboObstacleChance, int> dictionaryComboObstacleChance;

        #endregion Settings
        static ComponentObstacleDefault()
        {
            dictionaryComboObstacleChance = new Dictionary<ComboObstacleChance, int>();
            dictionaryComboObstacleChance.Add(ComboObstacleChance.VeryLow, 1);
            dictionaryComboObstacleChance.Add(ComboObstacleChance.Low, 2);
            dictionaryComboObstacleChance.Add(ComboObstacleChance.Medium, 3);
            dictionaryComboObstacleChance.Add(ComboObstacleChance.High, 4);
        }

        protected override void HandleEnable()
        {
            audioSources = new List<AudioSource>();
            audioSources.Add(audioSource);

            animators = gameObject.GetAllComponents<Animator>();
            if (activeOnVisible)
                for (int i = 0; i < animators.Count; i++)
                    animators[i].enabled = false;
        }
        protected override void OnBeforeDestroy()
        {
            foreach (AudioSource item in audioSources)
                item.clip = null;
        }
        public void Tick()
        {
            CheckVisible();
            CorrectAnimationSpeed();
            TickSound();

            if (!activeOnVisible || !isVisible)
                return;
            GetComponent<Animator>().enabled = true;
        }

        private void CheckVisible()
        {
            isVisible = ProcessingCameraManager.Default.IsObjectVisible(Camera.main, viewChecker);
        }
        private void CorrectAnimationSpeed()
        {
            DataGameSettings dgs = DataGameSettings.Default;
            for (int i = 0; i < animators.Count; i++)
            {
                if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Horizontal && activeOnVisible)
                    animators[i].speed = ProcessingGameManager.Default.CurGlobalAnimSpeed /
                        ((dgs.landscape_ortographic_width * dgs.landscape_horizontal_offset + dgs.landscape_ortographic_width) / (dgs.portrait_ortographic_width * dgs.portrait_horizontal_offset + dgs.portrait_ortographic_width)) * 0.9f;
                else
                    animators[i].speed = ProcessingGameManager.Default.CurGlobalAnimSpeed;
            }
        }
        public void OnTriggerEnter2D(Collider2D collision)
        {
            ComponentColliderTriggerTransition temp = collision.GetComponent<ComponentColliderTriggerTransition>();
            if (temp == null)
                return;
            ActorPlayer player = temp.GetRoot() as ActorPlayer;
            if ((player == null || player.CurAction != requireState) && requireState != BehaviorPlayerController.State.None)
                return;
            KillPlayer();
        }
        public void OnCollisionEnter2D(Collision2D collision)
        {
            //if (killEffect == KillEffect.CutLings)
            //    collision.gameObject.GetComponent<HingeJoint2D>().enabled = false;
            OnTriggerEnter2D(collision.collider);
            collision.rigidbody
                .AddRelativeForce(
                (collision.contacts[0].point -
                new Vector2(collision.otherCollider.transform.position.x,
                collision.otherCollider.transform.position.y))
                .normalized * 40, ForceMode2D.Impulse);
        }
        public void KillPlayer()
        {
            if (playerKillTriggered) return;
            playerKillTriggered = true;

            PlaySoundOnEnter();

            ProcessingTrackManager.Default.Player.CurState = ActorPlayer.State.Dead;
            ProcessingTrackManager.Default.AddToKillHistory(guid.id);
            IncrementKillCount();
            if (killEffect == KillEffect.Break)
                ProcessingTrackManager.Default.Player.BreakApart();
            if (screenDeathEffect != ComponentUIManager.ScreenDeathEffect.Type.None)
                ComponentUIManager.Default.AddRandomEffect(screenDeathEffect);
        }
        #region Hide Empty

        public void OnTriggerExit2D(Collider2D collision)
        {

        }

        public void OnTriggerStay2D(Collider2D collision)
        {

        }

        public void OnCollisionExit2D(Collision2D collision)
        {

        }

        public void OnCollisionStay2D(Collision2D collision)
        {

        }
        #endregion Hide Empty
        private void PlaySoundOnEnter()
        {
            if (playSoundOnEnter && !soundWasPlayed)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
                soundWasPlayed = true;
            }
        }
        private void TickSound()
        {
            for (int i = 0; i < audioSources.Count; i++)
            {
                if (audioSources[i] == null)
                {
                    audioSources.RemoveAt(i);
                    i--;
                }
                else
                {
                    audioSources[i].volume = isVisible ? 1f : 0f;
                }
            }
        }
        [SkipRename]
        private void PlaySoundAsync(AudioClip audioClip)
        {
            AudioSource temp = ComponentSoundManager.Default.PlaySound(audioClip, 0f);
            audioSources.Add(temp);
            Timer.Add(audioClip.length, () => { audioSources.Remove(temp); });
        }
        private void IncrementKillCount()
        {
            ProcessingSaveLoad.Save(TagKillCount, GetKillCount() + 1);
            Debug.Log(TagKillCount + ": " + GetKillCount());
        }
        public int GetKillCount()
        {
            return ProcessingSaveLoad.Load(TagKillCount, 0);
        }
    }
}
