﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Beebyte.Obfuscator;
using static HSG.DataGameSettings.Settings;

namespace HSG
{
    public class ComponentControls : MonoCached, ITick, ITickLate
    {
        public static ComponentControls Default => _default;
        private static ComponentControls _default;


        [FoldoutGroup("Setup")]
        public Transform mainHolder;
        [FoldoutGroup("Setup")]
        public GameObject buttonRun;
        [FoldoutGroup("Setup")]
        public GameObject buttonGlide;
        [FoldoutGroup("Setup")]
        public Image swipeObject;
        [FoldoutGroup("Setup")]
        public bool detectSwipeOnlyAfterRelease = false;
        [FoldoutGroup("Setup")]
        public float minDistanceForSwipe = 20f;

        public ControlsType ControlsType
        {
            get { return controlsType; }
            set
            {
                controlsType = value;
                buttonRun.SetActive(controlsType == ControlsType.Buttons);
                buttonGlide.SetActive(controlsType == ControlsType.Buttons);
                swipeObject.gameObject.SetActive(controlsType == ControlsType.Touches);
            }
        }
        private ControlsType controlsType;
        public static bool IsTouchStart => isTouchStart;
        private static bool isTouchStart;

        public static bool IsTouchNow => isTouchNow;
        private static bool isTouchNow;

        public static bool IsTouchEnd => isTouchEnd;
        private static bool isTouchEnd;

        private Vector2 fingerDownPosition;
        private Vector2 fingerUpPosition;

        public static event Action<SwipeData> OnSwipe = delegate { };
        public static SwipeData lastSwipe;

        public enum Action { None, RunFast, Glide }
        public static Action CurAction => curAction;
        private static Action curAction;

        public ComponentControls()
        {
            _default = this;
        }

        protected override void OnAwake()
        {
            ToggleWindow(ProcessingGameManager.Default.CurState == ProcessingGameManager.GameState.Play);
            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);
        }
        [SkipRename]
        public void ToggleWindow(bool arg)
        {
            curAction = Action.None;
            mainHolder.gameObject.SetActive(arg);
        }
        [SkipRename]
        public void ButtonRunFast()
        {
            curAction = Action.RunFast;
        }
        [SkipRename]
        public void ButtonGlide()
        {
            curAction = Action.Glide;
        }
        [SkipRename]
        public void UnPressed()
        {
            curAction = Action.None;
        }
        public void OnPointerDown()
        {
            isTouchNow = true;
            isTouchStart = true;
        }

        public void OnPointerUp()
        {
            isTouchNow = false;
            isTouchEnd = true;
        }

        public void Tick()
        {
            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    fingerUpPosition = touch.position;
                    fingerDownPosition = touch.position;
                }

                if (!detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved)
                {
                    fingerDownPosition = touch.position;
                    DetectSwipe();
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    fingerDownPosition = touch.position;
                    DetectSwipe();
                }
            }
        }
        public void TickLate()
        {
            isTouchStart = false;
            isTouchEnd = false;
            lastSwipe = default(SwipeData);
        }
        private void DetectSwipe()
        {
            if (SwipeDistanceCheckMet())
            {
                if (IsVerticalSwipe())
                {
                    var direction = fingerDownPosition.y - fingerUpPosition.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                    SendSwipe(direction);
                }
                else
                {
                    var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                    SendSwipe(direction);
                }
                fingerUpPosition = fingerDownPosition;
            }
        }

        private bool IsVerticalSwipe()
        {
            return VerticalMovementDistance() > HorizontalMovementDistance();
        }

        private bool SwipeDistanceCheckMet()
        {
            return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
        }

        private float VerticalMovementDistance()
        {
            return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
        }

        private float HorizontalMovementDistance()
        {
            return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
        }

        private void SendSwipe(SwipeDirection direction)
        {
            lastSwipe.Direction = direction;
            lastSwipe.StartPosition = fingerDownPosition;
            lastSwipe.EndPosition = fingerUpPosition;
            OnSwipe(lastSwipe);
        }
    }

    public struct SwipeData
    {
        public Vector2 StartPosition;
        public Vector2 EndPosition;
        public SwipeDirection Direction;
    }

    public enum SwipeDirection
    {
        Up,
        Down,
        Left,
        Right
    }
}
