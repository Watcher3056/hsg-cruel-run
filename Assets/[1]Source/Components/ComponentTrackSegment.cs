﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    public class ComponentTrackSegment : MonoCached
    {
        [HideInInspector]
        public Transform mainPivot;

        [FoldoutGroup("Setup")]
        public Renderer viewChecker;
        [FoldoutGroup("Setup")]
        public Transform leftCorner;
        [FoldoutGroup("Setup")]
        public Transform rightCorner;

        [FoldoutGroup("Status")]
        [ReadOnly]
        public Transform obstaclesHolder;
        [FoldoutGroup("Status")]
        [ReadOnly]
        public Transform collectableHolder;
        [FoldoutGroup("Status")]
        [ReadOnly]
        public bool playerEntered;
        [FoldoutGroup("Status")]
        [ReadOnly]
        public static ComponentTrackSegment curSegment;

        protected override void HandleEnable()
        {
            mainPivot = transform;

            obstaclesHolder = transform.FindDeep("ObstaclesHolder");
            if (obstaclesHolder == null)
            {
                obstaclesHolder = new GameObject("ObstaclesHolder").transform;
                obstaclesHolder.SetParent(transform);
            }
            collectableHolder = transform.FindDeep("CollectableHolder");
            if (collectableHolder == null)
            {
                collectableHolder = new GameObject("CollectableHolder").transform;
                collectableHolder.SetParent(transform);
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            ComponentColliderTriggerTransition temp = collision.GetComponent<ComponentColliderTriggerTransition>();
            if (temp == null)
                return;
            ActorPlayer player = temp.GetRoot() as ActorPlayer;
            if (player != null && !playerEntered)
            {
                curSegment = this;
                playerEntered = true;
            }
        }
    }
}
