﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    public class ComponentScreenEffect : MonoCached
    {
        [FoldoutGroup("Status")]
        [ShowInInspector]
        [ReadOnly]
        private List<Animator> animators;

        protected override void HandleEnable()
        {
            animators = gameObject.GetAllComponents<Animator>();
        }
        private void CorrectAnimationSpeed()
        {
            DataGameSettings dgs = DataGameSettings.Default;
            for (int i = 0; i < animators.Count; i++)
            {
                /*
                if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Horizontal && activeOnVisible)
                    animators[i].speed = ProcessingGameManager.Default.CurGlobalAnimSpeed /
                        ((dgs.landscape_ortographic_width * dgs.landscape_horizontal_offset + dgs.landscape_ortographic_width) / (dgs.portrait_ortographic_width * dgs.portrait_horizontal_offset + dgs.portrait_ortographic_width)) * 0.9f;
                else*/
                animators[i].speed = ProcessingGameManager.Default.CurGlobalAnimSpeed;
            }
        }
    }
}
