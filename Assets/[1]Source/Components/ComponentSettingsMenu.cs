﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.UI;
using static HSG.DataGameSettings;
using static HSG.DataGameSettings.Settings;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentSettingsMenu : MonoCached, ITick
    {
        public static ComponentSettingsMenu Default => _default;
        private static ComponentSettingsMenu _default;

        [FoldoutGroup("Setup")]
        public GameObject windowContentHolder;
        
        [FoldoutGroup("Setup/Sound")]
        [LabelText("Music")]
        public Slider sliderSoundMusic;
        [FoldoutGroup("Setup/Sound")]
        [LabelText("Effects")]
        public Slider sliderSoundEffects;

        [FoldoutGroup("Setup/Controls")]
        [LabelText("Controll Type")]
        public TMP_Dropdown dropDownControllType;
        [FoldoutGroup("Setup/Controls")]
        [LabelText("Glide Timing")]
        public Slider sliderGlideTiming;

        [FoldoutGroup("Setup/About")]
        public GameObject aboutWindow;
        [FoldoutGroup("Setup/About")]
        public TextMeshProUGUI textVersion;

        private static bool menuNowOpened, menuNowOpenedAbout;

        protected override void OnAwake()
        {
            _default = this;

            List<string> options = new List<string>();

            options = new List<string>();
            options.Add(Enum.GetName(typeof(ControlsType), ControlsType.Buttons));
            options.Add(Enum.GetName(typeof(ControlsType), ControlsType.Touches));
            dropDownControllType.AddOptions(options);

            if (ProcessingSaveLoad.Load(Enum.GetName(typeof(SettingsNames), SettingsNames.SettingsAvaliable), 0) == 1)
                LoadSettings();
            else
                SaveSettings();

            ButtonSetActiveWindow(menuNowOpened);
            ButtonSetActiveAboutWindow(menuNowOpenedAbout);
            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);
        }
        [SkipRename]
        public void ResetProgress()
        {
            DataGameSession.Default.CleanSavesLocal();
            ProcessingTrackManager.Default.Restart();
            ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.WaitingForStart;
            DataGameSession.Default.CleanSavesLocal();
        }
        [SkipRename]
        public void EditConsent()
        {
            GDPR.closeApp = true;
            ProcessingSceneLoad.To("GDPR");
        }
        public void LoadSettings()
        {
            DataGameSettings.Default.LoadSettings();

            sliderGlideTiming.value = DataGameSettings.Default.settings.controlsTimingValue;
            dropDownControllType.value = (int)DataGameSettings.Default.settings.controlsType;
            sliderSoundMusic.value = DataGameSettings.Default.settings.soundVolumeMusic;
            sliderSoundEffects.value = DataGameSettings.Default.settings.soundVolumeEffects;
        }
        public void SaveSettings()
        {
            Settings settings = new Settings();

            settings.controlsTimingValue = sliderGlideTiming.value;
            settings.controlsType = (Settings.ControlsType)dropDownControllType.value;
            settings.soundVolumeMusic = sliderSoundMusic.value;
            settings.soundVolumeEffects = sliderSoundEffects.value;

            DataGameSettings.Default.SaveSettings(settings);
        }
        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
        [SkipRename]
        public void ButtonSetActiveWindow(bool arg)
        {
            if (!arg)
                SaveSettings();
            windowContentHolder.SetActive(arg);
            menuNowOpened = windowContentHolder.activeSelf;
        }
        [SkipRename]
        public void ButtonSetActiveAboutWindow(bool arg)
        {
            aboutWindow.SetActive(arg);
            textVersion.text = Application.version + (Debug.isDebugBuild ? " (Development)" : " (Release)");
        }
        public void SaveAudioSettings()
        {
            DataGameSettings.Default.SaveAudioSettings(new Settings
            {
                soundVolumeMusic = sliderSoundMusic.value,
                soundVolumeEffects = sliderSoundEffects.value
            });
        }
        public void Tick()
        {
            if (!windowContentHolder.activeSelf)
                return;
            sliderGlideTiming.interactable = (Settings.ControlsType)dropDownControllType.value == Settings.ControlsType.Buttons ? false : true;
            SaveAudioSettings();
        }
    }
}
