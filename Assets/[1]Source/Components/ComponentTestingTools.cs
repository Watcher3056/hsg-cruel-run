﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using Beebyte.Obfuscator;
using UnityEngine.UI;

namespace HSG
{
    public class ComponentTestingTools : MonoCached, ITick
    {
        [FoldoutGroup("Setup")]
        public Transform mainHolder;
        [FoldoutGroup("Setup")]
        public Transform mainView;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI fpsCounter;
        [FoldoutGroup("Setup")]
        public Button buttonOpenMenu;
        [FoldoutGroup("Setup")]
        public float updateInterval = 0.5f;

        private float accum = 0; // FPS accumulated over the interval
        private int frames = 0; // Frames drawn over the interval
        private float timeleft; // Left time for current interval
        protected override void OnAwake()
        {
            if (!Debug.isDebugBuild)
            {
                Destroy(buttonOpenMenu.gameObject);
                HandleDestroyGO();
            }
            mainHolder.GetComponent<RectTransform>().localPosition = Vector3.zero;

            ButtonSetActiveMenu(false);

            timeleft = updateInterval;
        }
        public void Tick()
        {
            timeleft -= UnityEngine.Time.unscaledDeltaTime;
            accum += 1f / UnityEngine.Time.unscaledDeltaTime;
            ++frames;

            if (timeleft <= 0.0)
            {
                // display two fractional digits (f2 format)
                int fps = (int)(accum / frames);
                fpsCounter.text = fps.ToString();

                if (fps < 30)
                    fpsCounter.color = Color.yellow;
                else
                    if (fps < 10)
                    fpsCounter.color = Color.red;
                else
                    fpsCounter.color = Color.green;

                timeleft = updateInterval;
                accum = 0.0F;
                frames = 0;
            }
        }
        [SkipRename]
        public void ButtonSetActiveMenu(bool arg)
        {
            mainView.gameObject.SetActive(arg);
        }
        [SkipRename]
        public void ButtonGetMoney()
        {
            DataGameSession.Default.GodTearsCount += 100;
        }
        [SkipRename]
        public void ButtonClearMoney()
        {
            DataGameSession.Default.GodTearsCount = 0;
        }
        [SkipRename]
        public void ButtonUnlockOneNote()
        {
            ProcessingTrackManager.Default.CurCharacter.UnlockNewNote();
        }
        [SkipRename]
        public void ButtonCountinueRun()
        {
            if (ProcessingGameManager.Default.CurState != ProcessingGameManager.GameState.PlayerDead)
                return;
            ProcessingTrackManager.Default.Countinue();
            ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Play;
        }
        [SkipRename]
        public void ButtonToggleFPSCounter()
        {
            fpsCounter.gameObject.SetActive(!fpsCounter.gameObject.activeSelf);
        }
    }
}
