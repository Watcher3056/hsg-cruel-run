﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using UnityEngine;

namespace HSG
{
    public class ComponentSoundManager : MonoCached, ITick
    {
        public static ComponentSoundManager Default => _default;
        private static ComponentSoundManager _default;
        public class SoundPoolItem
        {
            public AudioSource audioSource;
            public float idleTime;
        }

        [FoldoutGroup("Setup")]
        public Transform poolHolder;
        [FoldoutGroup("Setup")]
        public int maxIdleTime;
        [FoldoutGroup("Setup")]
        public int minPoolSize;

        private List<SoundPoolItem> soundPool = new List<SoundPoolItem>(20);

        public ComponentSoundManager()
        {
            _default = this;
        }
        protected override void OnAwake()
        {

        }
        public void Tick()
        {
            for (int i = 0; i < soundPool.Count; i++)
            {
                if (!soundPool[i].audioSource.isPlaying)
                {
                    soundPool[i].idleTime += UnityEngine.Time.unscaledDeltaTime;
                    if (soundPool[i].idleTime > maxIdleTime && soundPool.Count > minPoolSize)
                    {
                        DestroyPoolItem(soundPool[i]);
                        i--;
                    }
                }
                else
                    soundPool[i].idleTime = 0f;
            }
        }
        public void ClearPull()
        {
            while (soundPool.Count != 0)
                DestroyPoolItem(soundPool[0]);
        }
        [SkipRename]
        public void PlaySound(AudioClip clip)
        {
            PlaySound(clip, 1f);
        }
        public AudioSource PlaySound(AudioClip clip, float volume)
        {
            SoundPoolItem pool = GetPool();
            pool.audioSource.clip = clip;
            pool.audioSource.loop = false;
            pool.audioSource.volume = volume;
            pool.audioSource.Play();
            return pool.audioSource;
        }
        private SoundPoolItem GetPool()
        {
            for (int i = 0; i < soundPool.Count; i++)
            {
                if (!soundPool[i].audioSource.isPlaying)
                    return soundPool[i];
            }
            SoundPoolItem poolItem = new SoundPoolItem();
            GameObject go = new GameObject("Sound Pool Item");
            go.transform.SetParent(poolHolder);

            poolItem.audioSource = go.AddComponent<AudioSource>();
            soundPool.Add(poolItem);
            return poolItem;
        }
        private void DestroyPoolItem(SoundPoolItem item)
        {
            GameObject.Destroy(item.audioSource.gameObject);
            item.audioSource = null;
            item.idleTime = 0f;
            soundPool.Remove(item);
        }
    }
}
