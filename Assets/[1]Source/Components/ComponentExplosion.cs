﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    public class ComponentExplosion : MonoCached
    {
        [FoldoutGroup("Setup")]
        public List<Rigidbody2D> particles;
        [FoldoutGroup("Setup")]
        public Transform explosionCenter;
        [FoldoutGroup("Setup")]
        public float posExpOffset;
        [FoldoutGroup("Setup")]
        public float power;
        [FoldoutGroup("Setup")]
        public float powerRotation;
        [FoldoutGroup("Setup")]
        public float radius;

        protected override void HandleEnable()
        {
            Vector2 expPosition = explosionCenter.position + new Vector3(UnityEngine.Random.Range(-posExpOffset, posExpOffset), UnityEngine.Random.Range(-posExpOffset, posExpOffset), 0f);
            for (int i = 0; i < particles.Count; i++)
                AddExplosionForce(particles[i], power * 100, expPosition, radius);
        }
        public void AddExplosionForce(Rigidbody2D body, float expForce, Vector3 expPosition, float expRadius)
        {
            var dir = (body.transform.position - expPosition);
            float calc = 1 - (dir.magnitude / expRadius);
            if (calc <= 0)
            {
                calc = 0;
            }

            body.AddForce(dir.normalized * expForce * calc);
            body.AddTorque(UnityEngine.Random.Range(-powerRotation, powerRotation), ForceMode2D.Force);
        }
    }
}
