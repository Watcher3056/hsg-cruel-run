﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HSG
{
    public class ComponentNotificationCallbackHelper : MonoCached
    {
        public Action callback;
        private Tweener tweener;
        private Button button;
        private EventTrigger eventTrigger;
        protected override void OnAwake()
        {
            button = GetComponent<Button>();
            if (button == null)
                eventTrigger = GetComponent<EventTrigger>();


            tweener = transform.DOPunchScale(transform.localScale / 6f, 1.5f, 1);
            tweener.onComplete += Restart;

            if (callback != null)
                SetCallback(callback);
        }
        public void SetCallback(Action callback)
        {
            if (button != null)
                button.onClick.AddListener(OnClick);
            else if (eventTrigger != null)
            {
                EventTrigger.TriggerEvent trigger = null;
                trigger = eventTrigger.triggers.Find((x) => { return x.eventID == EventTriggerType.PointerDown; }).callback;
                trigger.AddListener(OnClick);
            }
            this.callback = callback;
        }
        protected override void OnBeforeDestroy()
        {
            if (button != null)
                button.onClick.RemoveListener(OnClick);
            else if (eventTrigger != null)
                eventTrigger.triggers.Find((x) => { return x.eventID == EventTriggerType.PointerDown; }).callback.RemoveListener(OnClick);
            tweener.onComplete -= Restart;
            tweener.Complete();
            tweener.Kill();
        }
        private void Restart()
        {
            tweener.Restart();
        }
        private void OnClick(BaseEventData arg0)
        {
            OnClick();
        }
        private void OnClick()
        {
            OnBeforeDestroy();
            callback.Invoke();
            Destroy(this);
        }
    }
}
