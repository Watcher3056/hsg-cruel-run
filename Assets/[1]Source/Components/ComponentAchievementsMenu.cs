﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using SimpleLocalization;

namespace HSG
{
    public class ComponentAchievementsMenu : MonoCached
    {
        public static ComponentAchievementsMenu Default => _default;
        private static ComponentAchievementsMenu _default;

        [FoldoutGroup("Setup")]
        public Transform menuHolder;
        [FoldoutGroup("Setup")]
        public Transform contentHolder;
        [FoldoutGroup("Setup")]
        [AssetsOnly]
        public ComponentAchievementItem prefabAchievement;

        private List<ComponentAchievementItem> achievementItems;
        private List<DataAchievement> achievementListSorted;
        private static bool menuNowOpened;

        protected override void OnAwake()
        {
            _default = this;

            achievementItems = new List<ComponentAchievementItem>();
            achievementListSorted = new List<DataAchievement>(DataGameMain.Default.achievements.OrderBy(x => Convert.ToInt32(LocalizationManager.GetValue(x.localizationNameKey, "Sort"))));

            for (int i = 0; i < contentHolder.childCount; i++)
                contentHolder.GetChild(i).GetComponent<ComponentAchievementItem>().HandleDestroyGO();

            //List<List<DataAchievement>> tempList = new List<List<DataAchievement>>();
            //Array values = Enum.GetValues(typeof(DataAchievement.Type));
            //for (int i = 0; i < values.Length; i++)
            //{
            //    tempList.Add(DataGameMain.Default.achievements.FindAll((x) => { return x.type == (DataAchievement.Type)values.GetValue(i); }));
            //    achievementListSorted.AddRange(tempList[i].OrderBy(x => Convert.ToInt32(LocalizationManager.GetValue(x.localizationNameKey, "Sort"))));
            //}
            for (int i = 0; i < achievementListSorted.Count; i++)
                achievementItems.Add(this.Populate(Pool.None, prefabAchievement.gameObject, parent: contentHolder).GetComponent<ComponentAchievementItem>());

            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);

            ButtonToggleMenu(menuNowOpened);
        }
        [SkipRename]
        public void ButtonToggleMenu(bool arg)
        {
            if (arg)
            {
                for (int i = 0; i < achievementListSorted.Count; i++)
                    achievementItems[i].AssignAchievement(achievementListSorted[i]);
            }

            menuHolder.gameObject.SetActive(arg);
            menuNowOpened = arg;
        }
        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
    }
}
