﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace HSG
{
    public class ComponentNoteButton : MonoCached
    {
        [FoldoutGroup("Setup")]
        public Image viewReaded;
        [FoldoutGroup("Setup")]
        public Image viewUnreaded;
        [FoldoutGroup("Setup")]
        public Button button;

        public void SetReaded(bool arg)
        {
            viewReaded.gameObject.SetActive(arg);
            viewUnreaded.gameObject.SetActive(!arg);
        }
        [Beebyte.Obfuscator.SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
    }
}
