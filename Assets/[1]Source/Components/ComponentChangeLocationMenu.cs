﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;
using SimpleLocalization;
using TMPro;
using UnityEngine.UI;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentChangeLocationMenu : MonoCached
    {
        public static ComponentChangeLocationMenu Default => _default;
        private static ComponentChangeLocationMenu _default;

        [FoldoutGroup("Setup")]
        public Transform mainHolder;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI counter;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI locationName;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI costToUnlock;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI costToUnlockDollars;
        [FoldoutGroup("Setup")]
        public Image imageLocationPreview;
        [FoldoutGroup("Setup")]
        public Button buttonSelectBuy;
        [FoldoutGroup("Setup")]
        public Sprite spriteSelect;
        [FoldoutGroup("Setup")]
        public Sprite spriteBuy;
        [FoldoutGroup("Setup")]
        public GameObject costToUnlockView;

        public static int curIndex;
        private static bool menuNowOpened;

        protected override void OnAwake()
        {
            _default = this;

            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);

            SetActiveMenu(menuNowOpened);

            Localize();
            LocalizationManager.LocalizationChanged += Localize;
        }
        protected override void OnBeforeDestroy()
        {
            LocalizationManager.LocalizationChanged -= Localize;
        }
        private void Localize()
        {
            locationName.font = LocalizationManager.CurFont;
        }
        [SkipRename]
        public void ButtonScrollRight()
        {
            curIndex++;
            if (curIndex > DataGameMain.Default.locations.Count - 1)
                curIndex = 0;
            ShowLocationPreview(curIndex);
        }
        [SkipRename]
        public void ButtonScrollLeft()
        {
            curIndex--;
            if (curIndex < 0)
                curIndex = DataGameMain.Default.locations.Count - 1;
            ShowLocationPreview(curIndex);
        }
        [SkipRename]
        public void ButtonSelectBuy()
        {
            if (DataGameMain.Default.locations[curIndex].IsUnlocked)
            {
                ProcessingTrackManager.Default.CurLocation = DataGameMain.Default.locations[curIndex];
                SetActiveMenu(false);
            }
            else if (DataGameSession.Default.GodTearsCount >= DataGameMain.Default.locations[curIndex].costToUnlock)
            {
                DataGameSession.Default.GodTearsCount -= DataGameMain.Default.locations[curIndex].costToUnlock;
                DataGameMain.Default.locations[curIndex].Unlock();
                ShowLocationPreview(curIndex);
            }
            else
            {
                ProcessingIAPManager.Default.BuyProduct(DataGameMain.Default.locations[curIndex].localizationNameKey);
            }
        }
        [SkipRename]
        public void SetActiveMenu(bool arg)
        {
            mainHolder.gameObject.SetActive(arg);
            if (arg)
                ShowLocationPreview(curIndex);
            menuNowOpened = arg;
        }
        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
        public void ShowLocationPreview(int i)
        {
            counter.text = string.Format("{0}/{1}", curIndex + 1, DataGameMain.Default.locations.Count);
            locationName.text = LocalizationManager.Localize(DataGameMain.Default.locations[curIndex].localizationNameKey);
            imageLocationPreview.sprite = DataGameMain.Default.locations[curIndex].previewImage;

            if (DataGameMain.Default.locations[curIndex].IsUnlocked)
            {
                buttonSelectBuy.interactable =
                    ProcessingTrackManager.Default.CurLocation !=
                    DataGameMain.Default.locations[curIndex];
            }
            else
            {
                buttonSelectBuy.interactable = true;
                //DataGameSession.Default.GodTearsCount >=
                //DataGameMain.Default.locations[curIndex].costToUnlock;
            }
            (buttonSelectBuy.targetGraphic as Image).sprite =
                DataGameMain.Default.locations[curIndex].IsUnlocked ? spriteSelect : spriteBuy;
            costToUnlockView.SetActive(!DataGameMain.Default.locations[curIndex].IsUnlocked);
            costToUnlock.text = DataGameMain.Default.locations[curIndex].costToUnlock.ToString();
            costToUnlockDollars.text = DataGameMain.Default.locations[curIndex].costToUnlockDollars.ToString() + '$';
        }
    }
}
