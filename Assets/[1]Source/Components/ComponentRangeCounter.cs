﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;

namespace HSG
{
    public class ComponentRangeCounter : MonoCached, ITick
    {
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI currentCounter;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI recordCounter;

        public void Tick()
        {
            currentCounter.text = Convert.ToInt32(ProcessingTrackManager.Default.curDistanceTraveled).ToString();
            recordCounter.text = Convert.ToInt32(DataGameSession.Default.RecordDistance).ToString();
        }
    }
}
