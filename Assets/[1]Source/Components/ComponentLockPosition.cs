﻿using Homebrew;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace HSG
{
    public class ComponentLockPosition : MonoCached, ITickLate
    {
        [FoldoutGroup("Setup")]
        public Transform transformLockTo;
        [FoldoutGroup("Setup")]
        public bool freezeX, freezeY, freezeZ;

        private Vector3 parentLastPosition;
        private Vector3 diff;
        private Transform transformLastLockTo;
        public void Start()
        {
            if (transformLockTo != null)
                parentLastPosition = transformLockTo.position;
            transformLastLockTo = transformLockTo;

            Debug.Log("[" + DateTime.Now + " Tick: " + DateTime.Now.Ticks + "]: " + "[" + "COMPONENT" + "] " + "[" + this + "]" + "----Setup End");
        }
        public void TickLate()
        {
            if (transformLockTo == null)
                return;
            if (transformLockTo != transformLastLockTo)
            {
                transformLastLockTo = transformLockTo;
                parentLastPosition = transformLockTo.position;
            }
            diff = transformLockTo.position - parentLastPosition;
            if (freezeX) diff.x = 0f;
            if (freezeY) diff.y = 0f;
            if (freezeZ) diff.z = 0f;
            if (diff != Vector3.zero)
            {
                transform.position = transform.position + diff;
                parentLastPosition = transformLockTo.position;
            }
        }
    }
}