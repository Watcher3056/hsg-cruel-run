﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;


namespace HSG
{
    public class ComponentRagdollSwitch : MonoCached
    {
        public static ComponentRagdollSwitch Default => _default;
        private static ComponentRagdollSwitch _default;

        [FoldoutGroup("Setup")]
        public List<GameObject> toggleOnDead;

        protected override void OnAwake()
        {
            _default = this;
        }
        public void Toggle()
        {
            foreach (GameObject go in toggleOnDead)
                go.SetActive(!go.activeSelf);
        }
    }
}
