﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;


namespace HSG
{
    public class ComponentGUID : MonoCached
    {
        [FoldoutGroup("GUID")]
        [ValidateInput("_ValidateID", "GUID is not set!")]
        [ReadOnly]
        public string id;

        public Guid GetGuid()
        {
            return new Guid(id);
        }
#if UNITY_EDITOR
        private bool _ValidateID(string id)
        {
            if (this.id == "")
                UpdateGUID();
            return this.id != "";
        }
        [FoldoutGroup("GUID")]
        [Button]
        private void UpdateGUID()
        {
            GameObject rootGO = transform.root.gameObject;
            string assetPath = UnityEditor.Experimental.SceneManagement.PrefabStageUtility.GetPrefabStage(rootGO).prefabAssetPath;
            this.id = UnityEditor.AssetDatabase.AssetPathToGUID(assetPath);
        }
#endif
    }
}
