﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using SimpleLocalization;

namespace HSG
{
    public class ComponentIntroComics : MonoCached
    {
        [Serializable]
        public class Page
        {
            public enum TransitionType { Simple, Transparency }
            public Transform transformPage;
            public Image mainImage;
            [EnumToggleButtons]
            public TransitionType transitionType;
            [ShowIf("transitionType", TransitionType.Transparency)]
            public float transitionTime;
        }


        [FoldoutGroup("Setup")]
        public List<Page> pages;
        [FoldoutGroup("Setup")]
        public List<Button> buttons;

        private int curIndex, prevIndex = -1;
        private List<TextMeshProUGUI> tempTextListCur;
        protected override void OnAwake()
        {
            foreach (Page page in pages)
            {
                page.transformPage.GetComponent<RectTransform>().localPosition = Vector3.zero;
                page.transformPage.gameObject.SetActive(false);
            }
            ShowPage(0);
        }

        [SkipRename]
        public void ButtonNext()
        {
            curIndex++;
            if (curIndex >= pages.Count)
            {
                ProcessingSaveLoad.SavePP("IntroComicsReleased", true);
                ProcessingSceneLoad.To("loading");
                curIndex = 0;
                return;
            }
            ShowPage(curIndex);
        }
        [SkipRename]
        public void ButtonPrev()
        {
            curIndex--;
            if (curIndex < 0)
            {
                curIndex = 0;
                return;
            }
            ShowPage(curIndex);
        }
        private void ShowPage(int index)
        {
            float newScale = (float)Screen.height /
                (((float)Screen.width / (float)pages[index].mainImage.sprite.rect.width) * (float)pages[index].mainImage.sprite.rect.height);
            Debug.Log(newScale);
            pages[index].transformPage.localScale = new Vector3(newScale, newScale, 1f);

            int transitionIndex = index;
            if (index < prevIndex && prevIndex != pages.Count - 1)
                transitionIndex = prevIndex;


            if (pages[transitionIndex].transitionType == Page.TransitionType.Simple)
            {
                pages[index].transformPage.gameObject.SetActive(true);
                if (prevIndex >= 0)
                    pages[prevIndex].transformPage.gameObject.SetActive(false);
            }
            else if (pages[transitionIndex].transitionType == Page.TransitionType.Transparency)
            {
                pages[index].transformPage.SetAsLastSibling();
                tempTextListCur = pages[index].transformPage.gameObject.GetAllComponents<TextMeshProUGUI>();

                int tempIndex = prevIndex;

                pages[index].transformPage.gameObject.SetActive(true);
                pages[index].mainImage.color = pages[index].mainImage.color.SetColorAlpha(0f);
                DOTween.To(
                    () => pages[index].mainImage.color.a,
                    a => 
                    {
                        pages[index].mainImage.color = pages[index].mainImage.color.SetColorAlpha(a);
                        foreach (TextMeshProUGUI text in tempTextListCur) text.color = text.color.SetColorAlpha(a);
                    },
                    1f, pages[transitionIndex].transitionTime).onComplete +=
                    () =>
                    {
                        foreach (Button button in buttons) button.interactable = true;
                        pages[tempIndex].transformPage.gameObject.SetActive(false);
                    };

                foreach (Button button in buttons) button.interactable = false;
            }
            prevIndex = index;
        }
    }
}
