﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentBalanceCounter : MonoCached, ITick
    {
        [FoldoutGroup("Setup")]
        public Animator animator;
        [FoldoutGroup("Setup")]
        public GameObject countersHolder;
        [FoldoutGroup("Setup")]
        public TMP_Text godTearsCounter;
        //[FoldoutGroup("Setup")]
        //public TMP_Text coinsCounter;
        [FoldoutGroup("Setup")]
        public float showTime;

        private float curShowTime;
        private int prevGodTearsCount;
        private bool showPermanent;

        protected override void HandleEnable()
        {
            ProcessingGameManager.OnPlay += HandleOnPlay;
            ProcessingGameManager.OnPause += HandleOnPause;
            ProcessingGameManager.OnPlayerDead += HandleOnPlayerDead;
            ProcessingGameManager.OnWaitingForStart += HandleOnWaitingForStart;
        }
        protected override void OnBeforeDestroy()
        {
            ProcessingGameManager.OnPlay += HandleOnPlay;
            ProcessingGameManager.OnPause -= HandleOnPause;
            ProcessingGameManager.OnPlayerDead -= HandleOnPlayerDead;
            ProcessingGameManager.OnWaitingForStart -= HandleOnWaitingForStart;
        }
        [SkipRename]
        public void ShowCounters()
        {
            curShowTime = showTime;
        }

        public void Tick()
        {
            if (curShowTime > 0 || showPermanent)
            {
                godTearsCounter.text = DataGameSession.Default.GodTearsCount.ToString();
                //coinsCounter.text = DataGameSession.Default.CoinsCount.ToString();
                countersHolder.SetActive(true);
                curShowTime -= UnityEngine.Time.deltaTime;
            }
            else if (curShowTime <= 0)
            {
                countersHolder.SetActive(false);
                curShowTime = 0f;
            }

            if (prevGodTearsCount != DataGameSession.Default.GodTearsCount)
                animator.SetTrigger("Default");
            prevGodTearsCount = DataGameSession.Default.GodTearsCount;
        }
        private void HandleOnPlay()
        {
            showPermanent = false;
        }
        private void HandleOnPlayerDead()
        {
            showPermanent = true;
        }
        private void HandleOnPause()
        {
            showPermanent = true;
        }
        private void HandleOnWaitingForStart()
        {
            showPermanent = true;
        }
    }
}
