﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace HSG
{
    public class ComponentUIManager : MonoCached
    {
        [Serializable]
        public class ScreenDeathEffect
        {
            public enum Type { None, Acid, Burn, Cold, Blood, Meat, Broke, Lightning }
            [EnumToggleButtons]
            public Type type;
            public List<ComponentScreenEffect> prefabEffects;
        }
        [Serializable]
        public class OrientationObject { public GameObject onPortrait, onLandscape; }

        public static ComponentUIManager Default => _default;
        private static ComponentUIManager _default;

        public static Action<bool> OnApplicationFocused = (bool arg) => { };

        [FoldoutGroup("Setup")]
        [LabelText("Screen Effects Holder")]
        public Transform transformScreenEffectsHolder;
        [FoldoutGroup("Setup")]
        public List<ScreenDeathEffect> screenDeathEffects;
        [FoldoutGroup("Setup")]
        public GameObject deathScreen;
        [FoldoutGroup("Setup")]
        public GameObject adLoaingIndicator;
        [FoldoutGroup("Setup")]
        public ComponentPauseMenu pauseMenu;
        [FoldoutGroup("Setup")]
        public ComponentRangeCounter rangeCounter;
        [FoldoutGroup("Setup")]
        public ComponentBalanceCounter balanceCounter;
        [FoldoutGroup("Setup")]
        public ComponentChangeSkin changeSkin;
        [FoldoutGroup("Setup")]
        public Transform UIHolderNonPlayTime;
        [FoldoutGroup("Setup")]
        public Transform UIHolderPlayTime;
        [FoldoutGroup("Setup")]
        [ListDrawerSettings(ShowIndexLabels = false, NumberOfItemsPerPage = 5)]
        public List<OrientationObject> orientationObjectsNonPT;
        [FoldoutGroup("Setup")]
        [ListDrawerSettings(ShowIndexLabels = false, NumberOfItemsPerPage = 5)]
        public List<OrientationObject> orientationObjectsPT;
        [FoldoutGroup("Setup")]
        public List<GameObject> spawnedOrientObjs;

        private List<Image> imagesDeathScreen;
        public ComponentUIManager()
        {
            _default = this;
        }
        protected override void OnAwake()
        {
            adLoaingIndicator.SetActive(false);
            adLoaingIndicator.GetComponent<RectTransform>().localPosition = Vector3.zero;
            SM_GameMode.OnOrientationChanged += HandleOnOrientationChanged;
            ToggleBlackScreen(false, 0f);

            deathScreen.GetComponent<RectTransform>().localPosition = Vector3.zero;
            imagesDeathScreen = new List<Image>(Functions.GetAllComponents<Image>(deathScreen));
        }
        protected override void OnBeforeDestroy()
        {
            SM_GameMode.OnOrientationChanged -= HandleOnOrientationChanged;
        }
        TweenerCore<float, float, FloatOptions> tweener = null;
        public void ToggleBlackScreen(bool toggle, float fadeTime)
        {
            void OnComplete()
            {
                if (!toggle) deathScreen.gameObject.SetActive(toggle);
            }
            tweener.Complete();
            tweener.Kill();
            if (toggle) deathScreen.gameObject.SetActive(toggle);
            tweener = DOTween.To(() => imagesDeathScreen[0].color.a, x =>
            {
                foreach (Image image in imagesDeathScreen) image.color = image.color.SetColorAlpha(x);
            },
            toggle ? 1f : 0f, fadeTime);
            tweener.onComplete += OnComplete;
        }
        public void AddRandomEffect(ScreenDeathEffect.Type type)
        {
            ScreenDeathEffect effects = screenDeathEffects.Find((x) => x.type == type);
            AddEffect(effects.prefabEffects[UnityEngine.Random.Range(0, effects.prefabEffects.Count)]);
        }
        public void AddEffect(ComponentScreenEffect effect)
        {
            this.Populate(Pool.None, effect.gameObject, parent: transformScreenEffectsHolder)
                .GetComponent<RectTransform>().localPosition = Vector3.zero;
        }
        public void RemoveAllEffects()
        {
            ComponentScreenEffect effect = null;
            while (transformScreenEffectsHolder.childCount != 0)
            {
                effect = transformScreenEffectsHolder.GetChild(0).GetComponent<ComponentScreenEffect>();
                effect.transform.SetParent(null);
                effect.HandleDestroyGO();
            }
        }
        private void RemoveAllMenu()
        {
            for (int i = 0; i < spawnedOrientObjs.Count; i++)
            {
                if (spawnedOrientObjs[i].GetComponent<MonoCached>() != null)
                    spawnedOrientObjs[i].GetComponent<MonoCached>().HandleDestroyGO();
                else
                    Destroy(spawnedOrientObjs[i]);
            }
            spawnedOrientObjs.Clear();
        }
        private void HandleOnOrientationChanged()
        {
            RemoveAllMenu();

            for (int i = 0; i < orientationObjectsNonPT.Count; i++)
            {
                spawnedOrientObjs.Add(
                this.Populate(Pool.None, ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ?
                    orientationObjectsNonPT[i].onPortrait : orientationObjectsNonPT[i].onLandscape,
                    parent: UIHolderNonPlayTime).gameObject
                );
                spawnedOrientObjs[spawnedOrientObjs.Count - 1].transform.SetAsLastSibling();
            }
            for (int i = 0; i < orientationObjectsPT.Count; i++)
            {
                spawnedOrientObjs.Add(
                this.Populate(Pool.None, ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ?
                    orientationObjectsPT[i].onPortrait : orientationObjectsPT[i].onLandscape,
                    parent: UIHolderPlayTime).gameObject
                );
                spawnedOrientObjs[spawnedOrientObjs.Count - 1].transform.SetAsLastSibling();
            }
        }
        private void OnApplicationFocus(bool focus)
        {
            OnApplicationFocused.Invoke(focus);
        }
#if UNITY_EDITOR
        private bool test;
        [Button]
        public void _HandleOnOrientationChanged()
        {
            RemoveAllMenu();

            for (int i = 0; i < orientationObjectsNonPT.Count; i++)
            {
                spawnedOrientObjs.Add(
                this.Populate(Pool.None, test ?
                    orientationObjectsNonPT[i].onPortrait : orientationObjectsNonPT[i].onLandscape,
                    parent: UIHolderNonPlayTime).gameObject
                );
                spawnedOrientObjs[spawnedOrientObjs.Count - 1].transform.SetAsLastSibling();
            }
            for (int i = 0; i < orientationObjectsPT.Count; i++)
            {
                spawnedOrientObjs.Add(
                this.Populate(Pool.None, test ?
                    orientationObjectsPT[i].onPortrait : orientationObjectsPT[i].onLandscape,
                    parent: UIHolderPlayTime).gameObject
                );
                spawnedOrientObjs[spawnedOrientObjs.Count - 1].transform.SetAsLastSibling();
            }
            test = !test;
        }
#endif
    }
}
