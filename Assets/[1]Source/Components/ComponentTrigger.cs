﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace HSG
{
    public class ComponentTrigger : MonoCached, ITriggerTransitionReceiver2D
    {
        [FoldoutGroup("Setup")]
        public UnityEvent onTriggerEnter2D;
        [FoldoutGroup("Setup")]
        public BehaviorPlayerController.State requiredState;
        [FoldoutGroup("Setup")]
        public bool playSoundOnEnter;
        [ShowIf("playSoundOnEnter")]
        //[Required]
        public AudioSource audioSource;
        [ShowIf("playSoundOnEnter")]
        //[Required]
        public AudioClip audioClip;
        [FoldoutGroup("Setup")]
        public bool setScreenEffect;
        [FoldoutGroup("Setup")]
        [ShowIf("setScreenEffect")]
        //[Required]
        public ComponentScreenEffect screenEffect;

        private bool playerEnteredBefore;

        public void OnCollisionEnter2D(Collision2D collision)
        {

        }

        public void OnCollisionExit2D(Collision2D collision)
        {

        }

        public void OnCollisionStay2D(Collision2D collision)
        {

        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            ComponentColliderTriggerTransition temp = collision.GetComponent<ComponentColliderTriggerTransition>();

            if (temp == null)
                return;

            ActorPlayer player = temp.GetRoot() as ActorPlayer;

            if ((player == null || requiredState != player.CurAction) && requiredState != BehaviorPlayerController.State.None)
                return;
            OnEnter();
            onTriggerEnter2D.Invoke();
        }

        public void OnTriggerExit2D(Collider2D collision)
        {

        }

        public void OnTriggerStay2D(Collider2D collision)
        {

        }
        private void OnEnter()
        {
            if (playerEnteredBefore)
                return;
            if (playSoundOnEnter)
            {
                audioSource.clip = audioClip;
                audioSource.Play();
            }
            if (setScreenEffect)
            {
                ComponentUIManager.Default.AddEffect(screenEffect);
            }
            playerEnteredBefore = true;
        }
    }
}
