﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;
using UnityEngine.UI;

namespace HSG
{
    public class ComponentSkinButton : MonoCached
    {
        [FoldoutGroup("Setup")]
        public Image imagePreview;
        [FoldoutGroup("Setup")]
        public Button button;
    }
}
