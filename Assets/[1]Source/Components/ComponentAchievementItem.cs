﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;
using SimpleLocalization;
using static HSG.DataAchievement;

namespace HSG
{
    public class ComponentAchievementItem : MonoCached
    {
        [FoldoutGroup("Setup")]
        public Sprite spriteLocked;
        [FoldoutGroup("Setup")]
        public Image imageAchievementIcon;
        [FoldoutGroup("Setup")]
        public Image progressBar;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI textName;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI textDescription;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI textProgress;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI textReward;

        private DataAchievement achievement;
        protected override void OnAwake()
        {
            LocalizationManager.LocalizationChanged += UpdateView;
        }
        protected override void OnBeforeDestroy()
        {
            LocalizationManager.LocalizationChanged -= UpdateView;
        }
        public void UpdateView()
        {
            if (achievement == null)
                return;
            imageAchievementIcon.sprite = achievement.IsUnlocked ? achievement.icon : spriteLocked;
            textName.text = LocalizationManager.Localize(achievement.localizationNameKey);
            textDescription.text = LocalizationManager.Localize(achievement.localizationDescriptionKey);
            textReward.text = achievement.reward.ToString();

            Progress progress = achievement.GetProgress();
            if (achievement.IsUnlocked)
                progress.current = progress.required;
            textProgress.text = progress.current + " / " + progress.required;
            progressBar.fillAmount = (float)(progress.current) / (float)(progress.required);
        }
        public void AssignAchievement(DataAchievement achievement)
        {
            this.achievement = achievement;
            UpdateView();
        }
    }
}
