﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;
using SimpleLocalization;
using TMPro;
using UnityEngine.UI;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentChangeCharacterMenu : MonoCached
    {
        public static ComponentChangeCharacterMenu Default => _default;
        private static ComponentChangeCharacterMenu _default;

        [FoldoutGroup("Setup")]
        public Transform mainHolder;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI counter;
        [FoldoutGroup("Setup")]
        public TextMeshProUGUI characterName;
        [FoldoutGroup("Setup")]
        public Image imageCharacterPreview;
        [FoldoutGroup("Setup")]
        public Button buttonSelectBuy;
        [FoldoutGroup("Setup")]
        public Sprite spriteSelect;
        [FoldoutGroup("Setup")]
        public GameObject lockedView;

        private static int curIndex;
        private static bool menuNowOpened;

        protected override void OnAwake()
        {
            _default = this;
            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);

            SetActiveMenu(menuNowOpened);

            Localize();
            LocalizationManager.LocalizationChanged += Localize;
        }
        protected override void OnBeforeDestroy()
        {
            LocalizationManager.LocalizationChanged -= Localize;
        }
        private void Localize()
        {
            characterName.font = LocalizationManager.CurFont;
        }
        [SkipRename]
        public void ButtonScrollRight()
        {
            curIndex++;
            if (curIndex > DataGameMain.Default.characters.Count - 1)
                curIndex = 0;
            ShowCharacterPreview(curIndex);
        }
        [SkipRename]
        public void ButtonScrollLeft()
        {
            curIndex--;
            if (curIndex < 0)
                curIndex = DataGameMain.Default.characters.Count - 1;
            ShowCharacterPreview(curIndex);
        }
        [SkipRename]
        public void ButtonSelectBuy()
        {
            if (DataGameMain.Default.characters[curIndex].IsUnlocked)
            {
                ProcessingTrackManager.Default.CurCharacter = DataGameMain.Default.characters[curIndex];
                SetActiveMenu(false);
            }
        }
        [SkipRename]
        public void SetActiveMenu(bool arg)
        {
            mainHolder.gameObject.SetActive(arg);
            if (arg)
                ShowCharacterPreview(curIndex);
            menuNowOpened = arg;
        }
        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
        private void ShowCharacterPreview(int i)
        {
            counter.text = string.Format("{0}/{1}", curIndex + 1, DataGameMain.Default.characters.Count);
            characterName.text = DataGameMain.Default.characters[curIndex].IsUnlocked ?
                LocalizationManager.Localize(DataGameMain.Default.characters[curIndex].localizationNameKey) : 
                LocalizationManager.Localize("Required") + "\n" +
                LocalizationManager.Localize(DataGameMain.Default.characters[curIndex].GetRequiredLocation().localizationNameKey);
            imageCharacterPreview.sprite = DataGameMain.Default.characters[curIndex].previewImage;
            imageCharacterPreview.color = DataGameMain.Default.characters[curIndex].IsUnlocked ? Color.white : Color.black;

            buttonSelectBuy.interactable =
                ProcessingTrackManager.Default.CurCharacter !=
                DataGameMain.Default.characters[curIndex] &&
                DataGameMain.Default.characters[curIndex].IsUnlocked;

            lockedView.SetActive(!DataGameMain.Default.characters[curIndex].IsUnlocked);
        }
    }
}
