﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;
using Beebyte.Obfuscator;

namespace HSG
{
    public class ComponentShopMenu : MonoCached
    {
        public static ComponentShopMenu Default => _default;
        private static ComponentShopMenu _default;

        [FoldoutGroup("Setup")]
        public Transform mainHolder;

        public ComponentShopMenu()
        {
            _default = this;
        }
        protected override void OnAwake()
        {
            mainHolder.gameObject.SetActive(false);
            ProcessingDeferredOperation.Default.Add(() =>
            {
                gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
            }, true);
        }
        [SkipRename]
        public void ToggleWindow(bool arg)
        {
            mainHolder.gameObject.SetActive(arg);
        }
        [SkipRename]
        public void AddGodTears(int amount)
        {
            DataGameSession.Default.GodTearsCount += amount;
        }
        [SkipRename]
        public void PlaySound(AudioClip audioClip)
        {
            ComponentSoundManager.Default.PlaySound(audioClip);
        }
    }
}
