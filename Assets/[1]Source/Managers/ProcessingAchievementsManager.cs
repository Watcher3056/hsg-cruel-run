﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public class ProcessingAchievementsManager : ProcessingBase
    {
        public static ProcessingAchievementsManager Default => _default;
        private static ProcessingAchievementsManager _default;

        public ProcessingAchievementsManager()
        {
            _default = this;
        }
        public void Setup()
        {
            ProcessingGameManager.OnPlayerDead += CheckAllAchievements;
            ProcessingSaveLoad.onLocalDataUpdated += CheckAllAchievements;
            ComponentUIManager.OnApplicationFocused += HandleOnApplicationFocused;
        }
        private void HandleOnApplicationFocused(bool arg)
        {
            if (!arg)
                CheckAllAchievements();
        }
        protected override void OnDispose()
        {
            ProcessingGameManager.OnPlayerDead -= CheckAllAchievements;
            ProcessingSaveLoad.onLocalDataUpdated -= CheckAllAchievements;
            ComponentUIManager.OnApplicationFocused -= HandleOnApplicationFocused;
        }
        private static void CheckAllAchievements()
        {
            foreach (DataAchievement achievement in DataGameMain.Default.achievements)
                achievement.CheckProgress();
            ProcessingTutorial.Default.CheckNotifications();
        }
    }
}
