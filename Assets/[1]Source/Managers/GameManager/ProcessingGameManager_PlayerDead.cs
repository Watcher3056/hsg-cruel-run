﻿using AppodealAds.Unity.Api;
using Homebrew;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HSG
{
    public partial class ProcessingGameManager
    {
        public static event Action OnPlayerDead = () => { };
        private void SetupStatePlayerDead()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStatePlayerDead;
            state.OnEnd = EndStatePlayerDead;

            statesMap.Add(GameState.PlayerDead, state);
        }
        private void StartStatePlayerDead()
        {
            ComponentPauseMenu.Default.buttonSkins.interactable = false;
            ComponentControls.Default.ToggleWindow(false);
            ComponentUIManager.Default.UIHolderNonPlayTime.GetComponent<Canvas>().enabled = true;
            ComponentUIManager.Default.RemoveAllEffects();

            if (ProcessingTrackManager.Default.curDistanceTraveled > DataGameSession.Default.RecordDistance)
                DataGameSession.Default.RecordDistance = (int)ProcessingTrackManager.Default.curDistanceTraveled;
            if (ProcessingTrackManager.Default.curDistanceTraveled > ProcessingTrackManager.Default.CurLocation.LocalDistanceRecord)
                ProcessingTrackManager.Default.CurLocation.LocalDistanceRecord = (int)ProcessingTrackManager.Default.curDistanceTraveled;

            OnPlayerDead.Invoke();

            GC.Collect();
        }
        private void EndStatePlayerDead()
        {
            ComponentPauseMenu.Default.buttonSkins.interactable = true;
        }
    }
}
