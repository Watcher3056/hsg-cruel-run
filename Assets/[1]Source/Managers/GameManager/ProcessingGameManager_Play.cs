﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public partial class ProcessingGameManager
    {
        public static event Action OnPlay = () => { };
        private void SetupStatePlay()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStatePlay;
            state.OnEnd = EndStatePlay;

            statesMap.Add(GameState.Play, state);
        }
        private void StartStatePlay()
        {
            ComponentUIManager.Default.changeSkin.view.SetActive(false);
            ComponentUIManager.Default.UIHolderNonPlayTime.GetComponent<Canvas>().enabled = false;
            ComponentPauseMenu.Default.SetMenuVisible(false);
            ComponentControls.Default.ControlsType = DataGameSettings.Default.settings.controlsType;
            ComponentControls.Default.ToggleWindow(ProcessingTrackManager.Default.Player.CurState != ActorPlayer.State.Dead);

            ProcessingTrackManager.Default.Player.CurAction = BehaviorPlayerController.State.Run;

            Camera.main.GetComponent<AudioSource>().clip = ProcessingTrackManager.Default.CurLocation.mainTheme;
            Camera.main.GetComponent<AudioSource>().Play();

            OnPlay.Invoke();
        }
        public void EndStatePlay()
        {

        }
    }
}
