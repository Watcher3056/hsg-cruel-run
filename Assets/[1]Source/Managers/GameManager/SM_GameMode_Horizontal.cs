﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HSG
{
    public partial class SM_GameMode
    {
        private void SetupStateHorizontal()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStateHorizontal;
            state.OnEnd = EndStateHorizontal;

            statesMap.Add(GameMode.Horizontal, state);
        }
        private void StartStateHorizontal()
        {
            /*Screen.orientation = ScreenOrientation.LandscapeRight;Input.deviceOrientation == DeviceOrientation.LandscapeRight ? 
                ScreenOrientation.LandscapeRight : ScreenOrientation.LandscapeLeft;*/
        }
        private void EndStateHorizontal()
        {
            float ratio = (ProcessingTrackManager.Default.Player.dataPlayer.curSpeed -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedHorizontal) /
                (ProcessingTrackManager.Default.dataTrackManager.maxSpeedHorizontal -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedHorizontal);
            ProcessingTrackManager.Default.Player.dataPlayer.curSpeed =
                (ProcessingTrackManager.Default.dataTrackManager.maxSpeedVertical -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedVertical) *
                ratio + ProcessingTrackManager.Default.dataTrackManager.minSpeedVertical;
        }
    }
}
