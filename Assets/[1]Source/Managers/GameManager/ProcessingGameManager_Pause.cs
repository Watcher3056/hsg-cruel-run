﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public partial class ProcessingGameManager
    {
        public static event Action OnPause = () => { };
        private float animSpeed, fixedTimeScale, timeScale;
        private void SetupStatePause()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStatePause;
            state.OnEnd = EndStatePause;

            statesMap.Add(GameState.Pause, state);
        }
        private void StartStatePause()
        {
            Physics2D.autoSimulation = false;
            ComponentControls.Default.ToggleWindow(false);

            ProcessingTrackManager.Default.Player.CurAction = BehaviorPlayerController.State.None;
            ProcessingTrackManager.Default.Player.dataPlayer.animator.speed = 0f;

            animSpeed = ProcessingGameManager.Default.CurGlobalAnimSpeed;
            fixedTimeScale = ProcessingGameManager.Default.FixedTimeScale;
            timeScale = Homebrew.Time.Default.timeScale;

            ProcessingGameManager.Default.CurGlobalAnimSpeed = 0f;
            ProcessingGameManager.Default.FixedTimeScale = 0f;
            Homebrew.Time.Default.timeScale = 0f;

            Camera.main.GetComponent<AudioSource>().clip = DataGameSettings.Default.onPause;
            Camera.main.GetComponent<AudioSource>().Play();

            OnPause.Invoke();
        }
        private void EndStatePause()
        {
            Physics2D.autoSimulation = true;
            ProcessingTrackManager.Default.Player.dataPlayer.animator.speed = 2f;
            ProcessingGameManager.Default.CurGlobalAnimSpeed = animSpeed;
            ProcessingGameManager.Default.FixedTimeScale = fixedTimeScale / 0.02f;
            Homebrew.Time.Default.timeScale = timeScale;
        }
    }
}
