﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace HSG
{
    public partial class ProcessingGameManager
    {
        public static event Action OnWaitingForStart = () => { };
        private void SetupStateWaitingForStart()
        {
            StateDefault state = new StateDefault();

            state.OnStart = StartStateWaitingForStart;
            state.OnEnd = EndStateWaitingForStart;
            statesMap.Add(GameState.WaitingForStart, state);
        }
        private void StartStateWaitingForStart()
        {
            ProcessingTrackManager.Default.Restart();

            ComponentUIManager.Default.UIHolderNonPlayTime.GetComponent<Canvas>().enabled = true;
            ProcessingTrackManager.Default.Player.gameObject.SetActive(false);

            ComponentControls.Default.ToggleWindow(false);

            Camera.main.GetComponent<AudioSource>().clip = DataGameSettings.Default.onPause;
            Camera.main.GetComponent<AudioSource>().Play();

            OnWaitingForStart.Invoke();

            GC.Collect();
        }
        private void EndStateWaitingForStart()
        {
            ProcessingTrackManager.Default.Player.gameObject.SetActive(true);
        }
    }
}
