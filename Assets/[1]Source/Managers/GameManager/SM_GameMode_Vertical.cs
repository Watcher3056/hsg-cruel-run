﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HSG
{
    public partial class SM_GameMode
    {
        private void SetupStateVertical()
        {
            StateDefault state = new StateDefault();
            state.OnStart = StartStateVertical;
            state.OnEnd = EndStateVertical;

            statesMap.Add(GameMode.Vertical, state);
        }
        private void StartStateVertical()
        {
            //Screen.orientation = ScreenOrientation.Portrait;
        }
        private void EndStateVertical()
        {
            float ratio = (ProcessingTrackManager.Default.Player.dataPlayer.curSpeed -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedVertical) /
                (ProcessingTrackManager.Default.dataTrackManager.maxSpeedVertical -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedVertical);
            ProcessingTrackManager.Default.Player.dataPlayer.curSpeed =
                (ProcessingTrackManager.Default.dataTrackManager.maxSpeedHorizontal -
                ProcessingTrackManager.Default.dataTrackManager.minSpeedHorizontal) *
                ratio + ProcessingTrackManager.Default.dataTrackManager.minSpeedHorizontal;
        }
    }
}
