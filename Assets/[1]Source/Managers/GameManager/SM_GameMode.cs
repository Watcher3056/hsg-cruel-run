﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public partial class SM_GameMode : ITick, IDisposable
    {
        public enum GameMode { None, Vertical, Horizontal }

        public Dictionary<GameMode, StateDefault> statesMap = new Dictionary<GameMode, StateDefault>();

        public static event Action OnOrientationChanged = delegate { };
        public GameMode CurState { get { return curState; } set { curState = value; CheckState(); } }
        private GameMode curState = GameMode.None;
        private GameMode PrevState { get; set; }

        public SM_GameMode()
        {

        }
        public void Setup()
        {
            ProcessingUpdate.Default.Add(this);

            statesMap = new Dictionary<GameMode, StateDefault>();
            SetupStateVertical();
            SetupStateHorizontal();
        }

        private void CheckState()
        {
            if (PrevState != CurState)
            {
                Debug.Log(this + " CurState: " + CurState + " PrevState: " + PrevState);
                if (statesMap.ContainsKey(CurState) &&
                    statesMap[CurState].Condition((int)PrevState))
                {
                    if (statesMap.ContainsKey(PrevState))
                        statesMap[PrevState].OnEnd();
                    statesMap[CurState].OnStart();

                    PrevState = CurState;
                    OnOrientationChanged.Invoke();
                }
                else
                    CurState = PrevState;
            }
        }

        public void Tick()
        {
            CheckState();
            statesMap[CurState].Handle();
        }

        public void Dispose()
        {

        }
    }
}
