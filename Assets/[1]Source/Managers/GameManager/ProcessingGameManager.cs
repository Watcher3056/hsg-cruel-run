﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System.Collections;
using System.Threading;
using AppodealAds.Unity.Api;
using SimpleLocalization;
using DG.Tweening;
using UnityEngine.UI;

namespace HSG
{
    public partial class ProcessingGameManager : ProcessingBase, ITick, IMustBeWipedOut
    {
        public static ProcessingGameManager Default => _Default;
        private static ProcessingGameManager _Default;

        public SM_GameMode sm_GameMode;


        public enum GameState { None, Play, Pause, WaitingForStart, PlayerDead }
        public Dictionary<GameState, StateDefault> statesMap = new Dictionary<GameState, StateDefault>();
        public GameState CurState { get { return curState; } set { curState = value; CheckState(); } }
        private GameState curState = GameState.WaitingForStart;
        private GameState PrevState { get; set; }

        public float prevRatio, curScreenWidth, curScreenHeight;

        public bool AutoRotationOn { get { return autoRotationOn; } }
        private bool autoRotationOn;
        public float FixedTimeScale { get { return UnityEngine.Time.fixedDeltaTime; } set { UnityEngine.Time.fixedDeltaTime = 0.02f * value; } }
        public float CurGlobalAnimSpeed { get { return curGlobalAnimSpeed; } set { curGlobalAnimSpeed = value; } }
        private float curGlobalAnimSpeed;

        public ProcessingGameManager()
        {
            _Default = this;
        }
        public void Setup()
        {
            Debug.unityLogger.logEnabled = Debug.isDebugBuild;

            DOTween.Init(true, true, LogBehaviour.ErrorsOnly);
            SM_GameMode.OnOrientationChanged += HandleOnOrientationChanged;
            Application.wantsToQuit += OnApplicationQuit;
            ComponentUIManager.OnApplicationFocused += HandleOnApplicationFocused;
            Application.targetFrameRate = 60;
            curGlobalAnimSpeed = 1f;
            Input.multiTouchEnabled = false;
            //Screen.SetResolution(Screen.width / 2, Screen.height / 2, true);
            Screen.orientation = ScreenOrientation.Portrait;
            Screen.autorotateToLandscapeRight = false;
            Screen.autorotateToLandscapeLeft = false;
            Screen.autorotateToPortrait = false;
            Screen.autorotateToPortraitUpsideDown = false;

            Debug.Log("GPGS: Starting...");
            PlayGamesPlatform.DebugLogEnabled = true;//Debug.isDebugBuild;
            if (Application.platform == RuntimePlatform.Android)
            {
                PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                .EnableSavedGames()
                .Build();
                PlayGamesPlatform.InitializeInstance(config);
                PlayGamesPlatform.Activate();
            }
            CheckGPGS();

            LocalizationManager.Read();
            if (Application.systemLanguage == SystemLanguage.Russian ||
                Application.systemLanguage == SystemLanguage.Belarusian ||
                Application.systemLanguage == SystemLanguage.Ukrainian)
                LocalizationManager.Language = "Russian";
            else
                LocalizationManager.Language = "English";

            statesMap = new Dictionary<GameState, StateDefault>();
            SetupStatePlay();
            SetupStatePause();
            SetupStateWaitingForStart();
            SetupStatePlayerDead();

            sm_GameMode = new SM_GameMode();
            sm_GameMode.Setup();

            ProcessingDeferredOperation.Default.Add(() =>
            {
                sm_GameMode.CurState = SM_GameMode.GameMode.Vertical;
                HandleOnApplicationFocused(true);
            }, true);
            ProcessingDeferredOperation.Default.Add(() =>
            {
                if (autoRotationOn)
                    UpdateOrientation();
            }, triggerPeriod: 1.5f);
        }
        protected override void OnDispose()
        {
            sm_GameMode.Dispose();
            sm_GameMode = null;
            if (CurState == GameState.Pause)
            {
                ProcessingGameManager.Default.CurGlobalAnimSpeed = animSpeed;
                ProcessingGameManager.Default.FixedTimeScale = fixedTimeScale / 0.02f;
                Homebrew.Time.Default.timeScale = timeScale;
            }
            SM_GameMode.OnOrientationChanged -= HandleOnOrientationChanged;
            Application.wantsToQuit -= OnApplicationQuit;
            ComponentUIManager.OnApplicationFocused -= HandleOnApplicationFocused;
            //Homebrew.Time.Default.timeScale = 1f;
        }
        //Bullshit
        public void SlowMoveAfterPlayerDead()
        {
            void Action()
            {
                if (ProcessingTrackManager.Default.curDistanceTraveled <= ProcessingTrackManager.Default.dataTrackManager.distanceWithoutAds)
                    ComponentUIManager.Default.ToggleBlackScreen(true, 0.5f);
                ProcessingDeferredOperation.Default.Add(() =>
                {
                    ComponentPauseMenu.Default.pauseButton.GetComponent<Button>().interactable = true;
                    Homebrew.Time.Default.timeScale = 1f;
                    CurGlobalAnimSpeed = 1f;

                    if (ProcessingTrackManager.Default.curDistanceTraveled <= ProcessingTrackManager.Default.dataTrackManager.distanceWithoutAds)
                        ProcessingDeferredOperation.Default.Add(() =>
                        {
                            ProcessingTrackManager.Default.Restart();
                            CurState = GameState.PlayerDead;
                            CurState = GameState.Play;
                            ComponentUIManager.Default.ToggleBlackScreen(false, 1f);
                        }, true, false, 1f);
                    else
                        CurState = GameState.PlayerDead;
                }, true, false, 2f);
            }
            ComponentPauseMenu.Default.pauseButton.GetComponent<Button>().interactable = false;
            ComponentControls.Default.ToggleWindow(false);
            Homebrew.Time.Default.timeScale = 0.2f;
            CurGlobalAnimSpeed = 0.2f;

            Homebrew.Timer.Add(ProcessingTrackManager.Default.dataTrackManager.deadCamTiming * CurGlobalAnimSpeed, Action);
        }
        public void Tick()
        {
            CheckState();
            statesMap[CurState].Handle();

            //Screen resolution ->
            float curRatio = sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ? (float)Screen.height / Screen.width : (float)Screen.width / Screen.height;
            if (prevRatio != curRatio)
            {
                Debug.Log(String.Format("Screen Resolution Changed: {0} {1}", Screen.width, Screen.height));
                curScreenWidth = Screen.width;
                curScreenHeight = Screen.height;
                DataGameSettings.Default.AdaptScreenResolution();
            }
            prevRatio = curRatio;
            //Screen resolution <-
        }

        private Vector3 gyro;
        private ScreenOrientation curOrientation;
        public void UpdateOrientation()
        {
            gyro = Input.acceleration;
            gyro.x = (float)Math.Round((decimal)(gyro.x), 1);
            gyro.y = (float)Math.Round((decimal)(gyro.y), 1);
            gyro.z = (float)Math.Round((decimal)(gyro.z), 1);
            curOrientation = Screen.orientation;


            if (gyro.x >= 0.8f) // Tilt Right
            {
                if (curOrientation == ScreenOrientation.Portrait)
                    curOrientation = ScreenOrientation.LandscapeRight;
                else if (curOrientation == ScreenOrientation.LandscapeRight)
                    curOrientation = ScreenOrientation.PortraitUpsideDown;
                else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                    curOrientation = ScreenOrientation.LandscapeLeft;
                else if (curOrientation == ScreenOrientation.LandscapeLeft)
                    curOrientation = ScreenOrientation.Portrait;
            }
            else if (gyro.x <= -0.8f) // Tilt Left
            {
                if (curOrientation == ScreenOrientation.Portrait)
                    curOrientation = ScreenOrientation.LandscapeLeft;
                else if (curOrientation == ScreenOrientation.LandscapeLeft)
                    curOrientation = ScreenOrientation.PortraitUpsideDown;
                else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                    curOrientation = ScreenOrientation.LandscapeRight;
                else if (curOrientation == ScreenOrientation.LandscapeRight)
                    curOrientation = ScreenOrientation.Portrait;
            }
            else if (gyro.y >= 0.6f) // Tilt Forward
            {
                if (curOrientation == ScreenOrientation.Portrait)
                    curOrientation = ScreenOrientation.PortraitUpsideDown;
                else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                    curOrientation = ScreenOrientation.Portrait;
                else if (curOrientation == ScreenOrientation.LandscapeRight)
                    curOrientation = ScreenOrientation.LandscapeLeft;
                else if (curOrientation == ScreenOrientation.LandscapeLeft)
                    curOrientation = ScreenOrientation.LandscapeRight;
            }
            Screen.orientation = curOrientation;
            ProcessingGameManager.Default.sm_GameMode.CurState = curOrientation == ScreenOrientation.Portrait ||
                curOrientation == ScreenOrientation.PortraitUpsideDown ?
                SM_GameMode.GameMode.Vertical : SM_GameMode.GameMode.Horizontal;
        }

        public void CheckGPGS()
        {
            if (!Social.localUser.authenticated && Application.platform == RuntimePlatform.Android)
                Social.localUser.Authenticate(AuthenticateCallback);
            else
                ProcessingSaveLoad.Default.SyncDataWithCloud();
            void AuthenticateCallback(bool success)
            {
                if (success)
                {
                    Debug.Log("GPGS: AUTHENTIFICATED!");
                    ((PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.TOP);
                    ProcessingSaveLoad.Default.SyncDataWithCloud();
                }
                else
                {
                    Debug.Log("GPGS: AUTHENTIFICATE FAILURE!");
                }
            }
        }
        public void TryRestartForRewardedVideo(bool success)
        {
            void OnSuccess()
            {
                ProcessingTrackManager.Default.Countinue();
                CurState = GameState.Play;
            }
#if !UNITY_EDITOR
            if (success)
                ProcessingADSManager.Default.ShowRewardedVideo(OnSuccess);
            return;
#endif
            OnSuccess();
        }
        private void CheckState()
        {
            if (PrevState != CurState)
            {
                Debug.Log(this + " CurState: " + CurState + " PrevState: " + PrevState);
                if (statesMap.ContainsKey(CurState) &&
                    statesMap[CurState].Condition((int)PrevState))
                {
                    if (statesMap.ContainsKey(PrevState))
                        statesMap[PrevState].OnEnd();
                    statesMap[CurState].OnStart();

                    PrevState = CurState;
                }
                else
                    CurState = PrevState;
            }
        }
        /*
        private void SetGlobalAnimationSpeed(float value)
        {
            List<Animator> animators = new List<Animator>(GameObject.FindObjectsOfType<Animator>());

            for (int i = 0; i < animators.Count; i++)
                animators[i].speed = value;
            if (ProcessingTrackManager.Default.Player != null && ProcessingTrackManager.Default.Player.CurAction != BehaviorPlayerController.State.Glide)
                ProcessingTrackManager.Default.Player.dataPlayer.animator.speed = 2f;
            curGlobalAnimSpeed = value;
        }*/
        private bool OnApplicationQuit()
        {
            PlayGamesPlatform.Instance.SignOut();
            return true;
        }
        private void HandleOnOrientationChanged()
        {
            Debug.Log(" Cur Orientation: " + Enum.GetName(typeof(DeviceOrientation), Input.deviceOrientation) +
            " isAutoRotationEnabled: " + autoRotationOn);
        }
        private void HandleOnApplicationFocused(bool focused)
        {
            if (focused) autoRotationOn = DeviceAutoRotationIsOn();
        }
        private bool DeviceAutoRotationIsOn()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
    using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var context = actClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass systemGlobal = new AndroidJavaClass("android.provider.Settings$System");
            var rotationOn = systemGlobal.CallStatic<int>("getInt", context.Call<AndroidJavaObject>("getContentResolver"), "accelerometer_rotation");
 
            return rotationOn==1;
        }
#endif
            return true;
        }
    }
}
/* Do u like it?
 * Likes: 1
                                        ,   ,
                                        $,  $,     ,
                                         "ss.$ss. .s'
                                ,     .ss$$$$$$$$$$s,
                                $. s$$$$$$$$$$$$$$`$$Ss
                                "$$$$$$$$$$$$$$$$$$o$$$       ,
                               s$$$$$$$$$$$$$$$$$$$$$ $$$s,  ,s
                              s$$$$$$$$$"$$$$$$""""$$$$$$"$$$$$,
                              s$$$$$$$$$$s""$$$$ssssss"$$$$$$$$ "
                             s$$$$$$$$$$'             `"""ss"$"$s""
                             s$$$$$$$$$$,              `"""""$  .s$$ s
                             s$$$$$$$$$$$$s,...               `s$$'  `
                         `ssss$$$$$$$$$$$$$$$$$$$$####s.     .$$" $.   , s 
                           `""""$$$$$$$$$$$$$$$$$$$$#####$$$$$$"     $.$'
                                 "$$$$$$$$$$$$$$$$$$$$ $####s""     .$$$|
                                  "$$$$$$$$$$$$$$$$$$$$$$$$##s    .$$" $
                                   $$""$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" `
                                  $$"  "$"$$$$$$$$$$$$$$$$$$$$S""""'
                             ,   ,"     '  $$$$$$$$$$$$$$$$####s
                              $.          .s$$$$$$$$$$$$$$$$$####"
                 ,           "$s.   ..ssS$$$$$$$$$$$$$$$$$$$####"
                  $           .$$$S$$$$$$$$$$$$$$$$$$$$$$$$#####"
                 Ss     ..sS$$$$$$$$$$$$$$$$$$$$$$$$$$$######""
                   "$$sS$$$$$$$$$$$$$$$$$$$$$$$$$$$########"
           ,      s$$$$$$$$$$$$$$$$$$$$$$$$#########""'
           $    s$$$$$$$$$$$$$$$$$$$$$ #######""'      s'         ,
           $$..$$$$$$$$$$$$$$$$$$######"'       ....,$$....    ,$
            "$$$$$$$$$$$$$$$######"' ,      .sS$$$$$$$$$$$$$$$$s$$
              $$$$$$$$$$$$#####"     $, .s$$$$$$$$$$$$$$$$$$$$$$$$s.
   )          $$$$$$$$$$$#####'      `$$$$$$$$ $###########$$$$$$$$$$$.
  ((          $$$$$$$$$$$#####       $$$$$$$$###"       "####$$$$$$$$$$
  ) \         $$$$$$$$$$$$####.     $$$$$ $###"             "###$$$$$$$$$   s'
 (   )        $$$$$$$$$$$$$####.   $$$$$###"                ####$$$$$$$$s$$'
 )  ( (       $$"$$$$$$$ $$$$#####.$$$$$###'                .###$$$$$$$$$$"
 (  )  )   _,$"   $$$$$$$$$$$$######.$$##'                .###$$$$$$$$$$
 ) (  ( \.          "$$$$$$$$$$$$$#######,,,.          ..####$$$$$$$$$$$"
(   )$ )  )        ,$$$$$$$$$$$$$$$$$$####################$$$$$$$$$$$"
(   ($$  ( \     _sS"  `"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$S$$,
 )   )$$$s ) )  .      .   `$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"'  `$$
  (   $$$Ss/  .$,    .$,,s$$$$$$##S$$$$$$$$$$$$$$$$$$$$$$$$S""        '
    \)_$$$$$$$$$$$$$$$$$$$$$$$##"  $$        `$$.        `$$.
        `"S$$$$$ $$$$$$$$$$$$#"      $          `$          `$
            `"""""""""""""'         '           '           ' 
 */
