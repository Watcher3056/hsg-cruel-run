﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.Events;

namespace HSG
{
    public class ProcessingADSManager : ProcessingBase, IRewardedVideoAdListener, IInterstitialAdListener,
        ITick, IMustBeWipedOut
    {
        public delegate void ADSLoadedHandler(bool success);

        public static ProcessingADSManager Default => _Default;
        public DateTime lastAdShownTime;
        public const int showAdEver = 600;

        private static ProcessingADSManager _Default;

        private Action _onRewardedFinished;
        private Action _onInterstitialClosed;
        private Action<bool> _onRewardedVideoLoaded, _onInterstitialLoaded;
        private bool rewardedVideoCaching, interstitialCaching;

        public ProcessingADSManager()
        {
            _Default = this;
        }
        public void Setup()
        {
            Debug.Log("Appodeal API initializing...");
            string appKey = "25874dbc35657e89f772a48e2d1ad59cb754d4b94a4a30b8";
            Appodeal.setAutoCache(Appodeal.INTERSTITIAL, false);

            string curArch = DataGameSettings.GetArchitecture();
            Debug.Log("Current Architecture: " + curArch);
            if (curArch.Contains("64") && (curArch.Contains("arm") || curArch.Contains("arch")))
                Appodeal.disableNetwork("adcolony");
            
            if (Debug.isDebugBuild)
                Appodeal.setLogLevel(Appodeal.LogLevel.Debug);
            else
                Appodeal.setLogLevel(Appodeal.LogLevel.None);

            //Appodeal.disableWriteExternalStoragePermissionCheck();
            Appodeal.disableLocationPermissionCheck();
            Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.REWARDED_VIDEO, PlayerPrefs.GetInt("result_gdpr_sdk", 0) != 0);
            Appodeal.setRewardedVideoCallbacks(this);
            Appodeal.setInterstitialCallbacks(this);

            lastAdShownTime = DateTime.Now - new TimeSpan(0, 0, showAdEver / 2);
        }

        public void Tick()
        {
            if (ProcessingADSManager.Default.lastAdShownTime + new TimeSpan(0, 0, ProcessingADSManager.showAdEver) < DateTime.Now &&
                !Appodeal.isLoaded(Appodeal.INTERSTITIAL) && !interstitialCaching)
                CacheInterstitial((bool ok) => { });
        }
        public void CacheRewardedVideo(Action<bool> callback)
        {
            if (rewardedVideoCaching)
                return;
            if (!Appodeal.isPrecache(Appodeal.REWARDED_VIDEO) || !Appodeal.isLoaded(Appodeal.REWARDED_VIDEO))
            {
                Debug.Log("Cache Rewarded Video Enter");
                _onRewardedVideoLoaded = callback;
                ComponentUIManager.Default.adLoaingIndicator.SetActive(true);
                Appodeal.cache(Appodeal.REWARDED_VIDEO);
                rewardedVideoCaching = true;
                Debug.Log("Cache Rewarded Video Exit");
            }
            else
                callback.Invoke(true);
        }
        public void CacheInterstitial(Action<bool> callback)
        {
            if (interstitialCaching)
                return;
            if (!Appodeal.isPrecache(Appodeal.INTERSTITIAL) || !Appodeal.isLoaded(Appodeal.INTERSTITIAL))
            {
                Debug.Log("Cache Interstitial Enter");
                _onInterstitialLoaded = callback;
                //ComponentUIManager.Default.adLoaingIndicator.SetActive(true);
                Appodeal.cache(Appodeal.INTERSTITIAL);
                interstitialCaching = true;
                Debug.Log("Cache Interstitial Exit");
            }
            else
                callback.Invoke(true);
        }
        public void ShowRewardedVideo(Action onSuccess)
        {
            Debug.Log("Show Rewarded Video Enter");
            _onRewardedFinished = onSuccess;
            Appodeal.show(Appodeal.REWARDED_VIDEO);
            Debug.Log("Show Rewarded Video Exit");
        }
        public void ShowInterstitial(Action onSuccess)
        {
            Debug.Log("Show Interstitial Enter");
            _onInterstitialClosed = onSuccess;
            Appodeal.show(Appodeal.INTERSTITIAL);
            Debug.Log("Show Interstitial Exit");
        }

        public void onRewardedVideoLoaded(bool precache)
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Loaded Rewarded Video Enter " + precache);
                ComponentUIManager.Default.adLoaingIndicator.SetActive(false);
                rewardedVideoCaching = false;
                if (_onRewardedVideoLoaded != null)
                    _onRewardedVideoLoaded.Invoke(Appodeal.isLoaded(Appodeal.REWARDED_VIDEO));
                _onRewardedVideoLoaded = null;
                Debug.Log("Loaded Rewarded Video Exit " + precache);
            }, true);
        }

        public void onRewardedVideoFailedToLoad()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Failed To Load Rewarded Video Enter");
                onRewardedVideoLoaded(false);
                Debug.Log("Failed To Load Rewarded Video Exit");
            }, true);
        }

        public void onRewardedVideoShown()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Shown Rewarded Video Enter");
                lastAdShownTime = DateTime.Now;
                Debug.Log("Shown Rewarded Video Exit");
            }, true);
        }

        public void onRewardedVideoFinished(double amount, string name)
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {

            }, true);
        }

        public void onRewardedVideoClosed(bool finished)
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                if (finished)
                    _onRewardedFinished.Invoke();
            }, true);
        }

        public void onRewardedVideoExpired()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {

            }, true);
        }

        public void onRewardedVideoClicked()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {

            }, true);
        }

        public void onInterstitialLoaded(bool isPrecache)
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Loaded Interstitial Enter");
                ComponentUIManager.Default.adLoaingIndicator.SetActive(false);
                interstitialCaching = false;
                if (_onInterstitialLoaded != null)
                    _onInterstitialLoaded.Invoke(Appodeal.isLoaded(Appodeal.INTERSTITIAL));
                _onInterstitialLoaded = null;
                Debug.Log("Loaded Interstitial Exit");
            }, true);
        }

        public void onInterstitialFailedToLoad()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Failed To Load Interstitial Enter");
                onInterstitialLoaded(false);
                Debug.Log("Failed To Load Interstitial Exit");
            }, true);
        }

        public void onInterstitialShown()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                Debug.Log("Shown Interstitial Enter");
                lastAdShownTime = DateTime.Now;
                Debug.Log("Shown Interstitial Exit");
            }, true);
        }

        public void onInterstitialClosed()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
                _onInterstitialClosed.Invoke();
            }, true);
        }

        public void onInterstitialClicked()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
            }, true);
        }

        public void onInterstitialExpired()
        {
            ProcessingDeferredOperation.Default.Add(() =>
            {
            }, true);
        }
    }
}
