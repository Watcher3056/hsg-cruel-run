﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;


namespace HSG
{
    public class ProcessingCameraManager : ProcessingBase, IMustBeWipedOut, ITick
    {
        public static ProcessingCameraManager Default => _default;
        private static ProcessingCameraManager _default;
        public bool AutoRotationOn { get { return autoRotationOn; } }
        private bool autoRotationOn;
        // Setup
        private const float updateFrustumEvery = 0.2f;
        //
        private float updateTimeBuffer = 0f;
        private Dictionary<Camera, Plane[]> dicCameraFrustum = new Dictionary<Camera, Plane[]>();

        public ProcessingCameraManager()
        {
            _default = this;
        }
        public void Setup()
        {
            SM_GameMode.OnOrientationChanged += HandleOnOrientationChanged;
            ComponentUIManager.OnApplicationFocused += HandleOnApplicationFocused;
        }
        protected override void OnDispose()
        {
            SM_GameMode.OnOrientationChanged -= HandleOnOrientationChanged;
            ComponentUIManager.OnApplicationFocused -= HandleOnApplicationFocused;
        }
        public void Tick()
        {
            UpdateCameraFrustumPlanes();
            //UpdateOrientation();
        }

        KeyValuePair<Camera, Plane[]> keyValuePair;
        private void UpdateCameraFrustumPlanes()
        {
            updateTimeBuffer += UnityEngine.Time.unscaledDeltaTime;
            if (updateTimeBuffer > updateFrustumEvery)
            {
                for (int i = 0; i < dicCameraFrustum.Count; i++)
                {
                    keyValuePair = dicCameraFrustum.ElementAt(i);
                    dicCameraFrustum[keyValuePair.Key] = GeometryUtility.CalculateFrustumPlanes(keyValuePair.Key);
                }
                updateTimeBuffer = 0f;
            }
        }

        private enum Tilt { None, Left, Right, Forward }
        private Tilt curTilt;
        private Vector3 gyro;
        private ScreenOrientation curOrientation;
        private float curTimeBuffer;
        private const float requiredTimeBuffer = 2f;
        public void UpdateOrientation()
        {
            curTimeBuffer += UnityEngine.Time.unscaledDeltaTime;

            gyro = Input.acceleration;
            gyro.x = (float)Math.Round((decimal)(gyro.x), 1);
            gyro.y = (float)Math.Round((decimal)(gyro.y), 1);
            gyro.z = (float)Math.Round((decimal)(gyro.z), 1);
            curOrientation = Screen.orientation;


            if (gyro.x >= 0.8f) // Tilt Right
            {
                if (curTilt != Tilt.Left)
                {
                    curTilt = Tilt.Left;
                    curTimeBuffer = 0f;
                }
                if (curTimeBuffer >= requiredTimeBuffer)
                {
                    if (curOrientation == ScreenOrientation.Portrait)
                        curOrientation = ScreenOrientation.LandscapeRight;
                    else if (curOrientation == ScreenOrientation.LandscapeRight)
                        curOrientation = ScreenOrientation.PortraitUpsideDown;
                    else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                        curOrientation = ScreenOrientation.LandscapeLeft;
                    else if (curOrientation == ScreenOrientation.LandscapeLeft)
                        curOrientation = ScreenOrientation.Portrait;
                }
            }
            else if (gyro.x <= -0.8f) // Tilt Left
            {
                if (curTilt != Tilt.Right)
                {
                    curTilt = Tilt.Right;
                    curTimeBuffer = 0f;
                }
                if (curTimeBuffer >= requiredTimeBuffer)
                {
                    if (curOrientation == ScreenOrientation.Portrait)
                        curOrientation = ScreenOrientation.LandscapeLeft;
                    else if (curOrientation == ScreenOrientation.LandscapeLeft)
                        curOrientation = ScreenOrientation.PortraitUpsideDown;
                    else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                        curOrientation = ScreenOrientation.LandscapeRight;
                    else if (curOrientation == ScreenOrientation.LandscapeRight)
                        curOrientation = ScreenOrientation.Portrait;
                }
            }
            else if (gyro.y >= 0.6f) // Tilt Forward
            {
                if (curTilt != Tilt.Forward)
                {
                    curTilt = Tilt.Forward;
                    curTimeBuffer = 0f;
                }
                if (curTimeBuffer >= requiredTimeBuffer)
                {
                    if (curOrientation == ScreenOrientation.Portrait)
                        curOrientation = ScreenOrientation.PortraitUpsideDown;
                    else if (curOrientation == ScreenOrientation.PortraitUpsideDown)
                        curOrientation = ScreenOrientation.Portrait;
                    else if (curOrientation == ScreenOrientation.LandscapeRight)
                        curOrientation = ScreenOrientation.LandscapeLeft;
                    else if (curOrientation == ScreenOrientation.LandscapeLeft)
                        curOrientation = ScreenOrientation.LandscapeRight;
                }
            }
            else
                curTilt = Tilt.None;
            Screen.orientation = curOrientation;
            ProcessingGameManager.Default.sm_GameMode.CurState = curOrientation == ScreenOrientation.Portrait ||
                curOrientation == ScreenOrientation.PortraitUpsideDown ?
                SM_GameMode.GameMode.Vertical : SM_GameMode.GameMode.Horizontal;
        }
        public void ChangeOrientation()
        {
            curTimeBuffer = 0f;
            ProcessingGameManager.Default.sm_GameMode.CurState =
                ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ?
                SM_GameMode.GameMode.Horizontal :
                SM_GameMode.GameMode.Vertical;
            Screen.orientation =
                ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ?
                ScreenOrientation.Portrait : ScreenOrientation.LandscapeRight;
        }
        private void HandleOnApplicationFocused(bool focused)
        {
            //if (focused) autoRotationOn = DeviceAutoRotationIsOn();
        }
        private bool DeviceAutoRotationIsOn()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
    using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var context = actClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass systemGlobal = new AndroidJavaClass("android.provider.Settings$System");
            var rotationOn = systemGlobal.CallStatic<int>("getInt", context.Call<AndroidJavaObject>("getContentResolver"), "accelerometer_rotation");
 
            return rotationOn==1;
        }
#endif
            return true;
        }
        private void HandleOnOrientationChanged()
        {
            Debug.Log(" Cur Orientation: " + Enum.GetName(typeof(DeviceOrientation), Input.deviceOrientation) +
            " isAutoRotationEnabled: " + autoRotationOn);
        }
        public bool IsObjectVisible(Camera camera, Renderer renderer)
        {
            if (!dicCameraFrustum.ContainsKey(camera))
                dicCameraFrustum.Add(camera, GeometryUtility.CalculateFrustumPlanes(camera));
            return GeometryUtility.TestPlanesAABB(dicCameraFrustum[camera], renderer.bounds);
        }
    }
}
