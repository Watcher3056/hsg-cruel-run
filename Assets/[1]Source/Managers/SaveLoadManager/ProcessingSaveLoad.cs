﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using Homebrew;
using Sirenix.Serialization;
using UnityEngine;


namespace HSG
{
    public partial class ProcessingSaveLoad : ProcessingBase, IMustBeWipedOut
    {
        public static ProcessingSaveLoad Default => _default;
        private static ProcessingSaveLoad _default;

        public static event Action onLocalDataUpdated = () => { };

        private const string filenameLocal = "saves.txt";
        private const string filenameCloud = "savesCloud.txt";
        private const string tagVersion = "SaveFileVersion";
        private const string tagUserID = "UserID";
        private const int lastVersion = 1;

        private ISavedGameClient SavedGame;
        private bool alreadyInProcess;
        private bool dataWasLoadedFromCloud;

        public ProcessingSaveLoad()
        {
            _default = this;
            ProcessingGameManager.OnPlayerDead -= SyncDataWithCloud;
            ProcessingGameManager.OnPlayerDead += SyncDataWithCloud;
            ProcessingIAPManager.onItemPurchased -= SyncDataWithCloud;
            ProcessingIAPManager.onItemPurchased += SyncDataWithCloud;
        }
        protected override void OnDispose()
        {
            ProcessingGameManager.OnPlayerDead -= SyncDataWithCloud;
            ProcessingIAPManager.onItemPurchased -= SyncDataWithCloud;
        }
        public void Setup()
        {
            SetupMigration();

            TryMigrate();
        }
        public static void SavePP<T>(string key, T value)
        {
            ES2.Save(value, key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs });
        }
        public static T LoadPP<T>(string key, T defaultValue)
        {
             return ExistsPP(key) ? ES2.Load<T>(key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs }) : defaultValue;
        }
        public static bool ExistsPP(string key)
        {
            return ES2.Exists(key, new ES2Settings { saveLocation = ES2Settings.SaveLocation.PlayerPrefs });
        }
        public static void Save<T>(string key, T value)
        {
            ES2.Save<T>(value, filenameLocal + "?tag=" + key);
        }
        public static T Load<T>(string key)
        {
            return ES2.Load<T>(filenameLocal + "?tag=" + key);
        }
        public static T Load<T>(string key, T defaultValue)
        {
            return Exists(key) ? ES2.Load<T>(filenameLocal + "?tag=" + key) : defaultValue;
        }
        public static bool Exists(string key)
        {
            return ES2.Exists(filenameLocal + "?tag=" + key);
        }
        public static void CleanSaves()
        {
            ES2.Delete(filenameLocal);
            PlayerPrefs.DeleteAll();
            Debug.Log(Default + "::: Local saves was deleted!");
        }

        #region Saved Games
        private static byte[] GameDataToBytes(ES2Data gameData)
        {
            return SerializationUtility.SerializeValue(gameData, DataFormat.Binary);
        }
        private static ES2Data BytesToGameData(byte[] gameData)
        {
            return SerializationUtility.DeserializeValue<ES2Data>(gameData, DataFormat.Binary);
        }
        public void SyncDataWithCloud()
        {
            Debug.Log(this + "::: Sync data with cloud start...");
            if (PlayGamesPlatform.Instance != null && Application.platform == RuntimePlatform.Android)
                SavedGame = PlayGamesPlatform.Instance.SavedGame;

            if (dataWasLoadedFromCloud)
                SaveDataToCloud();
            else
                LoadDataFromCloud();
        }
        private void LoadDataFromCloud()
        {
            if (SavedGame == null /*|| !Social.localUser.authenticated */|| alreadyInProcess)
                return;
            alreadyInProcess = true;
            SavedGame.OpenWithAutomaticConflictResolution(filenameCloud,
                DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnLoadDataFromCloud);
        }
        private void SaveDataToCloud()
        {
            if (SavedGame == null /*|| !Social.localUser.authenticated */|| alreadyInProcess)
                return;
            alreadyInProcess = true;
            SavedGame.OpenWithAutomaticConflictResolution(filenameCloud,
                DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, OnSaveDataToCloud);
        }
        private void OnLoadDataFromCloud(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            void OnSavedGameDataRead(SavedGameRequestStatus _status, byte[] savedData)
            {
                Debug.Log(this + "::: Load data from cloud status: " +
                    Enum.GetName(typeof(SavedGameRequestStatus), _status));
                alreadyInProcess = false;
                if (_status != SavedGameRequestStatus.Success)
                    return;
                dataWasLoadedFromCloud = true;
                if (savedData != null && savedData.Length > 0)
                {
                    Debug.Log(this + "::: Loaded from cloud " + savedData.Length + " bytes of data");
                    SaveDataLocal(BytesToGameData(savedData));
                }
                DataGameSession.Default.SyncProgressWithCloud();
            }
            Debug.Log(this + "::: Load data from cloud. Saves opening status: " +
                Enum.GetName(typeof(SavedGameRequestStatus), status));
            if (status != SavedGameRequestStatus.Success)
            {
                alreadyInProcess = false;
                return;
            }
            SavedGame.ReadBinaryData(game, OnSavedGameDataRead);
        }
        private void OnSaveDataToCloud(SavedGameRequestStatus status, ISavedGameMetadata game)
        {
            void OnSavedGameDataWritten(SavedGameRequestStatus _status, ISavedGameMetadata _game)
            {
                Debug.Log(this + "::: Commit data to cloud status: " +
                    Enum.GetName(typeof(SavedGameRequestStatus), _status));
                alreadyInProcess = false;
                if (_status != SavedGameRequestStatus.Success)
                    return;
                DataGameSession.Default.SyncProgressWithCloud();
            }
            Debug.Log(this + "::: Commit data to cloud. Saves opening status: " +
                Enum.GetName(typeof(SavedGameRequestStatus), status));
            if (status != SavedGameRequestStatus.Success)
            {
                alreadyInProcess = false;
                return;
            }
            //encoding to byte array
            byte[] dataToSave = GameDataToBytes(ES2.LoadAll(filenameLocal));
            Debug.Log(this + "::: Trying to commit " + dataToSave.Length + " bytes of data to cloud");
            //updating metadata with new description
            SavedGameMetadataUpdate update = new SavedGameMetadataUpdate.Builder().Build();
            //uploading data to the cloud
            SavedGame.CommitUpdate(game, update, dataToSave,
                OnSavedGameDataWritten);
        }
        private void SaveDataLocal(ES2Data data)
        {
            Debug.Log(this + "::: Overwriting local data start...");
            Dictionary<string, object> dic = data.loadedData;
            List<string> keys = dic.Keys.ToList();

            using (ES2Writer writer = ES2Writer.Create(filenameLocal))
            {
                // Write our data to the file.
                for (int i = 0; i < keys.Count; i++)
                {
                    writer.Write(dic[keys[i]], keys[i]);
                    Debug.Log(this + "::: Overwrite key " + keys[i] + " with value: " + dic[keys[i]]);
                }
                // Remember to save when we're done.
                writer.Save();
            }
            Debug.Log(this + "::: Overwriting local data end!");
            TryMigrate();
        }
        #endregion /Saved Games

        #region Tests
        public static void TestMigration()
        {
            ProcessingSaveLoad.Save(ProcessingSaveLoad.tagVersion, 0);

            PlayerPrefs.SetInt(DataGameMain.Default.locations[1].localizationNameKey, 1);
            PlayerPrefs.SetInt(DataGameMain.Default.characters[1].localizationNameKey, 1);
            PlayerPrefs.SetInt(DataGameMain.Default.characters[1].skins[0].unique, 1);
            PlayerPrefs.SetInt(DataGameMain.Default.characters[1].localizationNameKey + ".NoteGatherProgress", 10);
            PlayerPrefs.SetInt("GodTears", 1000);

            ProcessingSaveLoad.Default.TryMigrate();

            if (!DataGameMain.Default.locations[1].IsUnlocked ||
                !DataGameMain.Default.characters[1].IsUnlocked ||
                DataGameMain.Default.characters[1].UnlockedNotesCount() != 10 ||
                DataGameSession.Default.GodTearsCount != 1000)
                Debug.LogError("Migration test was failed!");
        }
        public static void TestSerialization()
        {
            string filenameTest = "test.txt";
            Guid guid = Guid.NewGuid();

            ES2.Save(guid.ToString(), filenameTest + "?tag=guid");
            if (BytesToGameData(GameDataToBytes(ES2.LoadAll(filenameTest))).Load<string>("guid") == guid.ToString())
                Debug.Log("Test Success!");
            else
                Debug.Log("Test Failed!");
        }
        public static void TestSyncronization()
        {
            ES2Data originalData = ES2.LoadAll(filenameLocal);

            Debug.Log("::: Overwriting local originalData start...");
            Dictionary<string, object> dicOriginal = originalData.loadedData;
            List<string> keys = dicOriginal.Keys.ToList();

            using (ES2Writer writer = ES2Writer.Create(filenameLocal))
            {
                // Write our data to the file.
                for (int i = 0; i < keys.Count; i++)
                {
                    writer.Write(dicOriginal[keys[i]], keys[i]);
                    Debug.Log("::: Overwrite key " + keys[i] + " with value: " + dicOriginal[keys[i]]);
                }
                // Remember to save when we're done.
                writer.Save();
            }
            Debug.Log("::: Overwriting local originalData end!");

            Dictionary<string, object> dicNew = ES2.LoadAll(filenameLocal).loadedData;

            for (int i = 0; i < keys.Count; i++)
            {
                if (dicNew[keys[i]].ToString() != dicOriginal[keys[i]].ToString())
                    Debug.LogError("Key: " + keys[i] + " Original value: " + dicOriginal[keys[i]] + " Writed value: " + dicNew[keys[i]]);
            }
            Debug.Log("Sync test ended!");
        }
        #endregion /Tests
    }
}
