﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;


namespace HSG
{
    public partial class ProcessingSaveLoad
    {
        private delegate bool MigrateStage();
        List<MigrateStage> migrationList;
        private void SetupMigration()
        {
            migrationList = new List<MigrateStage>();

            migrationList.Add(MigrateTo1);
        }
        private void TryMigrate()
        {
            Debug.Log(this + "::: Start Migration(Updating save file version).");
            int curVersion = Load(tagVersion, 0);
            Debug.Log(this + "::: Current save file version: " + curVersion + "\n"
                + "Last save file version: " + lastVersion);
            //Launch migration chain
            for (int i = curVersion; i < migrationList.Count; i++)
            {
                if (migrationList[i].Invoke())
                {
                    Debug.Log(this + "::: Migration to version " + (i + 1) + " was successful!");
                    //Update version number of file
                    Save(tagVersion, i + 1);
                }
                else
                    Debug.Log(this + "::: Migration to version " + (i + 1) + " was NOT successful!");
            }
            ProcessingDeferredOperation.Default.Add(() => { onLocalDataUpdated.Invoke(); }, true);
        }
        private bool MigrateTo1()
        {
            if (PlayerPrefs.HasKey("RecordDistance"))
                //Move keys to new place
                Save("RecordDistance", PlayerPrefs.GetInt("RecordDistance", 0));
            if (PlayerPrefs.HasKey("GodTears"))
                Save("GodTears", PlayerPrefs.GetInt("GodTears", 0));
            //Remove old keys
            PlayerPrefs.DeleteKey("RecordDistance");
            PlayerPrefs.DeleteKey("GodTears");
            PlayerPrefs.DeleteKey("SettingsAvaliable");
            PlayerPrefs.DeleteKey("ControlsTimingValue");
            PlayerPrefs.DeleteKey("GammaValue");
            PlayerPrefs.DeleteKey("ControlsType");
            PlayerPrefs.DeleteKey("SoundVolumeMusic");
            PlayerPrefs.DeleteKey("SoundVolumeEffects");

            //Process data of all characters
            for (int i = 0; i < DataGameMain.Default.characters.Count; i++)
            {
                //We can overwrite ONLY if old key exists
                if (PlayerPrefs.HasKey(DataGameMain.Default.characters[i].localizationNameKey))
                {
                    //Move key to new place
                    Save(DataGameMain.Default.characters[i].localizationNameKey,
                            PlayerPrefs.GetInt(DataGameMain.Default.characters[i].localizationNameKey) == 1);
                    //Remove old key
                    PlayerPrefs.DeleteKey(DataGameMain.Default.characters[i].localizationNameKey);

                    //Move key to new place
                    Save(DataGameMain.Default.characters[i].localizationNameKey + ".NoteGatherProgress",
                            PlayerPrefs.GetInt(DataGameMain.Default.characters[i].localizationNameKey + ".NoteGatherProgress", 0));
                    //Remove old key
                    PlayerPrefs.DeleteKey(DataGameMain.Default.characters[i].localizationNameKey + ".NoteGatherProgress");
                }

                //Process data of all skins of all characters
                for (int j = 0; j < DataGameMain.Default.characters[i].skins.Count; j++)
                {
                    //We can overwrite ONLY if old key exists
                    if (!PlayerPrefs.HasKey(DataGameMain.Default.characters[i].skins[j].unique))
                        continue;
                    //Move key to new place
                    Save(DataGameMain.Default.characters[i].skins[j].unique,
                        PlayerPrefs.GetInt(DataGameMain.Default.characters[i].skins[j].unique) == 1);
                    //Remove old key
                    PlayerPrefs.DeleteKey(DataGameMain.Default.characters[i].skins[j].unique);
                }
                //Process data of all notes of all characters
                for (int j = 0; j < DataGameMain.Default.characters[i].notes.Count; j++)
                {
                    //We can overwrite ONLY if old key exists
                    if (!PlayerPrefs.HasKey(DataGameMain.Default.characters[i].localizationNameKey + ".Note" + j))
                        continue;
                    //Move key to new place
                    Save(DataGameMain.Default.characters[i].localizationNameKey + ".Note" + j,
                        PlayerPrefs.GetInt(DataGameMain.Default.characters[i].localizationNameKey + ".Note" + j) == 1);
                    //Remove old key
                    PlayerPrefs.DeleteKey(DataGameMain.Default.characters[i].localizationNameKey + ".Note" + j);
                }
            }
            //Process data of all locations
            for (int i = 0; i < DataGameMain.Default.locations.Count; i++)
            {
                //We can overwrite ONLY if old key exists
                if (!PlayerPrefs.HasKey(DataGameMain.Default.locations[i].localizationNameKey))
                    continue;
                //Move key to new place
                Save(DataGameMain.Default.locations[i].localizationNameKey,
                        PlayerPrefs.GetInt(DataGameMain.Default.locations[i].localizationNameKey) == 1);
                //Remove old key
                PlayerPrefs.DeleteKey(DataGameMain.Default.locations[i].localizationNameKey);
            }

            return true;
        }
    }
}
