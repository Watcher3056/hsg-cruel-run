﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using Sirenix.OdinInspector;

namespace HSG
{
    [Serializable]
    public class DataTrackManager : IData
    {
        [Serializable]
        public class ObstacleSpacing { public float spacing; public int triggerDistance; }

        [FoldoutGroup("Player")]
        public Transform playerSpawnPoint;

        [FoldoutGroup("Speed")]
        public float minSpeedVertical;
        [FoldoutGroup("Speed")]
        public float maxSpeedVertical;
        [FoldoutGroup("Speed")]
        public float minSpeedHorizontal;
        [FoldoutGroup("Speed")]
        public float maxSpeedHorizontal;
        public enum AccelerationMode { ByOneSecond, ByOneMeter }
        [FoldoutGroup("Speed")]
        [EnumToggleButtons]
        public AccelerationMode accelerationMode;
        [FoldoutGroup("Speed")]
        public float accelerationVertical;
        [FoldoutGroup("Speed")]
        public float accelerationHorizontal;

        [FoldoutGroup("Segments")]
        public Transform segmentsHolder;
        [FoldoutGroup("Segments")]
        [MinValue(2)]
        public int maxSegments;

        [FoldoutGroup("Collectable")]
        public Transform collectablesHolder;
        [FoldoutGroup("Collectable")]
        public ComponentCollectableItem godTearsPrefab;
        [FoldoutGroup("Collectable")]
        public ComponentCollectableItem notePrefab;
        [FoldoutGroup("Collectable")]
        public int spawnNotesEver;

#if UNITY_EDITOR
        [FoldoutGroup("Obstacles")]
        public List<ComponentObstacleDefault> testObstacles;
#endif
        [FoldoutGroup("Obstacles")]
        public List<ObstacleSpacing> obstacleSpacing;
        [FoldoutGroup("Obstacles")]
        public int maxObstacles;
        [FoldoutGroup("Obstacles")]
        public Transform obstaclesHolder;
        [FoldoutGroup("Obstacles")]
        public AnimationCurve curveSingleObstacles;
        [FoldoutGroup("Obstacles")]
        public AnimationCurve curveComboObstacles;


        [FoldoutGroup("Other")]
        public int distanceWithoutAds;
        [FoldoutGroup("Other")]
        public int maxDistanceToRevert;
        [FoldoutGroup("Other")]
        public Transform cameraTracker;
        [FoldoutGroup("Other")]
        public float deadCamTiming;

        public void Dispose()
        {

        }

    }
}
