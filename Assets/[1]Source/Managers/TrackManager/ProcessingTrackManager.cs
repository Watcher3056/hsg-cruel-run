﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using static HSG.ComponentObstacleDefault;
using static HSG.ComponentTrackSegment;
using static HSG.DataLocation;
using static HSG.DataTrackManager;

namespace HSG
{
    public class ProcessingTrackManager : ProcessingBase, ITick, IMustBeWipedOut
    {
        public struct KillHistoryItem { public string trapGuid; public int distance; }
        public class TrapsCount { public int count; }

        public static ProcessingTrackManager Default { get; private set; }

        public DataTrackManager dataTrackManager;

        public ActorPlayer Player { get { return actorPlayer; } set { actorPlayer = value; } }
        private ActorPlayer actorPlayer;

        public DataLocation CurLocation
        {
            set
            {
                curLocation = value;
                Restart();
                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.WaitingForStart;
            }
            get
            {
                return curLocation;
            }
        }
        private DataLocation curLocation;
        public DataCharacter CurCharacter
        {
            set
            {
                curCharacter = value;
                CurSkinIndex = 0;
                ProcessingTutorial.Default.CheckNotifications();
            }
            get
            {
                return curCharacter;
            }
        }
        private DataCharacter curCharacter;
        public int CurSkinIndex
        {
            set
            {
                curSkinIndex = value;
                ChangePlayerSkin(curSkinIndex);
            }
            get
            {
                return curSkinIndex;
            }
        }
        private int curSkinIndex;

        public float curDistanceTraveled = 0f;


        private List<KillHistoryItem> trapsIdsPlayerDiedBy;
        private List<ComponentTrackSegment> segments;
        private List<ComponentObstacleDefault> curObstaclesQueue;
        private List<ComponentObstacleDefault> allObstacles;
        private List<ComponentObstacleDefault> possibleObstacles;
        private Dictionary<string, TrapsCount> dictionaryTypeObstaclesCount;
        private Vector3 originPosition = Vector3.zero;
        private Vector2 prevPlayerPos;
        private int positionRevertionCount;
        private bool positionReverted;


        public void Setup(DataTrackManager dataTrackManager)
        {
            Default = this;
            this.dataTrackManager = dataTrackManager;

            trapsIdsPlayerDiedBy = new List<KillHistoryItem>();
            segments = new List<ComponentTrackSegment>(dataTrackManager.segmentsHolder.GetComponentsInChildren<ComponentTrackSegment>());
            curObstaclesQueue = new List<ComponentObstacleDefault>();
            allObstacles = new List<ComponentObstacleDefault>();
            dictionaryTypeObstaclesCount = new Dictionary<string, TrapsCount>();
            possibleObstacles = new List<ComponentObstacleDefault>();

            DataGameMain.Default.locations[0].Unlock();
            curCharacter = DataGameMain.Default.locations[0].characterToUnlock;
            //CurLocation = DataGameMain.Default.locations[0];
        }
        public void Tick()
        {
            if (Player.CurState == ActorPlayer.State.Dead ||
                ProcessingGameManager.Default.CurState != ProcessingGameManager.GameState.Play)
                return;


            if (segments.Count < dataTrackManager.maxSegments)
                CreateNewSegment();
            else
                RemoveUnVisibleSegments();

            if (allObstacles.Count < dataTrackManager.maxObstacles)
                HandleObstacleCreation();
            else
                RemovePassedObstacles();

            if (curSegment != null && curSegment.transform.position.x > dataTrackManager.maxDistanceToRevert)
                RevertPosition();

            Accelerate();

            if (!positionReverted)
                curDistanceTraveled += Vector2.Distance(prevPlayerPos, Player.transform.position);

            prevPlayerPos = Player.transform.position;
            positionReverted = false;
        }

        public void Restart()
        {
            for (int i = 0; i < segments.Count; i++)
            {
                segments[i].HandleDestroyGO();
            }
            segments.Clear();

            CreateNewSegment(false, originPosition);


            ProcessingGameManager.Default.CurGlobalAnimSpeed = dataTrackManager.minSpeedVertical;

            while (allObstacles.Count > 0)
                RemoveObstacle(0);
            while (dataTrackManager.collectablesHolder.childCount > 0)
            {
                dataTrackManager.collectablesHolder.GetChild(0).GetComponent<ComponentCollectableItem>().HandleDestroyGO();
                dataTrackManager.collectablesHolder.GetChild(0).SetParent(null);
            }

            RespawnPlayer(dataTrackManager.playerSpawnPoint.position, 
                ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ? 
                dataTrackManager.minSpeedVertical : dataTrackManager.minSpeedHorizontal);
            DataGameSettings.Default.AdaptScreenResolution();

            positionReverted = true;
            positionRevertionCount = 0;
            curObstaclesQueue = new List<ComponentObstacleDefault>();

            curDistanceTraveled = 0f;
            ComponentCollectableItem.notesSpawnedNow = 0;


            dictionaryTypeObstaclesCount.Clear();
            possibleObstacles = CurLocation.possibleObstacles;
#if UNITY_EDITOR
            if (dataTrackManager.testObstacles.Count > 0)
                possibleObstacles = dataTrackManager.testObstacles;
#endif
            for (int i = 0; i < possibleObstacles.Count; i++)
                dictionaryTypeObstaclesCount.Add(possibleObstacles[i].gameObject.name, new TrapsCount { count = 0 });
        }
        public void Countinue()
        {
            for (int i = 3; i > 0; i--)
                    RemoveObstacle(0);

            ProcessingDeferredOperation.Default.Add(() =>
            {
                RespawnPlayer(Player.transform.position, Player.dataPlayer.curSpeed);
                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Pause;
                ProcessingGameManager.Default.CurState = ProcessingGameManager.GameState.Play;
            }, true);
        }
        public void AddToKillHistory(string trapGuid)
        {
            trapsIdsPlayerDiedBy.Add(new KillHistoryItem { trapGuid = trapGuid, distance = (int)curDistanceTraveled });
            if (trapsIdsPlayerDiedBy.Count > 100)
                trapsIdsPlayerDiedBy.RemoveAt(0);
        }
        public List<KillHistoryItem> GetKillHistory()
        {
            return trapsIdsPlayerDiedBy;
        }
        private void RespawnPlayer(Vector3 position, float speed)
        {
            if (Player != null)
                Player.HandleDestroyGO();

            Player = this.Populate(Pool.None, CurCharacter.skins[CurSkinIndex].prefab, position, parent: dataTrackManager.playerSpawnPoint.parent).GetComponent<ActorPlayer>();
            Player.CurState = ActorPlayer.State.Live;
            Player.gameObject.SetActive(ProcessingGameManager.Default.CurState != ProcessingGameManager.GameState.WaitingForStart);

            dataTrackManager.cameraTracker.GetComponent<ComponentLockPosition>().transformLockTo = Player.transform;

            Player.dataPlayer.curSpeed = speed;
            prevPlayerPos = Player.transform.position;
        }
        private void HandleObstacleCreation()
        {
            if (possibleObstacles.Count == 0)
                return;

            //===========
            ComponentObstacleDefault newObstacle = null;


            float curveSingleValue = dataTrackManager.curveSingleObstacles.Evaluate(curDistanceTraveled % dataTrackManager.curveSingleObstacles.keys[dataTrackManager.curveSingleObstacles.length - 1].time);
            float curveComboValue = dataTrackManager.curveComboObstacles.Evaluate(curDistanceTraveled % dataTrackManager.curveComboObstacles.keys[dataTrackManager.curveComboObstacles.length - 1].time);
            Debug.Log("Single: " + curveSingleValue + " Combo: " + curveComboValue);

            if (curObstaclesQueue.Count > 0)
            {
                newObstacle = curObstaclesQueue[0];
                curObstaclesQueue.RemoveAt(0);
                Debug.Log("Combo Obstacle");
            }
            else if (UnityEngine.Random.Range(0, curveSingleValue) > UnityEngine.Random.Range(0, curveComboValue) || CurLocation.combos.Count == 0)
            {
                newObstacle = possibleObstacles[UnityEngine.Random.Range(0, possibleObstacles.Count)];
                if (newObstacle.maxTraps != 0 &&
                    dictionaryTypeObstaclesCount[newObstacle.gameObject.name].count >= newObstacle.maxTraps)
                    newObstacle = null;
                Debug.Log("Single Obstacle");
            }
            else
            {
                bool _success = false;
                while (!_success)
                {
                    Dictionary<ComboObstacleChance, int> dic = dictionaryComboObstacleChance;
                    float range = 0, rand = 0, top = 0;
                    ComboObstacleChance chanceGroup = ComboObstacleChance.High;

                    for (int j = 0; j < dic.Count; j++)
                        range += dic[(ComboObstacleChance)j];
                    rand = UnityEngine.Random.Range(0, range);

                    for (int j = 0; j < dic.Count; j++)
                    {
                        top += dic[(ComboObstacleChance)j];
                        if (rand < top)
                        {
                            chanceGroup = (ComboObstacleChance)j;
                            break;
                        }
                    }

                    List<ComboObstacle> obstaclesCombo = new List<ComboObstacle>();
                    for (int j = 0; j < CurLocation.combos.Count; j++)
                        if (CurLocation.combos[j].chance == chanceGroup)
                            obstaclesCombo.Add(CurLocation.combos[j]);
                    if (obstaclesCombo.Count > 0)
                    {
                        Debug.Log("Combo Obstacle Assigned");
                        curObstaclesQueue.AddRange(obstaclesCombo[UnityEngine.Random.Range(0, obstaclesCombo.Count)].obstacles);
                        _success = true;
                    }
                }
                Debug.Log("Combo Obstacles Assigned");
                newObstacle = null;
            }

            if (newObstacle == null)
                return;

            AddObstacle(newObstacle);
        }

        private void AddObstacle(ComponentObstacleDefault newObstacle)
        {
            Transform leftCornerOfNew = null;
            Transform rightCornerOfLast = null;

            newObstacle = this.Populate(Pool.None, newObstacle.gameObject, parent: dataTrackManager.obstaclesHolder)
                .GetComponent<ComponentObstacleDefault>();

            if (allObstacles.Count == 0)
                rightCornerOfLast = segments[0].rightCorner;
            else
                rightCornerOfLast = allObstacles[allObstacles.Count - 1].rightBorder;
            leftCornerOfNew = newObstacle.leftBorder;
            if (rightCornerOfLast.position.x < Player.transform.position.x && rightCornerOfLast.position.x < segments[0].rightCorner.position.x)
                rightCornerOfLast = segments[0].rightCorner;

            Vector3 difference = rightCornerOfLast.position - leftCornerOfNew.position;
            newObstacle.transform.Translate(difference);
            float spacing = dataTrackManager.obstacleSpacing.FindLast((x) => 
            {
                return x.triggerDistance <= curDistanceTraveled + newObstacle.transform.position.x;
            }).spacing;
            newObstacle.transform.position = 
                new Vector3(newObstacle.transform.position.x + 
                spacing,
                segments[0].leftCorner.transform.position.y, newObstacle.transform.position.z);

            allObstacles.Add(newObstacle);
            newObstacle.gameObject.name = newObstacle.gameObject.name.Replace("(Clone)", "").Trim();
            dictionaryTypeObstaclesCount[newObstacle.gameObject.name].count++;

            CreateCollectable(newObstacle.rightBorder.position);
        }
        private void RemoveObstacle(int index)
        {
            dictionaryTypeObstaclesCount[allObstacles[index].gameObject.name].count--;
            allObstacles[index].HandleDestroyGO();
            allObstacles.RemoveAt(index);
        }
        private void CreateCollectable(Vector3 pos)
        {
            if ((curDistanceTraveled + pos.x - Player.transform.position.x) / dataTrackManager.spawnNotesEver -
                CurCharacter.UnlockedNotesCount() - ComponentCollectableItem.notesSpawnedNow >= 1 &&
                CurCharacter.UnlockedNotesCount() + ComponentCollectableItem.notesSpawnedNow < CurCharacter.notes.Count &&
                CurCharacter == CurLocation.characterToUnlock)
            {
                if (dataTrackManager.notePrefab != null)
                    this.Populate(Pool.None, dataTrackManager.notePrefab.gameObject, pos, parent: dataTrackManager.collectablesHolder);
            }
            while (ComponentCollectableItem.godTearsSpawnedNow < 10)
            {
                if (ComponentCollectableItem.transformLastTear == null)
                    pos = new Vector2(20, pos.y);
                else
                    pos = new Vector2(ComponentCollectableItem.transformLastTear.position.x + 20, pos.y);
                if (dataTrackManager.godTearsPrefab != null)
                    this.Populate(Pool.None, dataTrackManager.godTearsPrefab.gameObject, pos, parent: dataTrackManager.collectablesHolder);
            }
        }

        private void RemovePassedObstacles()
        {
            for (int i = 0; i < allObstacles.Count; i++)
                if (!allObstacles[i].isVisible && Player.transform.position.x > allObstacles[i].rightBorder.transform.position.x)
                {
                    allObstacles[i].HandleDestroyGO();
                    allObstacles.RemoveAt(i);
                    return;
                }
            for (int i = 0; i < dataTrackManager.collectablesHolder.childCount; i++)
            {
                if (dataTrackManager.collectablesHolder.GetChild(i).position.x + 30f < Player.transform.position.x)
                {
                    dataTrackManager.collectablesHolder.GetChild(i).GetComponent<ComponentCollectableItem>().HandleDestroyGO();
                    dataTrackManager.collectablesHolder.GetChild(i).SetParent(null);
                    i--;
                }
            }
        }
        private void Accelerate()
        {
            if ((Player.dataPlayer.curSpeed > dataTrackManager.maxSpeedVertical && ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical) ||
                (Player.dataPlayer.curSpeed > dataTrackManager.maxSpeedHorizontal && ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Horizontal))
            {
                Player.dataPlayer.curSpeed = ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ? dataTrackManager.maxSpeedVertical : dataTrackManager.maxSpeedHorizontal;
                ProcessingGameManager.Default.CurGlobalAnimSpeed = ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical ? dataTrackManager.maxSpeedVertical : dataTrackManager.maxSpeedHorizontal;
            }
            else
            {
                if (dataTrackManager.accelerationMode == DataTrackManager.AccelerationMode.ByOneSecond)
                {
                    if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical)
                        Player.dataPlayer.curSpeed += dataTrackManager.accelerationVertical * Homebrew.Time.DeltaTime;
                    else
                        Player.dataPlayer.curSpeed += dataTrackManager.accelerationHorizontal * Homebrew.Time.DeltaTime;
                }
                else if (!positionReverted)
                {
                    if (ProcessingGameManager.Default.sm_GameMode.CurState == SM_GameMode.GameMode.Vertical)
                        Player.dataPlayer.curSpeed += dataTrackManager.accelerationVertical * Vector2.Distance(prevPlayerPos, Player.transform.position);
                    else
                        Player.dataPlayer.curSpeed += dataTrackManager.accelerationHorizontal * Vector2.Distance(prevPlayerPos, Player.transform.position);
                }

                ProcessingGameManager.Default.CurGlobalAnimSpeed = Player.dataPlayer.curSpeed;
            }

        }

        private void CreateNewSegment(bool spawnObstacle, Vector3 position)
        {
            ComponentTrackSegment newSegment = CurLocation.segmentPrefabs[UnityEngine.Random.Range(0, CurLocation.segmentPrefabs.Count)];

            newSegment = this.Populate(Pool.None, newSegment.gameObject, parent: dataTrackManager.segmentsHolder).GetComponent<ComponentTrackSegment>();

            newSegment.transform.position = position;

            segments.Add(newSegment.GetComponent<ComponentTrackSegment>());
        }
        private void CreateNewSegment()
        {
            ComponentTrackSegment newSegment = CurLocation.segmentPrefabs[UnityEngine.Random.Range(0, CurLocation.segmentPrefabs.Count)];

            newSegment = this.Populate(Pool.None, newSegment.gameObject, parent: dataTrackManager.segmentsHolder).GetComponent<ComponentTrackSegment>();

            Transform leftCornerOfNew = newSegment.GetComponent<ComponentTrackSegment>().leftCorner;
            Transform rightCornerOfLast = segments[segments.Count - 1].rightCorner;

            Vector3 difference = rightCornerOfLast.position - leftCornerOfNew.position;

            newSegment.transform.Translate(difference);

            segments.Add(newSegment.GetComponent<ComponentTrackSegment>());
        }

        private void RemoveUnVisibleSegments()
        {
            int checkLimit = segments.IndexOf(ComponentTrackSegment.curSegment);

            for (int i = 0; i < checkLimit; i++)
            {
                if (ProcessingCameraManager.Default.IsObjectVisible(Camera.main, segments[i].viewChecker))
                {
                    break;
                }
                segments[i].HandleDestroyGO();
                segments.RemoveAt(i);
                i--;
            }
        }

        private void RevertPosition()
        {
            Vector3 distanceToRevert =
                new Vector3((int)(ComponentTrackSegment.curSegment.transform.position.x - originPosition.x), 0, 0);

            for (int i = 0; i < segments.Count; i++)
                segments[i].transform.Translate(-distanceToRevert);
            actorPlayer.transform.Translate(-distanceToRevert);
            Camera.main.transform.Translate(-distanceToRevert);
            for (int i = 0; i < allObstacles.Count; i++)
                allObstacles[i].transform.Translate(-distanceToRevert);
            for (int i = 0; i < dataTrackManager.collectablesHolder.childCount; i++)
                dataTrackManager.collectablesHolder.GetChild(i).Translate(-distanceToRevert);

            positionReverted = true;
            positionRevertionCount++;
        }
        private void ChangePlayerSkin(int skinIndex)
        {
            float curSpeed = Default.Player.dataPlayer.curSpeed;
            Vector3 pos = Default.Player.transform.position;
            Default.Player.HandleDestroyGO();

            Player = this.Populate(Pool.None, curCharacter.skins[skinIndex].prefab, pos, parent: dataTrackManager.playerSpawnPoint.parent).GetComponent<ActorPlayer>();
            Player.CurState = ActorPlayer.State.Live;
            Player.dataPlayer.curSpeed = curSpeed;
            dataTrackManager.cameraTracker.GetComponent<ComponentLockPosition>().transformLockTo = Player.transform;

            Restart();
        }
        protected override void OnDispose()
        {
            dataTrackManager.Dispose();
            dataTrackManager = null;
            actorPlayer = null;
            segments = null;
        }
    }
}
