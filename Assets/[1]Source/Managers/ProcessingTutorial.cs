﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HSG
{
    public class ProcessingTutorial : ProcessingBase
    {
        #region Save Tags
        private const string tagNotificationControllsRun = "NotificationControllsRun";
        private const string tagNotificationControllsGlide = "NotificationControllsGlide";
        private const string tagNotificationStart = "NotificationStart";
        private const string tagNotificationLocations = "NotificationLocations";
        private const string tagNotificationAchievements = "NotificationAchievements";
        private const string tagNotificationNotes = "NotificationNotes";
        private const string tagNotificationCharacters = "NotificationCharacters";
        #endregion

        public static ProcessingTutorial Default => _default;
        private static ProcessingTutorial _default;

        public void Setup()
        {
            _default = this;
            ProcessingGameManager.OnPause += CheckNotifications;
            ProcessingGameManager.OnPlay += CheckNotifications;
            ProcessingGameManager.OnPlayerDead += CheckNotifications;
            ProcessingGameManager.OnWaitingForStart += CheckNotifications;
            SM_GameMode.OnOrientationChanged += CheckNotifications;
        }
        protected override void OnDispose()
        {
            ProcessingGameManager.OnPause -= CheckNotifications;
            ProcessingGameManager.OnPlay -= CheckNotifications;
            ProcessingGameManager.OnPlayerDead -= CheckNotifications;
            ProcessingGameManager.OnWaitingForStart -= CheckNotifications;
            SM_GameMode.OnOrientationChanged -= CheckNotifications;
        }

        public void CheckNotifications()
        {
            if (!ProcessingSaveLoad.LoadPP(tagNotificationControllsRun, false))
                SetNotification(ComponentControls.Default.buttonRun,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationControllsRun, true); });
            if (!ProcessingSaveLoad.LoadPP(tagNotificationControllsGlide, false))
                SetNotification(ComponentControls.Default.buttonGlide,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationControllsGlide, true); });
            if (!ProcessingSaveLoad.LoadPP(tagNotificationStart, false))
                SetNotification(ComponentPauseMenu.Default.buttonStartGame,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationStart, true); });

            CheckNotificationLocations();
            CheckNotificationNotes();
            CheckNotificationComics();
            CheckNotificationAchievements();
            CheckNotificationCharacter();
        }

        private void CheckNotificationLocations()
        {
            for (int i = 0; i < DataGameMain.Default.locations.Count; i++)
                if (!DataGameMain.Default.locations[i].IsUnlocked &&
                    DataGameMain.Default.locations[i].costToUnlock <= DataGameSession.Default.GodTearsCount &&
                    !ProcessingSaveLoad.LoadPP(tagNotificationLocations + '.' + DataGameMain.Default.locations[i].localizationNameKey, false))
                {
                    int j = i;
                    SetNotification(ComponentPauseMenu.Default.buttonLocations,
                        () => { ProcessingSaveLoad.SavePP(tagNotificationLocations + '.' + DataGameMain.Default.locations[j].localizationNameKey, true); });
                }
        }
        private void CheckNotificationNotes()
        {
            if (!ProcessingSaveLoad.LoadPP(tagNotificationNotes + '.' + ProcessingTrackManager.Default.CurCharacter.localizationNameKey, false) &&
                ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount() > 0)
            {
                SetNotification(ComponentPauseMenu.Default.buttonNotes,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationNotes + '.' + ProcessingTrackManager.Default.CurCharacter.localizationNameKey, true); });
            }
        }
        private void CheckNotificationComics()
        {
            if (!ProcessingTrackManager.Default.CurCharacter.ComicsIsReaded &&
                ProcessingTrackManager.Default.CurCharacter.UnlockedNotesCount() == ProcessingTrackManager.Default.CurCharacter.notes.Count)
            {
                SetNotification(ComponentPauseMenu.Default.buttonNotes,
                    () => { });
                SetNotification(ComponentNotesMenu.Default.buttonOpenComics,
                    () => { });
            }
        }
        private void CheckNotificationAchievements()
        {
            if (ProcessingSaveLoad.LoadPP(tagNotificationAchievements, false))
                return;
            for (int i = 0; i < DataGameMain.Default.achievements.Count; i++)
                if (DataGameMain.Default.achievements[i].IsUnlocked)
                {
                    SetNotification(ComponentPauseMenu.Default.buttonAchivements,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationAchievements, true); });
                    return;
                }
        }
        private void CheckNotificationCharacter()
        {
            for (int i = 1; i < DataGameMain.Default.characters.Count; i++)
                if (DataGameMain.Default.characters[i].IsUnlocked &&
                    !ProcessingSaveLoad.LoadPP(tagNotificationCharacters + '.' + DataGameMain.Default.characters[i].localizationNameKey, false))
                {
                    int j = i;
                    SetNotification(ComponentPauseMenu.Default.buttonCharacters,
                    () => { ProcessingSaveLoad.SavePP(tagNotificationCharacters + '.' + DataGameMain.Default.characters[j].localizationNameKey, true); });
                    return;
                }
        }
        private void SetNotification(Button button, Action callback)
        {
            SetNotification(button.gameObject, callback);
        }
        ComponentNotificationCallbackHelper temp = null;
        private void SetNotification(GameObject button, Action callback)
        {
            temp = button.GetComponent<ComponentNotificationCallbackHelper>();
            if (temp == null)
                temp = button.AddComponent<ComponentNotificationCallbackHelper>();
            if (temp.callback == null)
                temp.SetCallback(callback);
        }
    }
}
