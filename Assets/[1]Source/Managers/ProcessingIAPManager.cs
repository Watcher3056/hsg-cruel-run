﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using Homebrew;


// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.
namespace HSG
{
    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class ProcessingIAPManager : ProcessingBase, IStoreListener, IMustBeWipedOut
    {
        public static ProcessingIAPManager Default => _default;
        private static ProcessingIAPManager _default;

        public static event Action onItemPurchased = () => { };

        private static bool unityPurchasingInitialized;

        protected IStoreController controller;       // The Unity Purchasing system.
        protected IExtensionProvider extensions;     // The store-specific Purchasing subsystems.s;
        protected ProductCatalog catalog;

        // Allows outside sources to know whether the full initialization has taken place.
        public static bool initializationComplete;

        // Product identifiers for all products capable of being purchased: 
        // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
        // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
        // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

        // General product identifiers for the consumable, non-consumable, and subscription products.
        // Use these handles in the code to reference which product to purchase. Also use these values 
        // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
        // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
        // specific mapping to Unity Purchasing's AddProduct, below.
        /*
        public enum Consumable
        {
            godTears50,
            godTears100,
            godTears250,
            godTears500,
            godTears1000,
            godTears2500
        }
        public enum NonConsumable
        {
            mapForest,
            mapSnowHills
        }*/
        private const string itemsPrefix = "hsg.cruelworld.cruelrun.";
        //public static string kProductIDNonConsumable = "nonconsumable";
        //public static string kProductIDSubscription = "subscription";

        // Apple App Store-specific product identifier for the subscription product.
        //private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

        // Google Play Store-specific product identifier subscription product.
        //private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

        public ProcessingIAPManager()
        {
            _default = this;
            catalog = ProductCatalog.LoadDefaultCatalog();
        }
        public void Setup()
        {
            // If we haven't set up the Unity Purchasing reference
            if (controller == null)
            {
                // Begin to configure our connection to Purchasing
                InitializePurchasing();
            }
        }
        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return controller != null && extensions != null;
        }

        private void InitializePurchasing()
        {
            StandardPurchasingModule module = StandardPurchasingModule.Instance();
            module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

            ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);

            IAPConfigurationHelper.PopulateConfigurationBuilder(ref builder, _default.catalog);
            catalog = ProductCatalog.LoadDefaultCatalog();
            UnityPurchasing.Initialize(_default, builder);

            unityPurchasingInitialized = true;
        }
        /// <summary>
        /// Creates the static _default of CodelessIAPStoreListener and initializes purchasing
        /// </summary>

        public IStoreController StoreController
        {
            get { return controller; }
        }

        public IExtensionProvider ExtensionProvider
        {
            get { return extensions; }
        }

        public bool HasProductInCatalog(string productID)
        {
            foreach (var product in catalog.allProducts)
            {
                if (product.id == productID)
                {
                    return true;
                }
            }
            return false;
        }

        public Product GetProduct(string productID)
        {
            if (controller != null && controller.products != null && !string.IsNullOrEmpty(productID))
            {
                return controller.products.WithID(productID);
            }
            Debug.LogError("CodelessIAPStoreListener attempted to get unknown product " + productID);
            return null;
        }

        public void InitiatePurchase(string productID)
        {
            if (controller == null)
            {
                Debug.LogError("Purchase failed because Purchasing was not initialized correctly");
                return;
            }

            controller.InitiatePurchase(productID);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            initializationComplete = true;
            this.controller = controller;
            this.extensions = extensions;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.LogError(string.Format("Purchasing failed to initialize. Reason: {0}", error.ToString()));
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            bool success = false;
            for (int i = 0; i < DataGameMain.Default.locations.Count; i++)
            {
                if (String.Equals(e.purchasedProduct.definition.id, GetName(DataGameMain.Default.locations[i].localizationNameKey), StringComparison.Ordinal))
                {
                    DataGameMain.Default.locations[i].Unlock();
                    success = true;
                }
            }


            // Or ... an unknown product has been purchased by this user. Fill in additional products here....
            if (success)
            {
                onItemPurchased.Invoke();
                Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", e.purchasedProduct.definition.id));
            }
            else
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", e.purchasedProduct.definition.id));
            ComponentChangeLocationMenu.Default.ShowLocationPreview(ComponentChangeLocationMenu.curIndex);
            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
        {
            // we expect at least one receiver to get this message
            Debug.LogError("Failed purchase not correctly handled for product \"" + product.definition.id +
                              "\". Add an active IAPButton to handle this failure, or add an IAPListener to receive any unhandled purchase failures.");

            return;
        }
        /*
        private string GetName(Consumable consumable)
        {
            return GetName(Enum.GetName(typeof(Consumable), consumable));
        }
        private string GetName(NonConsumable nonConsumable)
        {
            return GetName(Enum.GetName(typeof(NonConsumable), nonConsumable));
        }*/
        private string GetName(string product)
        {
            return itemsPrefix + product;
        }
        /*
        public void BuyProduct(Consumable consumable)
        {
            BuyProductID(GetName(consumable));
        }
        public void BuyProduct(NonConsumable consumable)
        {
            BuyProductID(GetName(consumable));
        }*/
        public void BuyProduct(string product)
        {
            BuyProductID(GetName(product));
        }
        private void BuyProductID(string productId)
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = controller.products.WithID(productId);

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    controller.InitiatePurchase(product);
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
                    Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initiailization.
                Debug.Log("BuyProductID FAIL. Not initialized.");
            }
        }


        // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        public void RestorePurchases()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                Debug.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = extensions.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


        //  
        // --- IStoreListener
        //
    }
}