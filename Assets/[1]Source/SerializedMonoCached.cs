﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Homebrew;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace HSG
{
    [ShowOdinSerializedPropertiesInInspector]
    public abstract class SerializedMonoCached : MonoCached, ISerializationCallbackReceiver, ISupportsPrefabSerialization
    {
        public SerializationData SerializationData { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {

        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {

        }
    }
}
