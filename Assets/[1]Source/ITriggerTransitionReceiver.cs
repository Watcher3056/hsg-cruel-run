﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HSG
{
    public interface ITriggerTransitionReceiver2D
    {
        void OnTriggerEnter2D(Collider2D collision);
        void OnTriggerExit2D(Collider2D collision);
        //void OnTriggerStay2D(Collider2D collision);
        void OnCollisionEnter2D(Collision2D collision);
        void OnCollisionExit2D(Collision2D collision);
        //void OnCollisionStay2D(Collision2D collision);
    }
}
